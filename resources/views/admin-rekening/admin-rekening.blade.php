@extends('layout.layout')

@section('title', 'Daftar Rekening')

@section('name')
    <div style="color: grey;">Rekening</div>
@stop

@section('content')

<div class="card">
    <div class="tab-pane" id="profile" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <div class="ms-auto p-2 bd-highlight">
                            <form action="#" class="d-flex" method="GET">
                                @csrf
                                <table>
                                    <tr>
                                        <td>
                                            <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                        </td>
                                        <td>
                                            <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                        </td>
                                    </tr>
                                </table>
                            </form><br>
                        </div>
                    <h3>Data Rekening</h3><br>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Event</th>
                                        <th>Penyelenggara</th>
                                        <th>Email</th>
                                        <th>Bank</th>
                                        <th>No Rekening</th>
                                        <th>Atas Nama</th>
                                        <th>Hasil Penjualan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data_rekening as $row)
                                        <tr>
                                            <td>{{$row->nama}}</td>
                                            <td>{{$row->name_penyelenggara}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->bank}}</td>
                                            <td>{{$row->no_rek}}</td>
                                            <td>{{$row->atas_nama}}</td>
                                            <td>@foreach($count as $data)
                                                    @if($data->id_event==$row->id_event)
                                                        @if($data->hasil=="0")
                                                            Rp 0
                                                        @else
                                                            Rp {{number_format($data->hasil,2,",",".")}}
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td>
                                            <a href="#detail" button class="btn btn-info hover-shadow" data-toggle="modal" data-target="#detail-{{$row->id_event}}" onclick="">Detail</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data_rekening -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>
@foreach($data_rekening as $dt)
<div class="modal fade" id="detail-{{$dt->id_event}}">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Transaksi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($detail_transaksi as $detail)
                            @if($detail->id_event == $dt->id_event)
                                <tr>
                                    <td>{{$detail->name}}</td>
                                    <td>
                                        @if($detail->total=="0")
                                            Rp 0
                                        @else
                                            Rp {{number_format($detail->total,2,",",".")}}
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer float-right" >
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@stop
