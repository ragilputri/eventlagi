@extends('layout.dashboard')

@section('title', 'Registrasi Tiket')

@section('head')
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

@endsection

@section('tags')
{{$data_event->tags}}
@endsection

@section('content')
<style>

.ripple{
background-position: center;
transition: background 0.8s;
}
.ripple:hover{
  background: #47a7f5 radial-gradient(circle, transparent 1%, #47a7f5 1%) center/15000%;
}
.ripple:active{
  background-color: #6eb9f7;
  background-size: 100%;
  transition: background 0s;
}
.wrapper {
  display: grid;
  grid-template-columns: 275px 275px 275px;
  grid-gap: 35px;
}
@media only screen and (max-width: 767px){
.row{
    margin: 100px 25px;
}
}
</style>
    <div class="row" style="margin-top: 100px; align:center;">
        <div class="medium-6 columns float-center mb-5">
            @if($data_event->foto)
                <img class="thumbnail" src="{{ asset('images/'.$data_event->foto) }}" height="350" width="650">
            @endif
            <div class="text-center">
                <h2 class="font-weight-bold" style="font-family: Calibri;">{{$data_event->nama}}</h2>
                <p>{{$data_event->deskripsi}}</p>
            </div>
        </div>
        <br> <br> <br>
    <div class="medium-12 large-12">
    <form action="{{url('tiket-event/save/'.$data_event->id)}}" method="POST" enctype="multipart/form-data" id="tiket">
        @csrf
            <h2 class="font-weight-bold" style="font-family: Calibri;" align="center">Form Registrasi</h2>
            <input type="hidden" name="user_id" id="user_id" value="{{$user_data->id}}">
            <br>
                @foreach($data_tiket as $row)
                    <input type="hidden" name="id_event" id="id_event" value="{{$row->id_event}}">
                @endforeach
                    <div id="tiket">
                        @foreach($data_tiket as $row)
                        <label >
                        <input type="radio" name="price" id="price" onclick="tiket(this.value)" value="Rp {{$row->harga}}" style="position: absolute; width: 0; height: 0; opacity: 0;"  >
                                <input type="hidden" name="id_tiket" id="id_tiket" value="{{$row->id}}"  >
                                <div class="card bg-light mb-3" style="max-width: 20rem;">
                                    <div class="card-header" style="height: 2rem;"></div>
                                    <div class="card-body">
                                        <h5 class="card-title"><b>{{$row->jenis}}</b></h5>
                                        <p style="font-family: Calibri; font-size: 14pt;">
                                            @if($row->harga=="0")
                                                Rp 0
                                            @else
                                                Rp {{number_format($row->harga,2,",",".")}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                        </label>
                        @endforeach
                    </div>

                    <div>
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" id="nama" class="form-control">
                    </div>
                    <div>
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control">
                    </div>
                    <div>
                        <label for="no">No Telepon</label>
                        <input type="text" id="no" name="no_telp" class="form-control">
                    </div>
                    <div>
                        <label for="alamat">Alamat</label>
                        <input type="text" id="alamat" name="alamat" class="form-control">
                    </div>
                    <div>
                        <label for="ktp">KTP</label>
                        <input type="text" id="ktp" name="no_ktp" class="form-control">
                    </div>

                    <div>
                        <table class="table table-borderless">
                            <tr>
                                <td>Harga</td>
                                <td><input type="text" id="harga" value="" disabled></td>
                            </tr>

                            <tr>
                                <td>Discount</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td><input type="text" id="total" value="" disabled></td>
                            </tr>
                        </table>
                    </div>
                <div>
                    <button type="reset" class="btn btn-secondary">Clear</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

     <script type="text/JavaScript">

        function tiket(price) {
            document.getElementById('harga').value=price;
            document.getElementById('total').value=price;
        }
    </script>
@stop



