@extends('layout.dashboard')

@section('title', 'Event Online')

@section('head')
<!-- Styles -->
    <link rel="icon" href={{ asset('/dashboard/images/event.ico') }}>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href={{ asset('/dashboard/css/bootstrap.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/fontawesome-all.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/swiper.css') }} rel="stylesheet">
	<link href={{ asset('/dashboard/css/magnific-popup.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/styles.css') }} rel="stylesheet">

<style>
.post-content .category {
  position: absolute;
  top: 128.5px;
  left: 0;
  background: #00bfd8;;
  padding: 10px 15px;
  color: #FFFFFF;
  font-size: 14px;
  font-weight: 600;
  text-transform: uppercase;
}
.thumbnail .thumbnail-2 {
  position: absolute;
  top: 20px;
  right: 20px;
  z-index: 1;
  background: #e74c3c;
  width: 55px;
  height: 55px;
  padding: 12.5px 0;
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border-radius: 100%;
  color: #FFFFFF;
  font-weight: 700;
  text-align: center;
  -webkti-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.thumbnail .thumbnail-2 .thumbnail-3 {
  font-size: 18px;
  margin-top: 5px;
}
</style>
@endsection


@section('content')
<!-- Pricing -->
<div id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="wrap"  style="margin-top: 15vh;">
                    <center><h3>EVENT ONLINE</h3></center>
                    <form action="/event/online" method="get" >
                            <div class="search">
                                <input type="text" name="search" class="searchTerm" placeholder="&ensp;Cari event kamu disini">
                                <button type="submit" class="searchButton">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                    </form>
                </div>
                <hr style="margin-top:30vh;">
                <!-- Events -->
                <div id="events" class="cards-2">
                    <div class="container">
                        <div class="row">
                                <!-- Card -->
                                @foreach($data_event as $event)
                                    @if($event->registered=="terima" && date('Y-m-d') <= $event->tanggal)
                                        @if(date('Y-m-d') == $event->tanggal && date('h:i:sa') < $event->waktu_selesai || date('Y-m-d') < $event->tanggal)
                                            @foreach ($count as $data)
                                                @if ($data->id==$event->id && $data->total!='0')
                                                    @if($data->total < $event->kuota)
                                                        <div class="card">
                                                            <a href="/detail-event/{{$event->slug}}" style="text-decoration:none;">
                                                                <img class="card-img-top" src="/images/{{$event->foto}}" alt="Load Image" height="170px" style="object-fit: cover;">
                                                                <!-- Thumbnail-->
                                                                <div class="thumbnail">
                                                                    <div class="thumbnail-2">
                                                                        <div class="thumbnail-3">
                                                                            @if($event->harga=="0")
                                                                            FREE
                                                                            @elseif($event->harga!="0")
                                                                            IDR
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="post-content">
                                                                        <div class="category">{{$event->status}}</div>
                                                                    </div>
                                                                    <table align="center">
                                                                        <tr style="height: 7rem;">
                                                                            <td><h3 class="card-title">{{ Str::limit($event->nama, 40) }}<br><small style="color: #00acee; font-family: Perpetua;">{{\Carbon\Carbon::parse($event->tanggal)->format('l, d M Y')}}</small></h3></td>

                                                                        </tr>
                                                                        <tr style="height: 3rem;">
                                                                            <td><div style="margin-bottom: 1.5rem;">{{ Str::limit($event->deskripsi, 70) }}</div></td>
                                                                        </tr>
                                                                        <tr style="height: 1rem;">
                                                                            <td>
                                                                                <div>
                                                                                    <a class="btn btn-outline-primary btn-block" style="text-decoration:none;" href="/detail-event/{{$event->slug}}">BELI</a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @elseif($data->total >= $event->kuota)
                                                    <div class="card">
                                                        <a  style="text-decoration:none;" disabled>
                                                            <img class="card-img-top" src="/images/{{$event->foto}}" alt="Load Image" height="170px" style="object-fit: cover;">
                                                            <!-- Thumbnail-->
                                                            <div class="thumbnail">
                                                                <div class="thumbnail-2">
                                                                    <div class="thumbnail-3">
                                                                        @if($event->harga=="0")
                                                                        FREE
                                                                        @elseif($event->harga!="0")
                                                                        IDR
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="post-content">
                                                                    <div class="category">{{$event->status}}</div>
                                                                </div>
                                                                <table align="center">
                                                                    <tr style="height: 7rem;">
                                                                        <td><h7 class="card-title">{{ Str::limit($event->nama, 40) }}<br><small style="color: #00acee; font-family: Perpetua;">{{\Carbon\Carbon::parse($event->tanggal)->format('l, d M Y')}}</small></h7></td>

                                                                    </tr>
                                                                    <tr style="height: 3rem;">
                                                                        <td><div style="margin-bottom: 1.5rem;">{{ Str::limit($event->deskripsi, 70) }}</div></td>
                                                                    </tr>
                                                                    <tr style="height: 1rem;">
                                                                        <td>
                                                                            <div>
                                                                                <a class="btn btn-primary btn-block" style="text-decoration:none; color:#FFFFFF;" disabled>Habis</a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                @elseif ($data->id==$event->id && $data->total=='0')
                                                <div class="card">
                                                    <a href="/detail-event/{{$event->slug}}" style="text-decoration:none;">
                                                        <img class="card-img-top" src="/images/{{$event->foto}}" alt="Load Image" height="170px" style="object-fit: cover;">
                                                        <!-- Thumbnail-->
                                                        <div class="thumbnail">
                                                            <div class="thumbnail-2">
                                                                <div class="thumbnail-3">
                                                                    @if($event->harga=="0")
                                                                    FREE
                                                                    @elseif($event->harga!="0")
                                                                    IDR
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="post-content">
                                                                <div class="category">{{$event->status}}</div>
                                                            </div>
                                                            <table align="center">
                                                                <tr style="height: 7rem;">
                                                                    <td><h7 class="card-title">{{ Str::limit($event->nama, 40) }}<br><small style="color: #00acee; font-family: Perpetua;">{{\Carbon\Carbon::parse($event->tanggal)->format('l, d M Y')}}</small></h7></td>

                                                                </tr>
                                                                <tr style="height: 3rem;">
                                                                    <td><p class="card-text">{{ Str::limit($event->deskripsi, 70) }}</p></td>
                                                                </tr>
                                                                <tr style="height: 1rem;">
                                                                    <td>
                                                                        <div>
                                                                            <a class="btn btn-outline-primary btn-block" style="text-decoration:none;" href="/detail-event/{{$event->slug}}">BELI</a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </a>
                                                </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                @endforeach

                        </div> <!-- end of row -->
                    </div> <!-- end of container -->
                </div> <!-- end of cards-1 -->
                <!-- end of services -->
                <div style="margin-left: 40%;">
                    {{ $data_event -> appends(Request::all()) -> links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end pricing -->
@stop



