<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="Create a stylish landing page for your business startup and get leads for the offered services with this HTML landing page template.">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" />
	<meta property="og:site" content="" />
	<meta property="og:title" content=""/>
	<meta property="og:description" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>EventLagi - Join & Create Events Easily</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="icon" href={{ asset('/dashboard/images/event.ico') }}>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href={{ asset('/dashboard/css/bootstrap.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/fontawesome-all.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/swiper.css') }} rel="stylesheet">
	<link href={{ asset('/dashboard/css/magnific-popup.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/styles.css') }} rel="stylesheet">

    {{-- AOS --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<style>
.post-content .category {
  position: absolute;
  top: 128.5px;
  left: 0;
  background: #00bfd8;;
  padding: 10px 15px;
  color: #FFFFFF;
  font-size: 14px;
  font-weight: 600;
  text-transform: uppercase;
}
.thumbnail .thumbnail-2 {
  position: absolute;
  top: 20px;
  right: 20px;
  z-index: 1;
  background: #e74c3c;
  width: 55px;
  height: 55px;
  padding: 12.5px 0;
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border-radius: 100%;
  color: #FFFFFF;
  font-weight: 700;
  text-align: center;
  -webkti-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.thumbnail .thumbnail-2 .thumbnail-3 {
  font-size: 18px;
  margin-top: 5px;
}
</style>
</head>
<body data-spy="scroll" data-target=".fixed-top">

    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">

        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" >
                    <a class="nav-link page-scroll" href="/" style="color:#000000;">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle page-scroll" href="#explore" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:#000000;">Events</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#events"><span class="item-text">Event List</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                        <a class="dropdown-item text-primary" href="/create-events/check-session"><span class="item-text">Create Events</span></a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#pricing" style="color:#000000;">Pricing</a>
                </li>

                <!-- Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle page-scroll" href="#contact" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:#000000;">About</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#contact"><span class="item-text">Contact</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                        <a class="dropdown-item" href="#sosial-media"><span class="item-text">Social Media</span></a>
                    </div>
                </li>
                <!-- end of dropdown menu -->
                <li class="nav-item">
                    @if($request->session()->has('id'))
                        <a href="/users/{{$data_user->id}}">
                            <img class="rounded-circle ml-2" src="images/{{$data_user->photo}}" alt="profile" style="z-index= -1;width: 1.4rem;height: 1.4rem;align-items: middle;position: absolute;object-fit: cover;">
                        </a>
                    @else
                        @php
                            echo $data_user;
                        @endphp
                    @endif
                </li>
            </ul>
        </div>
    </nav>

    <header class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="text-container">
                            <h1><span class="turquoise">Hello,</span><br> Event Lagi</h1>
                            <p class="p-large text-white">Event Lagi is an information system and mobile application used for assist organizers in making event invitations and helping participants to join events that have been held in event.</p>
                            <a class="btn-solid-lg page-scroll" href="/login">LOGIN</a>
                        </div>
                    </div>
                </div>
        </div>
    </header>

    </header>
    <div id="explore" class="cards-2" style="margin-top: 3rem;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>EXPLORE</h2>
                    <div class="wrap">
                    <form action="/" method="get" >
                        <div class="search">
                            <input type="text" name="search" class="searchTerm" placeholder="&ensp;Cari event kamu disini">
                            <button type="submit" class="searchButton">
                                <i class="fa fa-search"></i>
                            </button>

                        </div>
                    </form>
                    </div>
                    <ul class="list-inline" style="margin-top:90px; list-style: none;">
                        <li style="display:inline; margin:20px;"><a href="/event/online"><button type="button" class="btn btn-solid-rect hover-shadow" style="padding:17px;"><i class="fas fa-wifi" style="margin-right: 0.5rem;"></i><span>Online Event</span></button></a></li>
                        <li style="display:inline;"><a href="/event/offline"><button type="button" class="btn btn-solid-rect hover-shadow" style="padding:17px;"><i class="fas fa-map-marker-alt" style="margin-right: 0.5rem;"></i><span>Offline Event</span></button></a></li>
                    </ul>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->

    <!-- Events -->
    <div id="events" class="cards-2" style="margin-top: 1rem;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>TODAY's HIGHLIGHT</h2>
                    <p class="p-heading p-large">Some most popular events are attached down here every day. Come to join events in EventLagi easier and faster!</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->

            <div class="row" data-aos="fade-up">
                    <!-- Card -->
                    @foreach($data_event as $event)
                        @if($event->registered=="terima" && date('Y-m-d') <= $event->tanggal)
                            @if(date('Y-m-d') == $event->tanggal && date('h:i:sa') < $event->waktu_selesai || date('Y-m-d') < $event->tanggal)
                                @foreach ($count as $data)
                                    @if ($data->id==$event->id && $data->total!='0')
                                        @if($data->total < $event->kuota)
                                            <div class="card">
                                                <a href="/detail-event/{{$event->slug}}" style="text-decoration:none;">
                                                    <img class="card-img-top" src="/images/{{$event->foto}}" alt="Load Image" height="170px" style="object-fit: cover;">
                                                    <!-- Thumbnail-->
                                                    <div class="thumbnail">
                                                        <div class="thumbnail-2">
                                                            <div class="thumbnail-3">
                                                                @if($event->harga=="0")
                                                                FREE
                                                                @elseif($event->harga!="0")
                                                                IDR
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="post-content">
                                                            <div class="category">{{$event->status}}</div>
                                                        </div>
                                                        <table align="center">
                                                            <tr style="height: 7rem;">
                                                                <td><h7 class="card-title">{{ Str::limit($event->nama, 40) }} <br><small style="color: #00acee; font-family: Perpetua;">{{\Carbon\Carbon::parse($event->tanggal)->format('l, d M Y')}}</small></h7></td>
                                                            </tr>
                                                            <tr style="height: 3rem;">
                                                                <td><div style="margin-bottom: 1.5rem;">{{ Str::limit($event->deskripsi, 70) }}</div></td>
                                                            </tr>
                                                            <tr style="height: 1rem;">
                                                                <td>
                                                                    <div>
                                                                        <a class="btn btn-outline-primary btn-block" style="text-decoration:none;" href="detail-event/{{$event->slug}}">BELI</a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </a>
                                            </div>
                                        @elseif($data->total >= $event->kuota)
                                        <div class="card">
                                            <a  style="text-decoration:none;" disabled>
                                                <img class="card-img-top" src="/images/{{$event->foto}}" alt="Load Image" height="170px" style="object-fit: cover;">
                                                <!-- Thumbnail-->
                                                <div class="thumbnail">
                                                    <div class="thumbnail-2">
                                                        <div class="thumbnail-3">
                                                            @if($event->harga=="0")
                                                            FREE
                                                            @elseif($event->harga!="0")
                                                            IDR
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="post-content">
                                                        <div class="category">{{$event->status}}</div>
                                                    </div>
                                                    <table align="center">
                                                        <tr style="height: 7rem;">
                                                            <td><h7 class="card-title">{{ Str::limit($event->nama, 40) }}<br><small style="color: #00acee; font-family: Perpetua;">{{\Carbon\Carbon::parse($event->tanggal)->format('l, d M Y')}}</small></h7></td>
                                                        </tr>
                                                        <tr style="height: 3rem;">
                                                            <td><div style="margin-bottom: 1.5rem;">{{ Str::limit($event->deskripsi, 70) }}</div></td>
                                                        </tr>
                                                        <tr style="height: 1rem;">
                                                            <td>
                                                                <div>
                                                                    <a class="btn btn-primary btn-block" style="text-decoration:none; color:#FFFFFF;" disabled>Habis</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </a>
                                        </div>
                                        @endif
                                    @elseif ($data->id==$event->id && $data->total=='0')
                                    <div class="card">
                                        <a href="/detail-event/{{$event->slug}}" style="text-decoration:none;">
                                            <img class="card-img-top" src="/images/{{$event->foto}}" alt="Load Image" height="170px" style="object-fit: cover;">
                                            <!-- Thumbnail-->
                                            <div class="thumbnail">
                                                <div class="thumbnail-2">
                                                    <div class="thumbnail-3">
                                                        @if($event->harga=="0")
                                                        FREE
                                                        @elseif($event->harga!="0")
                                                        IDR
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="post-content">
                                                    <div class="category">{{$event->status}}</div>
                                                </div>
                                                <table align="center">
                                                    <tr style="height: 7rem;">
                                                        <td><h7 class="card-title">{{ Str::limit($event->nama, 40) }}<br><small style="color: #00acee; font-family: Perpetua;">{{\Carbon\Carbon::parse($event->tanggal)->format('l, d M Y')}}</small></h7></td>

                                                    </tr>
                                                    <tr style="height: 3rem;">
                                                        <td><div style="margin-bottom: 1.5rem;">{{ Str::limit($event->deskripsi, 70) }}</div></td>
                                                    </tr>
                                                    <tr style="height: 1rem;">
                                                        <td>
                                                            <div>
                                                                <a class="btn btn-outline-primary btn-block" style="text-decoration:none;" href="detail-event/{{$event->slug}}">BELI</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </a>
                                    </div>
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach

            </div>
        </div>
    </div>

    <div style="margin-left: 40%;">
        {{ $data_event -> appends(Request::all()) -> links() }}
    </div>

    <!-- Contact -->
    <div id="contact" class="form-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Kontak Informasi</h2>
                    <ul class="list-unstyled li-space-lg">
                        <li class="address">Jangan ragu untuk menghubungi kami atau mengirimi kami pesan formulir kontak</li>
                        <li><i class="fas fa-map-marker-alt"></i>Jl. R.W. Monginsidi No.14, Karangwaru, Kec. Tegalrejo, Yogyakarta</li>
                        <li><i class="fas fa-phone"></i><a class="turquoise" href="tel:003024630820">0878-3869-6717</a></li>
                        <li><i class="fas fa-envelope"></i><a class="turquoise" href="mailto:eventlagilaravel@gmail.com">eventlagilaravel@gmail.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100939.98555098464!2d-122.507640204439!3d37.757814996609724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sro!4v1498231462606" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-lg-6">

                    <!-- Contact Form -->
                    <form id="contactForm" data-toggle="validator" data-focus="false">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="cname" required>
                            <label class="label-control" for="cname">Nama</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="cemail" required>
                            <label class="label-control" for="cemail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control-textarea" id="cmessage" required oninvalid="this.setCustomValidity('Atribut Wajib Diisi!')" oninput="setCustomValidity('')"></textarea>
                            <label class="label-control" for="cmessage">Ketik Pesan</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group checkbox">
                            <input type="checkbox" id="cterms" value="Agreed-to-Terms" required>Saya telah membaca dan setuju dengan Kebijakan Privasi yang dinyatakan EventLagi
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">KIRIM PESAN</button>
                        </div>
                        <div class="form-message">
                            <div id="cmsgSubmit" class="h3 text-center hidden"></div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div class="footer" id="sosial-media">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>About EventLagi</h4>
                        <p>We're passionate about offering some of the best business growth services for startups</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col middle">
                        <h4>Important Links</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Our business partners <a class="turquoise" href="#your-link">startupguide.com</a></div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
                            </li>
                        </ul>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col last">
                        <h4>Social Media</h4>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-google-plus-g fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © 2020 <a href="https://cyberolympus.com" target="_blank">Cyber Olympus</a> - All rights reserved</p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright -->
    <!-- end of copyright -->

    {{-- Script AOS --}}
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init({
            offset: 200,
            duration: 1000
        });
    </script>

    <!-- Scripts -->
    <script src={{ asset('/dashboard/js/jquery.min.js') }}></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src={{ asset('/dashboard/js/popper.min.js') }}></script> <!-- Popper tooltip library for Bootstrap -->
    <script src={{ asset('/dashboard/js/bootstrap.min.js') }}></script> <!-- Bootstrap framework -->
    <script src={{ asset('/dashboard/js/jquery.easing.min.js') }}></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src={{ asset('/dashboard/js/swiper.min.js') }}></script> <!-- Swiper for image and text sliders -->
    <script src={{ asset('/dashboard/js/jquery.magnific-popup.js') }}></script> <!-- Magnific Popup for lightboxes -->
    <script src={{ asset('/dashboard/js/validator.min.js') }}></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src={{ asset('/dashboard/js/scripts.js') }}></script> <!-- Custom scripts -->
</body>
</html>

