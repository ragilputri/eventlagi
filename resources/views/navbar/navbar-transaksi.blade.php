@extends('layout.dashboard')

@section('title', 'Pembayaran Tiket')

@section('head')
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
@endsection

@section('content')

@foreach($data_eventtiket as $row)
<!-- Pricing -->
<div id="pricing">
        <div class="container">
            <div class="row" style="margin:100px 0px;">
                <div class="col-lg-12">
                @if ($message = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                    <a href="#"><button type="button" class="close" data-dismiss="alert">×</button></a>
                    <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                    </div>
                @endif
                    <!-- Card-->
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">{{$row->nama}}</h3>
                            <h5 class="card-title">Tiket {{$row->jenis}}</h5>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="text-secondary">Tempat</span><br>
                                <span class="currency">{{$row->tempat}}</span><br><br>
                                <span class="text-secondary">Waktu</span><br>
                                <span class="currency">{{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}} WIB</span>
                            </div>
                            <hr class="cell-divide-hr">
                            <div class="row">
                                <div class="medium-6 columns">
                                <span class="text-secondary">Informasi Pemesan</span><br>
                                <span class="currency">Dipesan oleh : {{$row->name}} pada {{\Carbon\Carbon::parse($row->created_at)->format('d F Y, H:i')}}</span><br>
                                </div>
                                <div class="visible-print float-right" style="margin-left: 34%;">

                                    {!! QrCode::size(150)->generate($row->qr_code) !!}
                                </div>
                            </div>
                        </div>
                    </div> <!-- end of card -->
                    <div class="button-wrapper"><br>
                        <a href="/users/{{$row->user_id}}" class="btn-solid-reg page-scroll float-right" style="padding:20px 80px;">Pembayaran</a>
                    </div>
                </div> <!-- end of col -->
                @endforeach
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-2 -->
    <!-- end of pricing -->

@stop



