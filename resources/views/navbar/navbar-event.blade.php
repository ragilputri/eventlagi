@extends('layout.dashboard')

@section('title', 'Detail Event')

@section('head')
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

@endsection

@section('tags')
{{$data_event->tags}}
@endsection

@section('content')
<form action="{{url('tiket-event/save')}}" method="POST" enctype="multipart/form-data" >
    @csrf
    <div class="row" style="margin-top: 100px; margin-bottom: 5rem;">
      <div class="medium-7 columns" style="margin-bottom: 2rem;">
        @if($data_event->foto)
            <img class="thumbnail" src="{{ asset('images/'.$data_event->foto) }}" height="350" width="580">
        @endif

        <h5 style="font-family: Raleway, sans-serif; font-weight: bold;">TAGS :</h5>
          <div>
            @foreach (explode(", ", $data_event->tags) as $hashtag)
                <button type="button" class="btn btn-secondary btn-sm" style="margin-bottom: 0.5rem;">#{{$hashtag}}</button>
            @endforeach
        </div>
      </div>

      <div class="medium-5 large-5 columns">
        <h2 style="font-family: Raleway, sans-serif; font-weight: bold;">{{$data_event->nama}}</h2>
        <p>{{$data_event->deskripsi}}</p>
        <h5 style="font-family: Raleway, sans-serif; font-weight: bold;">Tanggal & Waktu</h5>
        <p style="font-family: Arial;">{{\Carbon\Carbon::parse($data_event->tanggal)->format('l, d F Y')}} | {{\Carbon\Carbon::parse($data_event->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($data_event->waktu_selesai)->format('H:i')}}</p>
        <h5 style="font-family: Raleway, sans-serif; font-weight: bold;">Lokasi</h5>
        <p>{{$data_event->tempat}}</p>
        <h5 style="font-family: Raleway, sans-serif; font-weight: bold;">Harga Tiket</h5>
        <div class="d-flex flex-row bd-highlight mb-3">
        @foreach($data_tiket as $tiket)
            <div class="bd-highlight text-secondary">{{$tiket->jenis}} @if($tiket->harga==0)
                                                                            Rp 0
                                                                        @else
                                                                            Rp {{number_format($tiket->harga,2,",",".")}}
                                                                        @endif</div> &nbsp;
        @endforeach
        </div>
        <a  href="{{ url('login-tiket/'.$data_event->id) }}"><button type="button" class="button large expanded btn_style btn_pale" style="background-color: #DC143C;">Registrasi</button></a>
        <div class="small secondary expanded button-group">
            <a href="http://twitter.com/share?source=sharethiscom" class="button" style="background-color:#00acee;">Twitter</a>
            <a href="https://www.facebook.com/sharer/sharer.php?u=YourPageLink.com&display=popup" class="button" style="background-color:#3b5998;">Facebook</a>
            <a href="https://mail.google.com/mail/u/2/#sent?compose=VpCqJXKKPxMPsdZvNsRhNWgbnzlfBCxPxmhdWPmxGVSJKGrwrBZGDSVTDdbcCbcRkNxZtjQ" class="button" style="background-color:#BB001B;">Gmail</a>
            <a href="#" class="button" style="background-color:#720e9e;">Yahoo</a>
        </div>
      </div>
      </div>
    </div>

    <!-- Untuk detail event masing-masing -->
    {{-- {!! QrCode::size(100)->generate(Request::url()); !!} --}}
    <div class="container">
    <div class="column row">
      <hr>
      <nav>
        <ul class="tabs" data-tabs id="example-tabs" role="tablist">
          <li class="tabs-title"><a href="#panel1" data-toggle="tab" id="panel1-tab" role="tab" aria-controls="panel1" aria-selected="true">Event Lainnya</a></li>
        </ul>
      </nav>
      <div class="tabs-content" data-tabs-content="example-tabs">
        <div class="tabs-panel is-active" id="panel1" role="tabpanel" aria-labelledby="panel1-tab">
          <h4>Event Lainnya</h4>
            @foreach ($data_lain as $data_lain)
        @if ($data_lain->id != $data_event->id && $data_lain->registered=="terima")
        <div class="media-object stack-for-small">
            <div class="media-object-section">
              <img class="thumbnail" src="{{ asset('images/'.$data_lain->foto) }}" width="250" height="200" style="object-fit: cover;">
            </div>
            <div class="media-object-section">
              <h5>{{$data_lain->nama}}</h5>
              <p>{{$data_lain->deskripsi}}</p>
              <p>{{$data_lain->tags}}</p>
              <a href="/detail-event/{{$data_lain->slug}}">Selengkapnya</a>

            </div>
        </div>
        @endif
        @endforeach
        </div>
      </div>
    </div>
</div>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    </div>
    @stop



