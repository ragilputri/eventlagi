@extends('layout.layout')

@section('title', 'Daftar Event')

@section('name')
    <div style="color: grey;">Daftar Event &nbsp; | &nbsp; <span style="color: orange; font-size: 12pt;">{{$data_penyelenggara->name_penyelenggara}}</span></div>
@stop

@section('content')
<a href="/admin/penyelenggara"><button class="btn btn-info text-white btn-rounded hover-shadow-sm"><i class="mdi mdi-arrow-left mr-2"></i><span>Kembali</span></button></a><br><br>
<div class="card">
    <div class="tab-pane" id="profile" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box"><br>
                    <h3>Daftar Event</h3><br>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Nama Event</th>
                                        <th>Jenis</th>
                                        <th>Foto</th>
                                        <th>Tempat</th>
                                        <th>Waktu</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Link</th>
                                        <th>Kuota</th>
                                        <th>Registered</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data_event as $row)
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($row->created_at)->format('d F Y H:i:s')}}</td>
                                            <td>{{$row->nama}}</td>
                                            <td>{{$row->jenis_event}}</td>
                                            <td><img width="120px" src="/images/{{$row->foto}}" alt="image"></td>
                                            <td>{{$row->tempat}}</td>
                                            <td>{{\Carbon\Carbon::parse($row->tanggal)->format('l, d F Y')}} pukul {{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}} WIB</td>
                                            <td>{{$row->deskripsi}}</td>
                                            <td>{{$row->status}}</td>
                                            <td>{{$row->link}}</td>
                                            <td>{{$row->kuota}}</td>
                                            <td>
                                                @if($row->registered=="terima")
                                                    <div style="color: green;">Diterima</div>
                                                @elseif($row->registered=="tolak")
                                                    <div style="color: red;">Ditolak</div>
                                                @else
                                                    Sedang diproses
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data_event -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>

@stop
