@extends('layout.layout')

@section('title', 'Daftar Penyelenggara')

@section('name')
    <div style="color: grey;">Penyelenggara</div>
@stop

@section('content')

@if ($message = Session::get('sukses'))
	<div class="alert alert-success alert-block">
	<a href="/admin/penyelenggara"><button type="button" class="close" data-dismiss="alert">×</button></a>
	<strong>{{ $message }}</strong>
	</div>
@endif

@if ($message = Session::get('gagal'))
	<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
	</div>
@endif

<div class="card">
    <div class="tab-pane" id="profile" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <div class="ms-auto p-2 bd-highlight">
                            <form action="/admin/penyelenggara" class="d-flex" method="GET">
                                @csrf
                                <table>
                                    <tr>
                                        <td>
                                            <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                        </td>
                                        <td>
                                            <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                                                <option value="0" selected disabled> Pilih Tahun</option>
                                                    <?php
                                                    $year = date('Y');
                                                    $min = $year - 60;
                                                    $max = $year;
                                                    for( $i=$max; $i>=$min; $i-- ) {
                                                    echo '<option value='.$i.'>'.$i.'</option>';
                                                    }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                                                <option value="0" selected disabled> Pilih Bulan</option>
                                                <option value="01"> Januari</option>
                                                <option value="02"> Februari</option>
                                                <option value="03"> Maret</option>
                                                <option value="04"> April</option>
                                                <option value="05"> Mei</option>
                                                <option value="06"> Juni</option>
                                                <option value="07"> Juli</option>
                                                <option value="08"> Agustus</option>
                                                <option value="09"> September</option>
                                                <option value="10"> Oktober</option>
                                                <option value="11"> November</option>
                                                <option value="12"> Desember</option>
                                            </select>
                                        </td>
                                        <td>
                                            <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                        </td>
                                    </tr>
                                </table>
                            </form><br>
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                            <script>
                            $(document).ready(function(){
                            $("#myInput").on("keyup", function() {
                                var value = $(this).val().toLowerCase();
                                $("#myTable tr").filter(function() {
                                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                });
                            });
                            });
                            </script>
                        </div>
                    <h3>Data Penyelenggara</h3><br>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Penyelenggara</th>
                                        <th>Email</th>
                                        <th>No HP</th>
                                        <th>KTP</th>
                                        <th>Event</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data_penyelenggara as $row)
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($row->created_at)->format('d F Y H:i:s')}}</td>
                                            <td>{{$row->name_penyelenggara}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->no_hp}}</td>
                                            <td>{{$row->ktp}}</td>
                                            <td><a href="/admin/penyelenggara/event/{{$row->id}}" class="btn btn-primary btn-sm">Daftar Event</a><br><br></td>
                                            <td>
                                                <a href="/admin/penyelenggara/edit/{{$row->id}}" class="btn btn-warning btn-sm btn-block">Edit</a>
                                                <a href="/admin/penyelenggara/delete/{{$row->id}}" class="btn btn-danger btn-sm btn-block">Remove</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data_penyelenggara -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>

@stop
