@extends('layout.layout')

@section('title', 'Daftar Peserta')

@section('name')
    <div style="color: grey;">Peserta</div>
@stop

@section('content')

@if ($message = Session::get('sukses'))
	<div class="alert alert-success alert-block">
	<a href="/admin/peserta"><button type="button" class="close" data-dismiss="alert">×</button></a>
	<strong>{{ $message }}</strong>
	</div>
@endif

@if ($message = Session::get('gagal'))
	<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
	</div>
@endif

<div class="card">
    <div class="tab-pane" id="profile" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box"><br>
                    <h3>Data Peserta</h3><br>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Event yang telah diikuti</th>
                                        <th>Tanggal Event</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data_eventtiket as $row)
                                        <tr>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->nama}}</td>
                                            <td>
                                                {{\Carbon\Carbon::parse($row->tanggal)->format('l, d F Y')}} <br>
                                                {{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}}
                                            </td>
                                            <td>
                                                @if($row->attend_time == "")
                                                    @if(date('Y-m-d') == $row->tanggal && date('H:i:sa') < $row->waktu_selesai || date('Y-m-d') < $row->tanggal)
                                                        Belum hadir
                                                    @endif
                                                    @foreach($data_transaksi as $dt)
                                                        @if($dt->id_eventtiket == $row->id)
                                                            @if($dt->status == "terima" && date('Y-m-d') == $row->tanggal && date('H:i:sa') > $row->waktu_selesai || $dt->status == "terima" && date('Y-m-d') > $row->tanggal)
                                                                <div style="color: red; font-weight: 400;">Tidak hadir</div>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if($row->attend_time!="" && $row->confirm_time!="")
                                                    <div style="color: green; font-weight: 400;">Hadir</div>
                                                @endif
                                            </td>
                                            <td>
                                                <!-- <a href="/admin/peserta/edit/{{$row->id}}" class="btn btn-warning btn-sm btn-block">Edit Data</a> -->
                                                <a href="/admin/peserta/delete/{{$row->id}}" class="btn btn-danger btn-sm btn-block">Remove</a>
                                            </td>
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
