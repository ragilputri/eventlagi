@extends('layout.layout')

@section('name', 'Edit Peserta')

@section('title', 'Edit Peserta')

@section('content')

			<form action="/admin/peserta/update/{{$data_eventtiket->id_event}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                    <label>Nama</label><br>
                    <input type="text" name="name" class="form-control" value="{{$data_eventtiket->name}}"><br>
                </p>
                <p>
                    <label>Email</label><br>
                    <input type="text" name="email" class="form-control" value="{{$data_eventtiket->email}}"><br>
                </p>
                <p>
                    <label>Event Yang Telah Diikuti</label><br>
                    <input type="text" name="nama" class="form-control" value="{{$data_eventtiket->nama}}"><br>
                </p
                <p>
                    <input type="submit" name="tombol-add" value="Update" class="btn btn-primary">
                </p>
            </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
