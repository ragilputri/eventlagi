<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Notifikasi Tiket</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
  <style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Lato:400);

    /* Take care of image borders and formatting */

    img {
      max-width: 600px;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a {
      text-decoration: none;
      border: 0;
      outline: none;
    }

    a img {
      border: none;
    }

    /* General styling */

    td, h1, h2, h3  {
      font-family: Helvetica, Arial, sans-serif;
      font-weight: 400;
    }

    body {
      -webkit-font-smoothing:antialiased;
      -webkit-text-size-adjust:none;
      width: 100%;
      height: 100%;
      color: #37302d;
      background: #ffffff;
    }

     table {
      border-collapse: collapse !important;
    }


    h1, h2, h3 {
      padding: 0;
      margin: 0;
      color: #ffffff;
      font-weight: 400;
    }

    h3 {
      color: #21c5ba;
      font-size: 24px;
    }

    .important-font {
      color: #21BEB4;
      font-weight: bold;
    }

    .hide {
      display: none !important;
    }

    .force-full-width {
      width: 100% !important;
    }
  </style>

  <style type="text/css" media="screen">
    @media screen {
       /* Thanks Outlook 2013! http://goo.gl/XLxpyl*/
      td, h1, h2, h3 {
        font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
      }
    }
  </style>

  <style type="text/css" media="only screen and (max-width: 480px)">
    /* Mobile styles */
    @media only screen and (max-width: 480px) {
      table[class="w320"] {
        width: 320px !important;
      }

      table[class="w300"] {
        width: 300px !important;
      }

      table[class="w290"] {
        width: 290px !important;
      }

      td[class="w320"] {
        width: 320px !important;
      }

      td[class="mobile-center"] {
        text-align: center !important;
      }

      td[class*="mobile-padding"] {
        padding-left: 20px !important;
        padding-right: 20px !important;
        padding-bottom: 20px !important;
      }

      td[class*="mobile-block"] {
        display: block !important;
        width: 100% !important;
        text-align: left !important;
        padding-bottom: 20px !important;
      }

      td[class*="mobile-border"] {
        border: 0 !important;
      }

      td[class*="reveal"] {
        display: block !important;
      }
    }
  </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
  <tr>
    <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

    <table cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <td style="border-bottom: 3px solid #3bcdc3;" width="100%">
          <center>
            <table cellspacing="0" cellpadding="0" width="500" class="w320">
              <tr>
                <td valign="top" style="padding:10px 0; text-align:left;" class="mobile-center">
                  <h4 class="text-info">Event Lagi</h4>
                </td>
              </tr>
            </table>
          </center>
        </td>
      </tr>
      <tr>
        <td  background="{{ asset('images/'.$data_event->foto) }}" bgcolor="#8b8284" valign="top" style="no-repeat center; background-color: #8b8284; background-position: center;">
          <div>
            <center>
              <table cellspacing="0" cellpadding="0" width="530" height="303" class="w320">
                <tr>
                  <td valign="middle" style="vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:left;" height="303">

                    <table cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                        <td>
                          <h1>Sukses Membeli Tiket!</h1><br>
                          <h2>{{$data_event->nama}}</h2>
                          <br>
                        </td>
                      </tr>
                    </table>

                    <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                    <td class="hide reveal">
                      &nbsp;
                    </td>
                      <td style="width:150px; height:33px; background-color: #3bcdc3;" >
                        <div>
                          <a href="#" style="background-color:#3bcdc3;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;">Dapatkan Aplikasi</a>
                          </div>
                      </td>
                      <td>
                        &nbsp;
                      </td>
                    </tr>
                  </table>
                  </td>
                </tr>
              </table>
            </center>
          </div>
          <!--[if gte mso 9]>
            </v:textbox>
          </v:rect>
          <![endif]-->
        </td>
      </tr>
      <tr class="force-full-width">
        <td valign="top" class="force-full-width">
          <center>
            <table cellspacing="0" cellpadding="0" width="500" class="w320">
              <tr>
                <td valign="top" style="border-bottom:1px solid #a1a1a1;">

                <table cellspacing="0" cellpadding="0" class="force-full-width">
                  <tr>
                    <td style="padding: 30px 0;" class="mobile-padding">

                    <table class="force-full-width" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="text-align: left;">
                          <span class="important-font">
                            Dipesan Oleh<br>
                          </span>
                          {{$data_eventtiket->name}} <br>
                          {{$data_eventtiket->email}}<br>
                          {{$data_eventtiket->no_telp}}
                        </td>
                        <td style="text-align: right; vertical-align:top;">
                          <span class="important-font">
                            Dipesan Pada<br>
                          </span>
                          Tiket #{{$data_eventtiket->id}}<br>
                          {{\Carbon\Carbon::parse($data_eventtiket->created_at)->format('d F Y H:i:s')}}
                        </td>
                      </tr>
                    </table>

                    </td>
                  </tr>
                  <tr>
                    <td style="padding-bottom: 30px;" class="mobile-padding">

                      <table class="force-full-width" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="mobile-block">
                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td class="mobile-border" style="background-color: #3bcdc3; color: #ffffff; padding: 5px; border-right: 3px solid #ffffff; text-align:left;">
                                  Informasi Tiket
                                </td>
                              </tr>
                              <tr>
                                <td  style="background-color: #ebebeb; padding: 8px; border-top: 3px solid #ffffff; text-align:left;">
                                  <span class="text-info">1 x Tiket</span>
                                  <p style="font-size:15px;">
                                    @if($data_transaksi->total=="0")
                                        <b>Rp 0</b>
                                    @else
                                        <b>Rp {{number_format($data_transaksi->total,2,",",".")}}</b>
                                    @endif
                                  </p>
                                  <span class="text-info">Tempat</span>
                                  <p style="font-size:15px;">{{$data_event->tempat}}, {{\Carbon\Carbon::parse($data_event->tanggal)->format('d F Y')}} dari pukul {{\Carbon\Carbon::parse($data_event->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($data_event->waktu_selesai)->format('H:i')}} WIB</p>
                                  <span class="text-info">Status</span>
                                  <p style="font-size:15px;">{{$data_event->status}} | {{$data_event->link}}</p>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr><br>
                        <tr>
                          <td class="mobile-block">
                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td class="mobile-border" style="background-color: #3bcdc3; color: #ffffff; padding: 5px; border-right: 3px solid #ffffff; text-align:left;">
                                  Informasi Rekening
                                </td>
                              </tr>
                              <tr>
                                <table class="table table-borderless" style="background-color: #ebebeb; padding: 8px; border-top: 3px solid #ffffff; text-align:left;">
                                    <tr>
                                        <td width="300px"><img src="{{ asset('/images/bank/BRI.png') }}" alt="BRI" width="50px"></td>
                                        <td width="400px">a/n Event Lagi <br> 0111-01-057361-50-7</td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{ asset('/images/bank/Mandiri.png') }}" alt="Mandiri" width="50px"></td>
                                        <td>a/n Event Lagi <br> 900-00-4619123-6</td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{ asset('/images/bank/BNI.png') }}" alt="BNI" width="50px"></td>
                                        <td>a/n Event Lagi <br> 040-76-334456-2</td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{ asset('/images/bank/BCA.png') }}" alt="BCA" width="50px"></td>
                                        <td>a/n Event Lagi <br> 3179-0100016-32-54</td>
                                    </tr>
                                </table>
                              </tr>
                            </table>
                          </td>
                        </tr><br>
                        <tr>
                          <td class="mobile-block">
                            <table cellspacing="0" cellpadding="0" class="force-full-width">
                              <tr>
                                <td class="mobile-border" style="background-color: #3bcdc3; color: #ffffff; padding: 5px; border-right: 3px solid #ffffff; text-align:left;">

                                </td>
                              </tr>
                                <td  style="background-color: #ebebeb; padding: 8px; border-top: 3px solid #ffffff; text-align:left;">
                                  <span class="text-info">Silahkan upload bukti pembayaran di link berikut : https://eventlagi.com/users/{{$data_transaksi->user_id}}</span>
                                </td>
                            </table>
                          </td>
                        </tr>
                      </table>

                    </td>
                  </tr>
                </table>
                </td>
              </tr>
              <tr>
                <td>

                  <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                      <td class="mobile-padding" style="text-align:left;">
                      <br>
                        Terima kasih telah bergabung bersama <a style="color: #2BB6AC;" href="https://eventlagi.com">Event Lagi </a>!
                      <br>
                      <br>
                      <br>

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </center>
        </td>
      </tr>
      <tr>
        <td style="background-color:#c2c2c2;">
          <center>
            <table cellspacing="0" cellpadding="0" width="500" class="w320">
              <tr>
                <td>
                  <table cellspacing="0" cellpadding="30" width="100%">
                    <tr>
                      <td style="text-align:center;">
                        <a href="#">
                          <img width="61" height="51" src="https://www.filepicker.io/api/file/fZaNDx7cSPaE23OX2LbB" alt="Google Plus" />
                        </a>
                        <a href="#">
                          <img width="61" height="51" src="https://www.filepicker.io/api/file/b3iHzECrTvCPEAcpRKPp" alt="Facebook" />
                        </a>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </center>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
