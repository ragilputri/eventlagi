@extends('layout.user')

@section('title', 'Ticket History')

@section('content')


<div class="row"> <!-- Div row-->
    <div class="col-lg-12 col-xlg-9 col-md-7"> <!-- Div col-lg-12-->
        <form action="/users/tikets/{{$data_user->id}}" class="d-flex row g-2" method="GET">
            @csrf
            <table>
                <tr>
                    <td>
                        <input class="form-control me-2" type="search" id="search" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                    </td>
                    <td>
                        <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                            <option value="0" selected disabled> Pilih Tahun</option>
                                <?php
                                $year = date('Y');
                                $min = $year - 60;
                                $max = $year;
                                for( $i=$max; $i>=$min; $i-- ) {
                                echo '<option value='.$i.'>'.$i.'</option>';
                                }?>
                        </select>
                    </td>
                    <td>
                        <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                            <option value="0" selected disabled> Pilih Bulan</option>
                            <option value="01"> Januari</option>
                            <option value="02"> Februari</option>
                            <option value="03"> Maret</option>
                            <option value="04"> April</option>
                            <option value="05"> Mei</option>
                            <option value="06"> Juni</option>
                            <option value="07"> Juli</option>
                            <option value="08"> Agustus</option>
                            <option value="09"> September</option>
                            <option value="10"> Oktober</option>
                            <option value="11"> November</option>
                            <option value="12"> Desember</option>
                        </select>
                    </td>
                    <td>
                        <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                    </td>
                </tr>
            </table>
        </form><br>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
        $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
        </script>
        <!-- tabel -->
        <div class="card">
            <div class="tab-pane" id="profile" role="tabpanel">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="white-box"><br>
                            <h3>Ticket History</h3><br>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Event</th>
                                                <th>Foto</th>
                                                <th>Tanggal Pelaksana</th>
                                                <th>Waktu Pelaksana</th>
                                                <th>Tempat</th>
                                                <th>Deskripsi</th>
                                                <th>Harga Tiket</th>
                                                <th>Dipesan Oleh</th>
                                                <th>Dipesan Pada</th>
                                            </tr>
                                        </thead>
                                        <tbody id="myTable">
                                            @foreach($data_eventtiket as $row)
                                                @if(date('Y-m-d') >= $row->tanggal && $row->status=="terima")
                                                    @if(date('Y-m-d') == $row->tanggal && date('h:i:sa') > $row->waktu_selesai || date('Y-m-d') > $row->tanggal)
                                                        <tr>
                                                            <td>{{$row->nama}}</td>
                                                            <td><img src="/images/{{$row->foto}}" alt="Image" width="130"></td>
                                                            <td>{{\Carbon\Carbon::parse($row->tanggal)->format('l, d F Y')}}</td>
                                                            <td>{{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}}</td>
                                                            <td>{{$row->tempat}}</td>
                                                            <td>{{$row->deskripsi}}</td>
                                                            <td>
                                                                @if($row->harga=="0")
                                                                    Rp 0
                                                                @else
                                                                    Rp {{number_format($row->harga,2,",",".")}}
                                                                @endif
                                                            </td>
                                                            <td>{{$row->name}}</td>
                                                            <td>{{\Carbon\Carbon::parse($row->created_at)->format('l, d F Y H:i:s')}}</td>
                                                        </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-left: 45%;">
                {{ $data_eventtiket -> appends(Request::all()) -> links() }}
            </div>
        </div>
    </div>
</div>
@stop

