@extends('layout.user')

@section('title', 'Daftar Peserta')

@section('content')

<div class="row">
    <div class="col-lg-12 col-xlg-9 col-md-7">
        <ul class="list-inline" style="list-style: none;">
            <li style="display:inline;"><a href="/users/penyelenggara/create/{{$data_user->id}}"><button class="btn btn-primary text-white">Buat Event</button></a></li>
            <li style="display:inline;"><a href="/users/penyelenggara/{{$data_user->id}}"><button class="btn btn-success text-white">Data Penyelenggara</button></a></li>
            <li style="display:inline;"><a href="/users/events/{{$data_user->id}}"><button class="btn btn-success text-white">Kembali</button></a></li>
        </ul>
        @if ($message = Session::get('sukses'))
            <div class="alert alert-success alert-block">
            <a href="/users/list/peserta"><button type="button" class="close" data-dismiss="alert">×</button></a>
            <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('gagal'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
            </div>
        @endif
        @foreach($data_peserta as $row)
        <div class="card">
            <div class="card-header">
            Form Tiket
            </div>
            <div class="card-body">
                <h2 class="card-title">List Peserta</h2>
                <a href="#modalHapus" data-toggle="modal" onclick=""><button class="btn btn-danger">Hapus Peserta</button></a>
            </div>
        </div>
        @endforeach
    </div>
</div>

@stop