@extends('layout.user')

@section('title', 'Daftar Tiket')

@section('content')

<div class="row">
<!-- Column -->
    <div class="col-lg-12 col-xlg-9 col-md-7">
        <ul class="list-inline" style="list-style: none;">
                <li style="display:inline;"><a href="/users/events/{{$data_user->id}}"><button class="btn btn-info text-white btn-rounded hover-shadow-sm"><i class="mdi mdi-arrow-left mr-2"></i><span>Kembali</span></button></a></li>
                <li style="display:inline;"><a href="#modalTambah" data-toggle="modal" onclick=""><button class="btn btn-purple text-white btn-rounded hover-shadow-sm ml-2"><i class="mdi mdi-plus mr-2"></i><span>Tambah Tiket</span></button></a></li>
                <li style="display:inline;"><a href="/users/rekening/{{$data_user->id}}/{{$data_event->id}}"><button class="btn btn-purple text-white btn-rounded hover-shadow-sm ml-2"><i class="mdi mdi-credit-card mr-2"></i><span>Daftar Rekening</span></button></a></li>
        </ul>
        @if ($message = Session::get('sukses'))
            <div class="alert alert-success alert-block">
            <a href="/users/kategori/tiket/{{$data_user->user_id}}/{{$data_event->id}}"><button type="button" class="close" data-dismiss="alert">×</button></a>
            <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('gagal'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
            </div>
        @endif

        <div class="alert alert-danger" role="alert">
        Anda harus menambahkan data <a href="/users/rekening/create/{{$data_user->id}}/{{$data_event->id}}" class="alert-link">Rekening</a> untuk proses transaksi.
        </div>
        <!-- tabel -->
        @foreach($data_kategori as $row)
        <div class="card">
            <div class="card-header">
            Form Tiket
            </div>
            <div class="card-body">
                <h2 class="card-title">Informasi Tiket {{$row->nama}}</h2>
                <h6 class="card-text">Jenis       :   {{$row->jenis}}</h6><br>
                <h6 class="card-text">Harga       :   @if($row->harga=="0")
                                                            Rp 0
                                                        @else
                                                            Rp {{number_format($row->harga,2,",",".")}}
                                                        @endif</h6><br>
                <h6 class="card-text">Deskripsi   :   {{$row->deskripsi}}</h6><br>

                <a href="/users/tiket/delete/{{$row->id}}"><button class="btn btn-danger">Hapus Tiket</button></a>
                <!-- <a href="#modalHapus" data-toggle="modal" onclick=""><button class="btn btn-danger">Hapus Tiket</button></a> -->
            </div>
        </div>
    @endforeach
        <!-- Modal Tambah Tiket -->
        <div class="modal fade" id="modalTambah">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Ingin Menambah Tiket?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/users/tiket/create/{{$data_user->id}}/{{$data_event->id}}" class="float-right">
                            <button class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                            <button class="btn btn-primary" type="submit">Ya</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
