@extends('layout.user')

@section('title', 'Transaksi')

@section('content')

<div class="row">
<!-- Column -->
<div class="col-lg-12 col-xlg-9 col-md-7">
    <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#transaksi"
                    role="tab">Transaksi</a>
            </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings"
                    role="tab">Transaksi Detail</a>
            </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pembayaran"
                    role="tab">Tata Cara Pembayaran</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="transaksi" role="tabpanel">

                @foreach($data_eventtiket as $row)
                <div class="card-body">
                    <div class="sl-item">
                        <div class="card text-dark bg-light mb-3" style="max-width: 100rem;">
                            <div class="card-body" id="home">
                                <form action="/uploadbukti/{{$row->id}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <h3 class="card-title">{{$row->nama}}</h3>
                                    <div>
                                        <table class="table table-borderless">
                                            <tr>
                                                <td style="width: 15rem;">Harga</td>
                                                <td>
                                                    @if($row->harga=="0")
                                                        Rp 0
                                                    @else
                                                        Rp {{number_format($row->harga,2,",",".")}}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Discount</td>
                                                <td>Rp 0</td>
                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                <td>
                                                    @if($row->total=="0")
                                                        <b>Rp 0</b>
                                                    @else
                                                        <b>Rp {{number_format($row->total,2,",",".")}}</b>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                @if($row->harga!="0")
                                    @if($row->status=="terima")
                                        <h3 class="text-secondary" align="center">Transaksi Diterima</h3>
                                    @elseif($row->status=="tolak")
                                        <div class="button-wrapper"><br>
                                            <input type="file" name="bukti_up" id="inputGroupFile01" placeholder="Upload Bukti Pembayaran">
                                            <button type="submit" class="btn btn-info">Submit Bukti Pembayaran</button>
                                        </div>
                                    @elseif($row->status=="" && $row->bukti_up!="")
                                        <h3 class="text-secondary" align="center">Diproses</h3>
                                    @elseif($row->status=="" && $row->bukti_up=="")
                                        <div class="button-wrapper"><br>
                                            <input type="file" name="bukti_up" id="inputGroupFile01" placeholder="Upload Bukti Pembayaran">
                                            <button type="submit" class="btn btn-info">Submit Bukti Pembayaran</button>
                                        </div>
                                    @endif

                                @else($row->harga=="0")
                                    <h3 class="text-secondary" align="center">Transaksi Berhasil</h3>
                                @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <!-- second tab -->

            <div class="tab-pane" id="settings" role="tabpanel">
                <div class="row" style="margin:10px 0px;">
                    <div class="col-lg-12">
                        @foreach ($data_eventtiket as $row)
                        <div class="card text-dark bg-light mb-3">
                            <div class="card-body">
                                <div class="price" style="margin: 0 20px 0 20px">
                                    <h4>Deskripsi Event</h4>
                                    <p>{{$row->deskripsi}}</p>
                                    <h4>Waktu</h4>
                                    <p>{{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}} WIB</p>
                                    <h4>Lokasi</h4>
                                    <p>{{$row->tempat}}</p>
                                </div>
                            </div>
                            <!-- Card-->
                            <div class="card col-lg-12">
                                <div class="card-body">
                                    <h3 class="card-title">{{$row->nama}}</h3>
                                    <h5 class="card-title">Tiket {{$row->jenis}}</h5>
                                    <hr class="cell-divide-hr">
                                    <div class="price">
                                        <span class="text-secondary">Tempat</span><br>
                                        <span class="currency">{{$row->tempat}}</span><br><br>
                                        <span class="text-secondary">Waktu</span><br>
                                        <span class="currency">{{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}} WIB</span>
                                    </div>
                                    <hr class="cell-divide-hr">
                                    <div class="price">
                                        <div class="medium-6 columns">
                                        <span class="text-secondary">Informasi Pemesan</span><br>
                                        <span class="currency">Dipesan oleh : {{$row->name}} pada <span>{{\Carbon\Carbon::parse($row->created_at)->format('l, d F Y H:i:s')}}</span></span><br>
                                        </div>
                                        <div class="medium-6 large-6 columns float-right visible-print">
                                            <!-- {!! QrCode::size(150)->generate('EventLagi.com'); !!} -->

                                            {!! QrCode::size(150)->generate($row->qr_code) !!}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end of card -->
                        </div>
                        @endforeach
                    </div> <!-- end of col -->
                </div>
            </div> <!-- end of row -->

        <!-- Pembayaran -->
            <div class="tab-pane" id="pembayaran" role="tabpanel">
                <div class="card-body">
                    <div class="sl-item">
                        <div class="card text-dark bg-light mb-3" style="max-width: 100rem;">
                            <div class="card-body" id="pembayaran">
                                <h4>Cara Pembayaran Melalui Transfer Bank<h4>
                                <h6>Berikut panduan melakukan pembayaran dengan transfer bank :</h6>
                                <p>1. Setelah anda selesai memilih produk dan melanjutkan ke halaman keranjang belanja</p>
                                <p>2. Pada halaman checkout, pilih durasi pengiriman dan pilih metode pembayaran transfer bank</p>
                                <p>3. User added image</p>
                                <p>4. Pada halaman selanjutnya, pilih bank yang digunakan, lalu isi nomor serta nama pemilik rekening.</p>
                                <p>5. Klik Bayar</p>
                                <p>6. User added image</p>
                                <p>7. Lakukan pembayaran sesuai jumlah tagihan beserta kode unik (3 angka terakhir dari nominal yang harus dibayarkan)</p>
                                <p>8. Tunggu hingga pembayaran anda diverifikasi oleh EventLagi. Untuk waktu verifikasi yaitu 1*24 jam</p>
                                <p>9. Pastikan pembayaran anda sudah berhasil</p>
                                <p>10. Unggah bukti untuk mempercepat proses verifikasi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-left: 45%;">
        {{ $data_eventtiket -> appends(Request::all()) -> links() }}
    </div>
</div>
</div>

@stop
