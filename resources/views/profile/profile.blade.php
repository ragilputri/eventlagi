@extends('layout.user')

@section('title', 'Profile')

@section('space')
    <div style="height: 2rem;"></div>
@endsection

@section('content')
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body profile-card">
                                <center class="m-t-30"><abbr title="Change Profile"> <a data-toggle="modal" data-target="#changeProfile"><img src="{{ asset('images/'.$data_user->photo) }}" alt="Profile Photo"
                                    class="rounded-circle hover-shadow waves-effect" width="150" height="150" style="object-fit: cover;" /></a> </abbr>
                                    <h4 class="card-title m-t-10">{{$data_user->name}}</h4>
                                    <h6 class="card-subtitle">{{$data_user->email}}</h6>
                                    @php
                                        if($data_user->bio != "") {
                                            $data_user->bio = preg_replace("#\[sp\]#", "&nbsp;", $data_user->bio);
                                            $data_user->bio = preg_replace("#\[nl\]#", "<br>\n", $data_user->bio);
                                            echo "<blockquote>".$data_user->bio."</blockquote>";
                                        }
                                    @endphp
                                </center>
                            </div>
                            
                            <!-- Photo Options -->
                            <div class="modal fade show" id="changeProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <div style="height: 1rem;"></div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    <center>
                                        <a href="#viewPhoto" data-toggle="modal" data-target="#viewPhoto" class="pilih">View Photo</a>
                                        <hr>
                                        <label for="uploadPhoto" class="pilih mt-1">
                                            <a>Upload Photo</a>
                                        </label>
                                        <hr>
                                        <a href="/users/remove-photo/{{$data_user->id}}" class="pilih">Remove Photo</a>
                                    </center>
                                    </div>
                                    <div class="modal-footer">
                                        <div style="height: 1rem;"></div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <!-- View Photo -->
                            <div class="modal fade show" id="viewPhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <div style="height: 1rem;"></div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <center>
                                            <img src="{{ asset('images/'.$data_user->photo) }}" alt="Profile photo" width="250vw" height="250vh" style="object-fit: cover;">
                                        </center>
                                    </div>
                                    <div class="modal-footer">
                                        <div style="height: 1rem;"></div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <!-- Upload Photo -->
                            <form action="/users/upload-photo/{{$data_user->id}}" id="form" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="file"  accept="image/*" id="uploadPhoto" name="photo"  style="opacity: 0; position: absolute; width: 0; height: 0;">
                            </form>
                            <script>
                                document.getElementById("uploadPhoto").onchange = function() {
                                    document.getElementById("form").submit();
                                };
                            </script>

                        </div>
                    </div>
                    <!-- Column -->

                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profileList"
                                        role="tab">Settings</a>
                                </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#passwordList"
                                        role="tab">Change Password</a> </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <!-- First Tab -->
                                <div class="tab-pane active" id="profileList" role="tabpanel">
                                    <div class="card-body">
                                        <form action="/users/update-profile/{{$data_user->id}}" method="POST" class="form-horizontal form-material" enctype="multipart/form-data" onsubmit="addNewLines()">
                                            @csrf
                                            <div class="form-group">
                                                <label class="col-md-12">Nama Lengkap</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="nama" value="{{$data_user->name}}" class="form-control form-control-line pl-0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12 mt-2">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" name="email" value="{{$data_user->email}}" class="form-control form-control-line pl-0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 mt-2">No HP</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="no_hp" value="{{$data_user->phone}}" class="form-control form-control-line pl-0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 mt-2">Bio</label>
                                                <div class="col-md-12">
                                                    <textarea id="bio" rows="3" style="display: none;" name="bio"></textarea>
                                                    
                                                        @php
                                                            $data_user->bio = preg_replace("#\[sp\]#", "&nbsp;", $data_user->bio);
                                                            $data_user->bio = preg_replace("#\[nl\]#", '\n', $data_user->bio);
                                                            echo "<textarea id='userTypes' class='form-control form-control-line pl-0' rows='4'>".$data_user->bio."</textarea>";
                                                        @endphp
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-success text-white hover-shadow">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                        <script>
                                            function addNewLines() {
                                                var text = document.getElementById('userTypes').value;
                                                var text = text.replace(/  /g, "[sp][sp]");
                                                var text = text.replace(/\n/g, "[nl]");
                                                document.getElementById('bio').value = text;
                                                return false;
                                            }
                                        </script>
                                    </div>
                                </div>

                                <!-- Second Tab -->
                                <div class="tab-pane" id="passwordList" role="tabpanel">
                                    <div class="card-body">
                                        @if ($message = Session::get('sukses'))
                                            <div class="alert alert-success alert-block">
                                            <a href="/users/profile/{{$data_user->id}}"><button type="button" class="close" data-dismiss="alert">×</button></a>
                                            <strong>{{ $message }}</strong>
                                            </div>
                                        @endif

                                        @if ($message = Session::get('gagal'))
                                            <div class="alert alert-danger alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                            </div>
                                        @endif
                                        
                                        <form class="form-horizontal form-material" action="/users/change-password/{{$data_user->id}}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12 mt-2">Current Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="current_password" class="form-control form-control-line pl-0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12 mt-2">New Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="password" class="form-control form-control-line pl-0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12 mt-2">Confirm New Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="confirm_password" class="form-control form-control-line pl-0">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-success text-white">Change Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@stop
