@extends('layout.user')

@section('title', 'Create Event')

@section('content')
<h3>Form Event</h3><br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

			<form action="/users/event/save/{{$data_user->id}}/{{$data_penyelenggara->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                    <label>Penyelenggara</label><br>
                    <input type="text" value="{{$data_penyelenggara->name_penyelenggara}}" class="form-control" disabled><br>
                </p>
                <p>
                    <label>Jenis Event</label><br>
                    <input type="text" name="jenis_event" class="form-control"><br>
                </p>
                <p>
                    <label>Foto</label><br>
                    <input type="file" name="foto"><br><br>
                </p>
                <p>
                    <label>Nama Event</label><br>
                    <input type="text" name="nama" class="form-control"><br>
                </p>
                <p>
                    <label>Tanggal</label><br>
                    <input type="date" name="tanggal" class="form-control"><br>
                </p>
                <p>
                    <label>Tempat</label><br>
                    <input type="text" name="tempat" class="form-control"><br>
                </p>
                <p>
                    <label>Waktu Mulai</label><br>
                    <input type="time" name="waktu_mulai" class="form-control"><br>
                </p>
                <p>
                    <label>Waktu Selesai</label><br>
                    <input type="time" name="waktu_selesai" class="form-control"><br>
                </p>
                <p>
                    <label>Status</label><br>
                    <select name="status" id="status" class="form-control">
                        <option value="-"></option>
                        <option value="Online">Online</option>
                        <option value="Offline">Offline</option>
                    </select><br>
                </p>
                <p>
                    <label>Deskripsi</label><br>
                    <textarea type="text" name="deskripsi" class="form-control" rows="3"></textarea><br>
                    <div id='tampil'></div>
                </p>
                <p>
                    <label for="tags">Tags</label><br>
                    <input type="text" name="tags" class="form-control" placeholder="ex: seminar, konser, event, musik"><br>
                </p>
                <p>
                    <label>Kuota</label><br>
                    <input type="text" name="kuota" class="form-control"><br>
                </p>
                <p>
                    <input type="submit" name="tombol_add" value="Simpan" class="btn btn-primary">
                </p>
            </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
