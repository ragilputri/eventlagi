@extends('layout.user')

@section('title', 'Edit Event')

@section('content')
<h3>Form Event Edit</h3><br>
			<form action="/users/event/update/{{$data_user->id}}/{{$data_event->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                    <label>Penyelenggara</label><br>
                    <input type="text" value="{{$data_event->name_penyelenggara}}" class="form-control" disabled><br>
                </p>
                <p>
                    <label>Jenis Event</label><br>
                    <input type="text" name="jenis_event" value="{{$data_event->jenis_event}}" class="form-control"><br>
                </p>
                <p>
                    <label>Foto</label><br>
                    @if($data_event->foto)
                        <img id="foto" src="{{ asset('images/'.$data_event->foto) }}" height="70">
                    @endif
                        <br><br>
                        <input type="file" name="foto" placeholder=""  value="{{ $data_event->foto }}">
                        <span class="text-danger">{{ $errors->first('image') }}</span><br><br>
                </p>
                <p>
                    <label>Nama Event</label><br>
                    <input type="text" name="nama" value="{{$data_event->nama}}" class="form-control"><br>
                </p>
                <p>
                    <label>Tanggal</label><br>
                    <input type="date" name="tanggal" value="{{$data_event->tanggal}}" class="form-control"><br>
                </p>
                <p>
                    <label>Tempat</label><br>
                    <input type="text" name="tempat" value="{{$data_event->tempat}}" class="form-control"><br>
                </p>
                <p>
                    <label>Waktu Mulai</label><br>
                    <input type="time" name="waktu_mulai" value="{{$data_event->waktu_mulai}}" class="form-control"><br>
                </p>
                <p>
                    <label>Waktu Selesai</label><br>
                    <input type="time" name="waktu_selesai" value="{{$data_event->waktu_selesai}}" class="form-control"><br>
                </p>
                <p>
                    <label>Deskripsi</label><br>
                    <textarea name="deskripsi" class="form-control">{{$data_event->deskripsi}}</textarea><br>
                </p>
                <p>
                    <label>Tags</label><br>
                    <input type="text" name="tags" value="{{$data_event->tags}}" class="form-control"><br>
                </p>
                <p>
                    <label>Status</label><br>
                    <select name="status" class="form-control">
                        <option value="Online" @if($data_event->status=='Online') selected @endif>Online</option>
                        <option value="Offline" @if($data_event->status=='Offline') selected @endif>Offline</option>
                    </select><br>
                </p>
                <p>
                    <label>Kuota</label><br>
                    <input type="text" name="kuota" value="{{$data_event->kuota}}" class="form-control"><br>
                </p>
                <p>
                    <input type="submit" name="tombol_add" value="Simpan" class="btn btn-primary">
                </p>
            </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
