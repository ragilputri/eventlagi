@extends('layout.user')

@section('title', 'Create Ticket')

@section('content')
<style>
    .wrapper {
    display: grid;
    grid-template-columns: 275px 275px 275px;
    grid-gap: 36px;
    }
</style>
<div class="row"> <!-- Div row-->
    <div class="medium-5 large-12 "> <!-- Div col-lg-12-->
    <div class="wrapper" >
        @foreach($data_kategori as $row)

                <div class="card bg-light mb-3" style="max-width: 20rem;">
                    <div class="card-header" style="height: 2rem;"></div>
                    <div class="card-body">
                        <h5 class="card-title"><b>{{$row->jenis}}</b></h5>
                        <p class="card-text">Rp {{$row->harga}}</p>
                        <p class="card-text">{{$row->deskripsi}}</p>
                    </div>
                </div>

        @endforeach


    </div>
    </div>
</div>
<h3>Form Tiket</h3><br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <form action="/users/tiket/save/{{$data_user->id}}/{{$data_event->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                    <label>Event</label><br>
                    <input type="text" name="id_event" value="{{$data_event->nama}}" class="form-control" disabled><br>
                </p>
                <p>
                    <label>Jenis</label><br>
                    <input type="text" name="jenis" class="form-control"><br>
                </p>
                <p>
                    <label>Harga</label><br>
                    <input type="text" name="harga" class="form-control">
                    <div class="text-secondary">*Jika event Anda gratis maka isi harga dengan "0"</div><br>
                </p>
                <p>
                    <label>Deskripsi</label><br>
                    <textarea type="text" name="deskripsi" class="form-control"></textarea>
                </p>
                <p>
                    <ul class="list-inline" style="list-style: none;">
                        <li ><input type="submit" name="simpan" value="Simpan" class="btn btn-danger"></li>
                    </ul>
                </p>
            </form>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
