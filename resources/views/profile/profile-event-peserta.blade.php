@extends('layout.user')

@section('title', 'Daftar Peserta')

@section('content')
<div class="row">
<!-- Column -->
    <div class="col-lg-12 col-xlg-9 col-md-7">
        <ul class="list-inline" style="list-style: none;">
            <li style="display:inline;"><a href="/users/events/{{$data_user->id}}"><button class="btn btn-info text-white btn-rounded hover-shadow-sm"><i class="mdi mdi-arrow-left mr-2"></i><span>Kembali</span></button></a></li>
        </ul>
        <div class="card">
    <div class="tab-pane" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 col-xs-6 b-r"> <strong>Kuota</strong>
                    <br>
                    <p class="text-muted">
                    {{$data_event->kuota}}
                    </p>
                </div>
                <div class="col-md-4 col-xs-6 b-r"> <strong>Total Pembeli</strong>
                    <br>
                    <p class="text-muted">{{$count}}</p>
                </div>
                <div class="col-md-4 col-xs-6 b-r"> <strong>Total Hadir</strong>
                    <br>
                    <p class="text-muted">
                    @foreach($hadir as $hdr)
                        @if($hdr->id_event == $data_event->id)
                        {{$hdr->total}}
                        @endif
                    @endforeach
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- tabel -->
        <div class="card">
            <div class="tab-pane" id="profile" role="tabpanel">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="white-box">
                            <div class="ms-auto p-2 bd-highlight">
                                <form action="#" class="d-flex" method="GET">
                                    @csrf
                                    <table>
                                        <tr>
                                            <td>
                                                <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                            </td>
                                            <td>
                                                <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                                                    <option value="0" selected disabled> Pilih Tahun</option>
                                                        <?php
                                                        $year = date('Y');
                                                        $min = $year - 60;
                                                        $max = $year;
                                                        for( $i=$max; $i>=$min; $i-- ) {
                                                        echo '<option value='.$i.'>'.$i.'</option>';
                                                        }?>
                                                </select>
                                            </td>
                                            <td>
                                                <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                                                    <option value="0" selected disabled> Pilih Bulan</option>
                                                    <option value="01"> Januari</option>
                                                    <option value="02"> Februari</option>
                                                    <option value="03"> Maret</option>
                                                    <option value="04"> April</option>
                                                    <option value="05"> Mei</option>
                                                    <option value="06"> Juni</option>
                                                    <option value="07"> Juli</option>
                                                    <option value="08"> Agustus</option>
                                                    <option value="09"> September</option>
                                                    <option value="10"> Oktober</option>
                                                    <option value="11"> November</option>
                                                    <option value="12"> Desember</option>
                                                </select>
                                            </td>
                                            <td>
                                                <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form><br>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                <script>
                                $(document).ready(function(){
                                $("#myInput").on("keyup", function() {
                                    var value = $(this).val().toLowerCase();
                                    $("#myTable tr").filter(function() {
                                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                    });
                                });
                                });
                                </script>
                            </div>
                            <h3>Daftar Peserta Event {{$data_event->nama}}</h3><br>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>No Hp</th>
                                                <th>Alamat</th>
                                                <th>No KTP</th>
                                                <th>Jenis Tiket</th>
                                                <th>Harga Tiket</th>
                                                <th>Status Transaksi</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody id="myTable">
                                        @foreach($data_eventtiket as $row)
                                                <tr>
                                                    <td>{{\Carbon\Carbon::parse($row->created_at)->format('d F Y')}}</td>
                                                    <td>{{$row->name}}</td>
                                                    <td>{{$row->email}}</td>
                                                    <td>{{$row->no_hp}}</td>
                                                    <td>{{$row->alamat}}</td>
                                                    <td>{{$row->no_ktp}}</td>
                                                    <td>{{$row->jenis}}</td>
                                                    <td>
                                                        @if($row->harga=="0")
                                                            Rp 0
                                                        @else
                                                            Rp {{number_format($row->harga,2,",",".")}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @foreach($data_transaksi as $transaksi)
                                                            @if($transaksi->id_eventtiket == $row->id)
                                                                @if($transaksi->status=="terima" && $transaksi->bukti_up!="")
                                                                    <span style="color: green;">SUKSES</span>
                                                                @elseif($transaksi->status=="tolak")
                                                                    <span style="color: red;">GAGAL</span>
                                                                @elseif($transaksi->status=="" && $transaksi->bukti_up!="")
                                                                    <span class="text-secondary">Diproses</span>
                                                                @elseif($transaksi->status=="" && $transaksi->bukti_up=="" && $transaksi->harga!="0" )
                                                                    <span class="text-secondary">Belum Melakukan Transaksi</span>
                                                                @elseif($transaksi->status=="terima" && $transaksi->bukti_up=="" && $transaksi->harga=="0" )
                                                                    <span style="color: green;">FREE</span>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                    @if($row->status!="" )
                                                        <div style="color: green; font-weight: 400;">Hadir</div>
                                                    @elseif($row->status==""&& date('Y-m-d') > $data_event->tanggal)
                                                        Tidak Hadir
                                                    @elseif($row->status == "" && date('Y-m-d') == $data_event->tanggal && date('H:i:sa') < $data_event->waktu_selesai || date('Y-m-d') < $data_event->tanggal)
                                                        Belum Hadir
                                                    @endif
                                            </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
        {{ $data_eventtiket -> appends(Request::all()) -> links() }}
        </div>

    </div>
</div>

@stop

