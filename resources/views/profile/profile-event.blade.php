@extends('layout.user')

@section('title', 'Events')

@section('content')
<div class="row">
<!-- Column -->
    <div class="col-lg-12 col-xlg-9 col-md-7">
        <ul class="list-inline" style="list-style: none;">
            <li style="display:inline;"><a href="/users/penyelenggara/create/{{$data_user->id}}"><button class="btn btn-info text-white btn-rounded hover-shadow-sm"><i class="mdi mdi-plus mr-2"></i><span>Create Event</span></button></a></li>
            <li style="display:inline;"><a href="/users/penyelenggara/{{$data_user->id}}"><button class="btn btn-info text-white btn-rounded ml-2 hover-shadow-sm"><i class="mdi mdi-account-multiple mr-2"></i><span>List Penyelenggara</span></button></a></li>
        </ul>
        @if ($message = Session::get('sukses'))
            <div class="alert alert-success alert-block">
            <a href="/users/events/{{$data_user->id}}"><button type="button" class="close" data-dismiss="alert">×</button></a>
            <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('gagal'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
            </div>
        @endif
        <!-- tabel -->
        <div class="card">
            <div class="tab-pane" id="profile" role="tabpanel">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="white-box">
                            <div class="ms-auto p-2 bd-highlight">
                                <form action="/users/events/{{$data_user->id}}" class="d-flex" method="GET">
                                    @csrf
                                    <table>
                                        <tr>
                                            <td>
                                                <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                            </td>
                                            <td>
                                                <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                                                    <option value="0" selected disabled> Pilih Tahun</option>
                                                        <?php
                                                        $year = date('Y');
                                                        $min = $year - 60;
                                                        $max = $year;
                                                        for( $i=$max; $i>=$min; $i-- ) {
                                                        echo '<option value='.$i.'>'.$i.'</option>';
                                                        }?>
                                                </select>
                                            </td>
                                            <td>
                                                <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                                                    <option value="0" selected disabled> Pilih Bulan</option>
                                                    <option value="01"> Januari</option>
                                                    <option value="02"> Februari</option>
                                                    <option value="03"> Maret</option>
                                                    <option value="04"> April</option>
                                                    <option value="05"> Mei</option>
                                                    <option value="06"> Juni</option>
                                                    <option value="07"> Juli</option>
                                                    <option value="08"> Agustus</option>
                                                    <option value="09"> September</option>
                                                    <option value="10"> Oktober</option>
                                                    <option value="11"> November</option>
                                                    <option value="12"> Desember</option>
                                                </select>
                                            </td>
                                            <td>
                                                <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form><br>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                <script>
                                $(document).ready(function(){
                                $("#myInput").on("keyup", function() {
                                    var value = $(this).val().toLowerCase();
                                    $("#myTable tr").filter(function() {
                                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                    });
                                });
                                });
                                </script>
                            </div>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Persetujuan</th>
                                                <th>Penyelenggara</th>
                                                <th>Jenis Event</th>
                                                <th>Foto</th>
                                                <th>Nama Event</th>
                                                <th>Tanggal</th>
                                                <th>Tempat</th>
                                                <th>Waktu Mulai</th>
                                                <th>Waktu Selesai</th>
                                                <th>Deskripsi</th>
                                                <th>Tags</th>
                                                <th>Status</th>
                                                <th>Link</th>
                                                <th>Kuota</th>
                                                <th>Peserta</th>
                                                <th>Tiket</th>
                                                <th>Option</th>
                                            </tr>
                                        </thead>
                                        <tbody id="myTable">
                                            @foreach($data_event as $row)
                                                <tr>
                                                    <td>@if($row->registered=="")
                                                        <p><b>Sedang diproses,</b><br>Periksa email Anda</p>
                                                        @elseif($row->registered=="terima")
                                                        <p style="color: green;"><b>Diterima</b></p>
                                                        @elseif($row->registered=="tolak")
                                                        <p class="text-danger"><b>Ditolak</b></p>
                                                        <p>Alasan : {{$row->komentar}}</p>
                                                        @endif
                                                    </td>
                                                    <td>{{$row->name_penyelenggara}}</td>
                                                    <td>{{$row->jenis_event}}</td>
                                                    <td> <img width="120px" src="/images/{{$row->foto}}" alt="image"></td>
                                                    <td>{{$row->nama}}</td>
                                                    <td>{{\Carbon\Carbon::parse($row->tanggal)->format('l, d F Y')}}</td>
                                                    <td>{{$row->tempat}}</td>
                                                    <td>{{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}}</td>
                                                    <td>{{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}}</td>
                                                    <td>{{$row->deskripsi}}</td>
                                                    <td>{{$row->tags}}</td>
                                                    <td>{{$row->status}}</td>
                                                    <td>{{$row->link}}</td>
                                                    <td>{{$row->kuota}}</td>
                                                    <td><a href="/users/event/peserta/{{$row->user_id}}/{{$row->id}}" class="btn btn-info btn-sm">Daftar Peserta</a></td>
                                                    <td>
                                                        <a href="/users/kategori/tiket/{{$row->user_id}}/{{$row->id}}" class="btn btn-info btn-sm">Daftar Tiket</a>
                                                    </td>
                                                    <td>
                                                        <!-- <a href="#modalHapus" data-toggle="modal" onclick="" class="btn btn-danger btn-sm">Delete</a><br><br> -->
                                                        @if($row->registered!="tolak")
                                                        <a href="/users/event/edit/{{$row->user_id}}/{{$row->id}}" class="btn btn-warning btn-sm btn-block">Edit</a>
                                                        @endif
                                                        <a href="/users/event/delete/{{$row->user_id}}/{{$row->id}}" class="btn btn-danger btn-sm btn-block">Delete</a>
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="modalHapus">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Yakin Menghapus Data?</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="/users/event/delete/{{$row->user_id}}/{{$row->id}}" class="float-right">
                                                                    <button class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                                                    <button class="btn btn-primary" type="submit">Ya</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
        {{ $data_event -> appends(Request::all()) -> links() }}
        </div>

    </div>
</div>

@stop

