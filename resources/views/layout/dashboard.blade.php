<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="@yield('tags')">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> 
	<meta property="og:site" content="" /> 
	<meta property="og:title" content=""/> 
	<meta property="og:description" content="" /> 
	<meta property="og:image" content="" /> 
	<meta property="og:url" content="" />
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>@yield('title')</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="icon" href={{ asset('/dashboard/images/event.ico') }}>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href={{ asset('/dashboard/css/bootstrap.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/fontawesome-all.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/swiper.css') }} rel="stylesheet">
	<link href={{ asset('/dashboard/css/magnific-popup.css') }} rel="stylesheet">
    <link href={{ asset('/dashboard/css/styles.css') }} rel="stylesheet">
    @yield('head')
</head>
<body data-spy="scroll" data-target=".fixed-top">

    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">

        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" >
                    <a class="nav-link page-scroll" href="/" style="color:#000000;">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle page-scroll" href="#contact" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:#000000;">About</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#contact"><span class="item-text">Contact</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                        <a class="dropdown-item" href="#sosial-media"><span class="item-text">Social Media</span></a>
                    </div>
                </li>
                <!-- end of dropdown menu -->
                <li class="nav-item page-scroll">
                    @if($request->session()->has('id'))
                        <a href="/users/{{$data_user->id}}">
                            <img class="rounded-circle ml-2" src={{ asset('images/'.$data_user->photo) }} alt="profile" style="width: 1.4rem;height: 1.4rem;position: absolute;object-fit: cover;">
                        </a>
                    @else
                        @php
                            echo $data_user;
                        @endphp
                    @endif
                </li>
            </ul>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->
    @yield('content')

    <!-- Footer -->
    <div class="footer" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>About EventLagi</h4>
                        <p>We're passionate about offering some of the best business growth services for startups</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col middle">
                        <h4>Important Links</h4>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Our business partners <a class="turquoise" href="https://cyberolympus.com" target="_blank">cyberolympus.com</a></div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Read our <a class="turquoise" href="#">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
                            </li>
                        </ul>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4" id="sosial-media">
                    <div class="footer-col last">
                        <h4>Social Media</h4>
                        <span class="fa-stack">
                            <a href="https://www.facebook.com/sharer/sharer.php?u=YourPageLink.com&display=popup">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="http://twitter.com/share?source=sharethiscom">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-google-plus-g fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </a>
                        </span>
                    </div>
                </div> 
            </div> 
        </div> 
    </div> 
    
    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © 2020 <a href="https://eventlagi.com" target="_blank">EventLagi</a> - All rights reserved</p>
                </div> 
            </div> 
        </div> 
    </div> 
    
    <!-- Scripts -->
    <script src={{ asset('/dashboard/js/jquery.min.js') }}></script> 
    <script src={{ asset('/dashboard/js/popper.min.js') }}></script> 
    <script src={{ asset('/dashboard/js/bootstrap.min.js') }}></script> 
    <script src={{ asset('/dashboard/js/jquery.easing.min.js') }}></script> 
    <script src={{ asset('/dashboard/js/swiper.min.js') }}></script> 
    <script src={{ asset('/dashboard/js/jquery.magnific-popup.js') }}></script> 
    <script src={{ asset('/dashboard/js/validator.min.js') }}></script> 
    <script src={{ asset('/dashboard/js/scripts.js') }}></script> 
</body>
</html>
