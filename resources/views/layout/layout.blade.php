<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 4 admin, bootstrap 4, css3 dashboard, bootstrap 4 dashboard, materialpro admin bootstrap 4 dashboard, frontend, responsive bootstrap 4 admin template, materialpro admin lite design, materialpro admin lite dashboard bootstrap 4 dashboard template">
    <meta name="description"
        content="Material Pro Lite is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>@yield('title')</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/materialpro-lite/" />
    <!-- Favicon icon -->
    <link rel="icon" sizes="16x16" href={{ asset('dashboard/images/event.ico') }}>
    <!-- chartist CSS -->
    <link href={{ asset('/plugins/style/chartist-js/dist/chartist.min.css') }} rel="stylesheet">
    <link href={{ asset('/plugins/style/chartist-js/dist/chartist-init.css') }} rel="stylesheet">
    <link href={{ asset('/plugins/style/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }} rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href={{ asset('/plugins/style/c3-master/c3.min.css') }} rel="stylesheet">
    <!-- Custom CSS -->
    <link href={{ asset("css/style.min.css")}} rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin6">
                    <a class="navbar-brand ml-5" href="/">
                        <!-- Logo icon -->
                        {{-- <b class="logo-icon">
                            <img src={{ asset('/plugins/images/logo-light-icon.png') }} alt="homepage" class="dark-logo" />

                        </b> --}}
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src={{ asset('/plugins/images/e-logo.png') }} alt="homepage"  class="dark-logo" style="height: 2.5rem;" />

                        </span>
                    </a>
                    <a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
                        href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </div>
                
            </nav>
        </header>

        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/dashboard" aria-expanded="false"><i class="mdi mr-2 mdi-gauge"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/penyelenggara" aria-expanded="false">
                                <i class="mdi mr-2 mdi-account-card-details"></i><span class="hide-menu">Penyelenggara</span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/event" aria-expanded="false"><i class="mdi mr-2 mdi-calendar-text"></i><span
                                    class="hide-menu">Event</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/tiket" aria-expanded="false"><i class="mdi mr-2 mdi-ticket-confirmation"></i><span class="hide-menu">Kategori Tiket</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/transaksi" aria-expanded="false"><i class="fas mr-2 fa-hand-holding-usd"></i><span
                                    class="hide-menu">Transaksi</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/rekening" aria-expanded="false"><i class="mdi mdi-credit-card mr-2"></i><span
                                    class="hide-menu">Rekening</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="/admin/peserta" aria-expanded="false"><i class="mdi mr-2 mdi-account-multiple"></i><span class="hide-menu">Peserta</span></a>
                        </li>
                        <li class="text-center p-10 upgrade-btn">
                            <a href="/logout" class="btn btn-block btn-danger text-white mt-4 hover-shadow" target="_selfs">LOGOUT</a>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <div class="sidebar-footer">
                <div class="row">
                    <div class="col-12 link-wrap">
                        <div style="height: 1.65rem;"></div>
                    </div>
                </div>
            </div>
        </aside>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="page-title mb-0 p-0">@yield('name')</h3>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <div class="text-right upgrade-btn">
                            @yield('button')
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                @yield('content')
            </div>
            <footer class="footer" style="text-align:center;"> © 2021 EventLagi - All rights reserved
            </footer>
        </div>
    </div>
    <script src={{asset('/plugins/style/jquery/dist/jquery.min.js') }}></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src={{asset('/plugins/style/popper.js/dist/umd/popper.min.js')}}></script>
    <script src={{asset('/plugins/style/bootstrap/dist/js/bootstrap.min.js')}}></script>
    <script src={{ asset('js/app-style-switcher.js') }}></script>
    <!--Wave Effects -->
    <script src={{asset("js/waves.js")}}></script>
    <!--Menu sidebar -->
    <script src={{asset('js/sidebarmenu.js')}}></script>

    <!-- chartist chart -->
    <script src={{asset('/plugins/style/chartist-js/dist/chartist.min.js')}}></script>
    <script src={{asset('/plugins/style/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}></script>
    <!--c3 JavaScript -->
    <script src={{asset('/plugins/style/d3/d3.min.js')}}></script>
    <script src={{asset('/plugins/style/c3-master/c3.min.js')}}></script>
    <!--Custom JavaScript -->
    <script src={{asset("js/pages/dashboards/dashboard1.js")}}></script>
    <script src={{asset("js/custom.js")}}></script>
</body>

</html>
