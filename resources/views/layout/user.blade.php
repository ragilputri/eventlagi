<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 4 admin, bootstrap 4, css3 dashboard, bootstrap 4 dashboard, materialpro admin bootstrap 4 dashboard, frontend, responsive bootstrap 4 admin template, materialpro admin lite design, materialpro admin lite dashboard bootstrap 4 dashboard template">
    <meta name="description"
        content="Material Pro Lite is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>@yield('title')</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/materialpro-lite/" />
    <!-- Favicon icon -->
    <link rel="icon" sizes="16x16" href={{ asset('dashboard/images/event.ico') }}>
    <!-- chartist CSS -->
    <link href={{ asset('plugins/style/chartist-js/dist/chartist.min.css') }} rel="stylesheet">
    <link href={{ asset('/plugins/style/chartist-js/dist/chartist-init.css') }} rel="stylesheet">
    <link href={{ asset('/plugins/style/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }} rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href={{ asset('/plugins/style/c3-master/c3.min.css') }} rel="stylesheet">
    <!-- Custom CSS -->
    <link href={{ asset("css/style.min.css")}} rel="stylesheet">
    @yield('head')
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin6">
                    <a class="navbar-brand ml-5" href="/">
                        <!-- Logo icon -->
                        {{-- <b class="logo-icon">
                            <img src={{ asset('/plugins/images/logo-light-icon.png') }} alt="homepage" class="dark-logo" />

                        </b> --}}
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src={{ asset('/plugins/images/e-logo.png') }} alt="homepage"  class="dark-logo" style="height: 2.5rem;" />

                        </span>
                    </a>
                    <a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
                        href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </div>
                
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <ul class="navbar-nav d-lg-none d-md-block ">
                        <li class="nav-item">
                            <a class="nav-toggler nav-link waves-effect waves-light text-white "
                                href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->

                        <li class="nav-item search-box">
                            <form class="app-search" style="display: none;">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a
                                    class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                    </ul>

                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link text-muted waves-effect waves-dark" href="/" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-store mr-1"></i><span><b>Home</b></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li class="text-center p-20">
                            <abbr title="Profile">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/users/profile/{{$data_user->id}}" aria-expanded="false">
                                    <img src="{{asset('images/'.$data_user->photo)}}" alt="Profile" style="border-radius: 100%; height: 9rem; width: 9rem; object-fit: cover;" class="mb-3">
                                </a>
                            </abbr>
                            <h4>Hello, {{strtok($data_user->name, ' ')}}!</h4>
                        </li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/users/{{$data_user->id}}" aria-expanded="false"><i class="mdi mr-2 mdi-gauge"></i>
                            <span class="hide-menu">Dashboard</span></a>
                        </li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/users/events/{{$data_user->id}}" aria-expanded="false">
                            <i class="mdi mr-2 mdi-calendar-multiple-check"></i><span class="hide-menu">Events</span></a>
                        </li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/users/tikets/{{$data_user->id}}" aria-expanded="false">
                            <i class="mdi mr-2 mdi-ticket-confirmation"></i><span class="hide-menu">Tickets</span></a>
                        </li>

                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/users/profile/{{$data_user->id}}" aria-expanded="false">
                            <i class="mdi mr-2 mdi-account"></i><span class="hide-menu">Profile</span></a>
                        </li>

                        <li class="text-center p-20 upgrade-btn">
                            <a href="/logout" class="btn btn-block btn-danger text-white waves-effect waves-dark hover-shadow" target="_blank">LOGOUT</a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <div class="sidebar-footer">
                <div class="row">
                    <div class="col-12 link-wrap">
                        <div style="height: 1.65rem;"></div>
                    </div>
                </div>
            </div>
        </aside>
        <div class="page-wrapper">

            <div >
                <div class="row">
                    <div class="col-md-12 col-4 align-self-center">
                        <div>
                            @yield('space')
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                @yield('content')
            </div>
            <footer class="footer" style="text-align:center;"> © 2021 EventLagi - All rights reserved
            </footer>
        </div>
    </div>

    <script src={{asset('/plugins/style/jquery/dist/jquery.min.js') }}></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src={{asset('/plugins/style/popper.js/dist/umd/popper.min.js')}}></script>
    <script src={{asset('/plugins/style/bootstrap/dist/js/bootstrap.min.js')}}></script>
    <script src={{ asset('js/app-style-switcher.js') }}></script>
    <!--Wave Effects -->
    <script src={{asset("js/waves.js")}}></script>
    <!--Menu sidebar -->
    <script src={{asset('js/sidebarmenu.js')}}></script>
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src={{asset('/plugins/style/chartist-js/dist/chartist.min.js')}}></script>
    <script src={{asset('/plugins/style/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}></script>
    <!--c3 JavaScript -->
    <script src={{asset('/plugins/style/d3/d3.min.js')}}></script>
    <script src={{asset('/plugins/style/c3-master/c3.min.js')}}></script>
    <!--Custom JavaScript -->
    <script src={{asset("js/pages/dashboards/dashboard1.js")}}></script>
    <script src={{asset("js/custom.js")}}></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>
