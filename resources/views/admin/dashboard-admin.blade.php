@extends('layout.layout')

@section('title', 'Admin Dashboard')

@section('name')
    <div style="color: grey;">Dashboard</div>
@stop

@section('content')

@if ($message = Session::get('sukses'))
	<div class="alert alert-success alert-block">
	<a href="/admin/dashboard-admin"><button type="button" class="close" data-dismiss="alert">×</button></a>
	<strong>{{ $message }}</strong>
	</div>
@endif

@if ($message = Session::get('gagal'))
	<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
	</div>
@endif

@foreach($data_event as $row)
<div class="card">
  <div class="card-header">
    Form Event
  </div>
  <div class="card-body">
    <img width="400px" src="{{ asset('images/'.$row->foto) }}" alt="image">
        <table class="table table-borderless">
            <tr>
                <td style="width: 5rem;">Nama</td>
                <td style="width: 2rem;">:</td>
                <td>{{$row->nama}}</td>
            </tr>
            <tr>
                <td>Penyelenggara</td>
                <td>:</td>
                <td>{{$row->name_penyelenggara}}</td>
            </tr>
            <tr>
                <td>Tiket</td>
                <td>:</td>
                <td>
                <a href="#tiket" button class="btn btn-info hover-shadow" data-toggle="modal" data-target="#tiket-{{$row->id}}" onclick="">Detail</a>
                </td>
            </tr>
            <tr>
                <td>Jenis</td>
                <td>:</td>
                <td>{{$row->jenis_event}}</td>
            </tr>
            <tr>
                <td>Tempat</td>
                <td>:</td>
                <td>{{$row->tempat}}</td>
            </tr>
            <tr>
                <td>Waktu</td>
                <td>:</td>
                <td>{{\Carbon\Carbon::parse($row->tanggal)->format('l, d F Y')}} | {{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}} - {{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{$row->status}}</td>
            </tr>
            <tr>
                <td>Deskripsi</td>
                <td>:</td>
                <td>{{$row->deskripsi}}</td>
            </tr>
            <tr>
                <td>Link</td>
                <td>:</td>
                <td>{{$row->link}}</td>
            </tr>
        </table>
        @if($row->registered=="")
        <a href="/admin/dashboard/terima/{{$row->id}}/{{$row->id_penyelenggara}}"><button type="submit" value="terima" name="btn"  class="btn btn-warning hover-shadow">Terima</button></a>
        <a href="#modalTolak" data-toggle="modal" data-target="#modalTolak-{{$row->id}}" class="popup-with-move-anim btn btn-danger hover-shadow">Tolak</a>
        @elseif($row->registered=="terima")
        <h4>Data Diterima</h4>
        @elseif($row->registered=="tolak")
        <h4>Data Ditolak</h4>
        @endif
  </div>
</div>
@endforeach
<div style="margin-left: 45%;">
    {{ $data_event -> appends(Request::all()) -> links() }}
</div>

@foreach($data_event as $event)
<div id="modalTolak-{{$event->id}}" class="modal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Komentar Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/dashboard/tolak/{{$event->id}}/{{$event->id_penyelenggara}}">
            @csrf
                <p style="font-size: 14pt; font-weight: 2rem;">
                  Mengapa data ini ditolak?
                </p>
                <p>
                    <textarea name="komentar" id="komentar" cols="30" rows="10" class="form-control form-control-line pl-3" placeholder="Masukkan komentar" required oninvalid="this.setCustomValidity('Komentar Wajib Diisi!')" oninput="setCustomValidity('')"></textarea>
                </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endforeach

@foreach($data_event as $tiket)
<div class="modal fade" id="tiket-{{$tiket->id}}">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Tiket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Jenis Tiket</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($data_kategori as $data)
                        @if($data->id_event == $tiket->id)
                            <tr>
                                <td>{{$data->jenis}}</td>
                                <td>
                                    @if($data->harga=="0")
                                        Rp 0
                                    @else
                                        Rp {{number_format($data->harga,2,",",".")}}
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer float-right" >
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@stop
