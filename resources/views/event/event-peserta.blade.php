@extends('layout.layout')

@section('title', 'Daftar Peserta')

@section('name')
    <div style="color: grey;">Daftar Peserta &nbsp; | &nbsp; <span style="color: orange; font-size: 12pt;">{{$data_event->nama}}</span></div>
@stop

@section('content')
<a href="/admin/event"><button class="btn btn-info text-white btn-rounded hover-shadow-sm"><i class="mdi mdi-arrow-left mr-2"></i><span>Kembali</span></button></a><br><br>
<div class="card">
    <div class="tab-pane" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-xs-6 b-r"> <strong>Kuota</strong>
                    <br>
                    <p class="text-muted">
                    {{$data_event->kuota}}
                    </p>
                </div>
                <div class="col-md-6 col-xs-6 b-r"> <strong>Total Peserta</strong>
                    <br>
                    <p class="text-muted">{{$count}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="tab-pane" id="profile" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box"><br>
                    <h3>Daftar Peserta Event {{$data_event->nama}}</h3><br>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>No Hp</th>
                                        <th>Alamat</th>
                                        <th>No KTP</th>
                                        <th>Jenis Tiket</th>
                                        <th>Harga Tiket</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data_eventtiket as $row)
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($row->created_at)->format('d F Y H:i:s')}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->no_hp}}</td>
                                            <td>{{$row->alamat}}</td>
                                            <td>{{$row->no_ktp}}</td>
                                            <td>{{$row->jenis}}</td>
                                            <td>
                                                @if($row->harga=="0")
                                                    Rp 0
                                                @else
                                                    Rp {{number_format($row->harga,2,",",".")}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->status=="terima" && $row->bukti_up!="")
                                                    <span style="color: green;">SUKSES</span>
                                                @elseif($row->status=="tolak")
                                                    <span style="color: red;">GAGAL</span>
                                                @elseif($row->status=="" && $row->bukti_up!="")
                                                    <span class="text-secondary">Diproses</span>
                                                @elseif($row->status=="" && $row->bukti_up=="" && $row->harga!="0" )
                                                    <span class="text-secondary">Belum Melakukan Transaksi</span>
                                                @elseif($row->status=="terima" && $row->bukti_up=="" && $row->harga=="0" )
                                                    <span style="color: green;">FREE</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data_eventtiket -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>

@stop
