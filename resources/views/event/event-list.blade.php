@extends('layout.layout')

@section('title', 'Daftar Event')

@section('name')
    <div style="color: grey;">Event</div>
@stop

@section('content')

<style>
.breadcrumbs{padding:10px 20px;font-family: 'Roboto', sans-serif;font-size:11px;color:#333;background:#f8f8f8;border-radius:3px 3px 0 0;box-shadow: 1px 3px 5px 0 rgba(0,0,0,0.1);}
.breadcrumbs a{color:#333}
#bread-crumbs {overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
.postmeta{color:#eee;}
.postmeta .post_meta{margin-left:47px}
.postmeta .imageurlpost{position:absolute;}
.postmeta .imageurlpost amp-img{border-radius:50%; bottom:10px;border:3px solid #56cffe;transition: all .3s}
.postmeta .imageurlpost amp-img:hover{transition: all .3s;transform: rotate(720deg)}
.postmeta a{color:#fff;padding:5px 10px 5px 10px;margin-right:5px;font-family:'Roboto', sans-serif;font-size:12px;font-weight:400}
.postmeta .fn a{background:rgba(255, 105, 105, 0.68);border-radius: 3px;}
.postmeta .clock a{background:rgba(105, 158, 255, 0.69);border-radius: 3px;}
.post-labels {font-family:'Roboto', sans-serif;display: block;margin: 40px 5px 20px;}
.post-labels a {padding: 3px 5px;border-radius: 5px;margin-right: 7px;color: #fff;font-size: 12px;}
.post-labels a:nth-child(1){background:#ff9244;margin-left: -5px;}
.post-labels a:nth-child(2){background:#25d366;margin-left: -5px;}
.post-labels a:nth-child(3){background:#448aff;margin-left: -5px;}
.post-labels a:nth-child(4){background:#d325be;margin-left: -5px;}
.post-labels a:nth-child(n+5){display:none}
.post .info {background: #fafafa;position:relative;padding: 10px;border-left: 5px solid #3572b1;margin-bottom: 10px;padding-left: 55px;}
.post .info:before {content: "\f06a";color: #00bf8f;position: absolute;left: 10px;font-family: fontawesome;font-size: 40px;top: -15px;}
</style>

@if ($message = Session::get('sukses'))
	<div class="alert alert-success alert-block">
	<a href="/admin/event"><button type="button" class="close" data-dismiss="alert">×</button></a>
	<strong>{{ $message }}</strong>
	</div>
@endif

@if ($message = Session::get('gagal'))
	<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
	</div>
@endif
<div class="card">
    <div class="tab-pane" id="profile" role="tabpanel">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <div class="ms-auto p-2 bd-highlight">
                                <form action="/admin/event" class="d-flex" method="GET">
                                    @csrf
                                    <table>
                                        <tr>
                                            <td>
                                                <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                            </td>
                                            <td>
                                                <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                                                    <option value="0" selected disabled> Pilih Tahun</option>
                                                        <?php
                                                        $year = date('Y');
                                                        $min = $year - 60;
                                                        $max = $year;
                                                        for( $i=$max; $i>=$min; $i-- ) {
                                                        echo '<option value='.$i.'>'.$i.'</option>';
                                                        }?>
                                                </select>
                                            </td>
                                            <td>
                                                <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                                                    <option value="0" selected disabled> Pilih Bulan</option>
                                                    <option value="01"> Januari</option>
                                                    <option value="02"> Februari</option>
                                                    <option value="03"> Maret</option>
                                                    <option value="04"> April</option>
                                                    <option value="05"> Mei</option>
                                                    <option value="06"> Juni</option>
                                                    <option value="07"> Juli</option>
                                                    <option value="08"> Agustus</option>
                                                    <option value="09"> September</option>
                                                    <option value="10"> Oktober</option>
                                                    <option value="11"> November</option>
                                                    <option value="12"> Desember</option>
                                                </select>
                                            </td>
                                            <td>
                                                <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form><br>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                <script>
                                $(document).ready(function(){
                                $("#myInput").on("keyup", function() {
                                    var value = $(this).val().toLowerCase();
                                    $("#myTable tr").filter(function() {
                                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                    });
                                });
                                });
                                </script>
                            </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Persetujuan</th>
                                        <th>Penyelenggara</th>
                                        <th>Jenis Event</th>
                                        <th>Foto</th>
                                        <th>Nama Event</th>
                                        <th>Tanggal</th>
                                        <th>Tempat</th>
                                        <th>Waktu Mulai</th>
                                        <th>Waktu Selesai</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Link</th>
                                        <th>Kuota</th>
                                        <th>Peserta</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data_event as $row)
                                    <tr>
                                        <td>
                                            @if($row->registered=="terima")
                                                <div style="color: green;">Diterima</div>
                                            @elseif($row->registered=="tolak")
                                                <div style="color: red;">Ditolak</div>
                                                <p>Alasan : {{$row->komentar}}</p>
                                            @else
                                                Sedang diproses
                                            @endif
                                        </td>
                                        <td>{{$row->name_penyelenggara}}</td>
                                        <td>{{$row->jenis_event}}</td>
                                        <td> <img width="120px" src="/images/{{$row->foto}}" alt="image"></td>
                                        <td>{{$row->nama}}</td>
                                        <td>{{\Carbon\Carbon::parse($row->tanggal)->format('l, d F Y')}}</td>
                                        <td>{{$row->tempat}}</td>
                                        <td>{{\Carbon\Carbon::parse($row->waktu_mulai)->format('H:i')}}</td>
                                        <td>{{\Carbon\Carbon::parse($row->waktu_selesai)->format('H:i')}}</td>
                                        <td>{{$row->deskripsi}}</td>
                                        <td>{{$row->status}}</td>
                                        <td>{{$row->link}}</td>
                                        <td>{{$row->kuota}}</td>
                                        <td><a href="/admin/event/peserta/{{$row->id}}" class="btn btn-primary btn-sm">Daftar Peserta</a></td>
                                        <td>
                                            <a href="/admin/event/edit/{{$row->id}}" class="btn btn-warning btn-sm btn-block">Edit</a>
                                            <a href="/admin/event/delete/{{$row->id}}" class="btn btn-danger btn-sm btn-block">Remove</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data_event -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>
@stop
