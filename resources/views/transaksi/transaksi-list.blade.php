@extends('layout.layout')

@section('title', 'Daftar Transaksi')

@section('name')
    <div style="color: grey;">Transaksi</div>
@stop

@section('content')

@if ($message = Session::get('sukses'))
	<div class="alert alert-success alert-block">
	<a href="/admin/transaksi"><button type="button" class="close" data-dismiss="alert">×</button></a>
	<strong>{{ $message }}</strong>
	</div>
@endif

@if ($message = Session::get('gagal'))
	<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
	</div>
@endif

<!-- <ul class="list-inline" style="list-style: none;">
    <li style="display:inline;"><a href="/admin/transaksi/sukses"><button class="btn btn-success text-white">Transaksi Sukses</button></a></li>
    <li style="display:inline;"><a href="/admin/transaksi/belum"><button class="btn btn-success text-white">Transaksi Belum Dibayar</button></a></li>
</ul> -->
<div class="card">
    <div class="card-body">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="ms-auto p-2 bd-highlight">
                    <form action="/admin/transaksi" class="d-flex" method="GET">
                        @csrf
                        <table>
                            <tr>
                                <td>
                                    <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                </td>
                                <td>
                                    <select name="status" class="form-control" id="status">
                                        <option value="0" selected disabled>Status berdasarkan</option>
                                        <option value="terima">Terima</option>
                                        <option value="tolak">Tolak</option>
                                    </select>
                                </td>
                                <td>
                                    <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                                        <option value="0" selected disabled> Pilih Tahun</option>
                                            <?php
                                            $year = date('Y');
                                            $min = $year - 60;
                                            $max = $year;
                                            for( $i=$max; $i>=$min; $i-- ) {
                                            echo '<option value='.$i.'>'.$i.'</option>';
                                            }?>
                                    </select>
                                </td>
                                <td>
                                    <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                                        <option value="0" selected disabled> Pilih Bulan</option>
                                        <option value="01"> Januari</option>
                                        <option value="02"> Februari</option>
                                        <option value="03"> Maret</option>
                                        <option value="04"> April</option>
                                        <option value="05"> Mei</option>
                                        <option value="06"> Juni</option>
                                        <option value="07"> Juli</option>
                                        <option value="08"> Agustus</option>
                                        <option value="09"> September</option>
                                        <option value="10"> Oktober</option>
                                        <option value="11"> November</option>
                                        <option value="12"> Desember</option>
                                    </select>
                                </td>
                                <td>
                                    <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                </td>
                            </tr>
                        </table>
                    </form><br>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    <script>
                    $(document).ready(function(){
                    $("#myInput").on("keyup", function() {
                        var value = $(this).val().toLowerCase();
                        $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                    });
                    </script>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Waktu</th>
                                <th>Event</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No HP</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Bukti Transaksi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($data as $row)
                            <tr>
                                <td>{{\Carbon\Carbon::parse($row->waktu)->format('d F Y H:i:s')}}</td>
                                <td>{{$row->nama}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->email}}</td>
                                <td>{{$row->no_hp}}</td>
                                <td>
                                    @if($row->harga==0)
                                        Rp 0
                                    @else
                                        Rp {{number_format($row->harga,2,",",".")}}
                                    @endif
                                </td>
                                <td>
                                    @if($row->total==0)
                                        Rp 0
                                    @else
                                        Rp {{number_format($row->total,2,",",".")}}
                                    @endif
                                </td>
                                <td>
                                    @if($row->bukti_up != "")
                                        <img src="/images/{{$row->bukti_up}}" alt="image" width="200">
                                    @endif
                                    @if($row->harga=="0")
                                        <center>-</center>
                                    @endif
                                </td>
                                <td>
                                    @if($row->status=="terima" && $row->bukti_up!="")
                                        <span style="color: green;">SUKSES</span>
                                    @elseif($row->status=="tolak")
                                        <span style="color: red;">GAGAL</span>
                                    @elseif($row->status=="" && $row->bukti_up!="")
                                        <span class="text-secondary">Diproses</span>
                                    @elseif($row->status=="" && $row->bukti_up=="" && $row->harga!="0" )
                                        <span class="text-secondary">Belum Melakukan Transaksi</span>
                                    @elseif($row->status=="terima" && $row->bukti_up=="" && $row->harga=="0" )
                                        <span style="color: green;">FREE</span>
                                    @endif
                                </td>
                                <td>
                                    @if($row->harga!="0" && $row->bukti_up!="")
                                        <a href="#detail" button class="btn btn-info hover-shadow" data-toggle="modal" data-target="#detail-{{$row->id}}" onclick="">Detail</a>
                                    @endif
                                    <!-- <a href="transaksi/edit/{{$row->id}}" class="btn btn-warning btn-sm">Edit Data</a>
                                    <a href="transaksi/delete/{{$row->id}}" class="btn btn-danger btn-sm">Remove Data</a> -->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data -> appends(Request::all()) -> links() }}
        </div>
    </div>
    </div>
</div>
@foreach($data as $dt)
<!-- Modal -->
<div class="modal fade" id="detail-{{$dt->id}}">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Transaksi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="/admin/transaksi-terima/{{$dt->id}}/{{$dt->id_eventtiket}}">
            @csrf
                <table cellpadding="3">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{$dt->name}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{{$dt->email}}</td>
                    </tr>
                </table>
                <hr>
                <h5>{{$dt->nama}}</h5>
                <p>Rp {{number_format($dt->harga,2,",",".")}}</p>
                <center>
                    <img src="/images/{{$dt->bukti_up}}" alt="image" width="250">
                </center>
            </div>
            <div class="modal-footer float-right" >
                    <a href="" button class="btn btn-danger hover-shadow-sm" data-toggle="modal" data-target="#tolak-{{$dt->id}}" onclick="">Tolak</a>
                    <button class="btn btn-success text-white hover-shadow-sm" type="submit">Terima</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@foreach($data as $dr)
<!-- Modal Tolak-->
<div class="modal fade" id="tolak-{{$dr->id}}">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Komentar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/admin/transaksi-tolak/{{$dr->id}}" class="float-right">
            <div class="modal-body">
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">Recipient:</label>
                        <input type="text" class="form-control" id="recipient-name" value="{{$dr->email}}" disabled>
                    </div>
                    <div class="mb-3">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea class="form-control" id="message-text" name="komentar" rows="3"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Kirim</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@stop


