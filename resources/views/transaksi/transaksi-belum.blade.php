@extends('layout.layout')

@section('title', 'Daftar Transaksi Belum Bayar')

@section('button')
<a href="/admin/transaksi/create" class="btn btn-lg btn-primary" tabindex="-1" role="button"><b style="color: white;">Tambah Transaksi</b></a>
<!-- <a href="penyelenggara/create" class="btn btn-danger d-none d-md-inline-block text-white">Registrasi Penyelenggara</a> -->
@stop

@section('content')


<ul class="list-inline" style="list-style: none;">
    <li style="display:inline;"><a href="/admin/transaksi/sukses"><button class="btn btn-success text-white">Transaksi Sukses</button></a></li>
    <li style="display:inline;"><a href="/admin/transaksi"><button class="btn btn-success text-white">Kembali</button></a></li>
</ul>
<div class="card">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="ms-auto p-2 bd-highlight">
                    <form action="/admin/transaksi/belum" class="d-flex" method="GET">
                        @csrf
                        <table>
                            <tr>
                                <td>
                                    <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                </td>
                                <td>
                                    <select style="cursor:pointer;" class="form-control" id="tag_select" name="year">
                                        <option value="0" selected disabled> Pilih Tahun</option>
                                            <?php
                                            $year = date('Y');
                                            $min = $year - 60;
                                            $max = $year;
                                            for( $i=$max; $i>=$min; $i-- ) {
                                            echo '<option value='.$i.'>'.$i.'</option>';
                                            }?>
                                    </select>
                                </td>
                                <td>
                                    <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control" id="tag_select" name="month">
                                        <option value="0" selected disabled> Pilih Bulan</option>
                                        <option value="01"> Januari</option>
                                        <option value="02"> Februari</option>
                                        <option value="03"> Maret</option>
                                        <option value="04"> April</option>
                                        <option value="05"> Mei</option>
                                        <option value="06"> Juni</option>
                                        <option value="07"> Juli</option>
                                        <option value="08"> Agustus</option>
                                        <option value="09"> September</option>
                                        <option value="10"> Oktober</option>
                                        <option value="11"> November</option>
                                        <option value="12"> Desember</option>
                                    </select>
                                </td>
                                <td>
                                    <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                </td>
                            </tr>
                        </table>
                    </form><br>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                    <script>
                    $(document).ready(function(){
                    $("#myInput").on("keyup", function() {
                        var value = $(this).val().toLowerCase();
                        $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                    });
                    </script>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Waktu</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No HP</th>
                                <th>Harga</th>
                                <th>Total</th>
                                <th>Bukti Transaksi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($data_transaksi as $row)
                            <tr>
                                <td>{{$row->waktu}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->email}}</td>
                                <td>{{$row->no_hp}}</td>
                                <td>{{$row->harga}}</td>
                                <td>{{$row->total}}</td>
                                <td>
                                <img src="/images/{{$row->bukti_up}}" alt="image">
                                </td>
                                <td>
                                @if($row->status=="")
                                    <span class="text-secondary">Belum melakukan transaksi</span>
                                @endif
                                
                                </td>
                                <td>
                                    <a href="transaksi/edit/{{$row->id}}" class="btn btn-warning btn-sm">Edit Data</a>
                                    <a href="transaksi/delete/{{$row->id}}" class="btn btn-danger btn-sm">Remove Data</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
            {{ $data_transaksi -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>
@stop


