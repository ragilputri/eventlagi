@extends('layout.user')

@section('title', 'Edit Rekening')

@section('content')
<h3>Form Rekening</h3><br>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

			<form action="/users/rekening/update/{{$data_user->id}}/{{$data_rekening->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                <label>Nama Bank</label>
                <select class="form-control" aria-label=".form-select-sm example" name="bank">
                    <option selected disabled></option>
                    <option value="bank indonesia" @if($data_rekening->bank=='bank indonesia') selected @endif>Bank Indonesia (Bank Sentral)</option>
                    <option value="bni" @if($data_rekening->bank=='bni') selected @endif>Bank Negara Indonesia</option>
                    <option value="bri" @if($data_rekening->bank=='bri') selected @endif>Bank Rakyat Indonesia</option>
                    <option value="bank tabungan negara" @if($data_rekening->bank=='bank tabungan negara') selected @endif>Bank Tabungan Negara</option>
                    <option value="bank mandiri" @if($data_rekening->bank=='bank mandiri') selected @endif>Bank Mandiri</option>
                    <option value="bank agroniaga" @if($data_rekening->bank=='bank agroniaga') selected @endif>Bank Agroniaga</option>
                    <option value="bank antardaerah" @if($data_rekening->bank=='bank antardaerah') selected @endif>Bank Antardaerah</option>
                    <option value="bank artha graha" @if($data_rekening->bank=='bank artha graha') selected @endif>Bank Artha Graha Internasional</option>
                    <option value="bank bni syariah" @if($data_rekening->bank=='bank bni syariah') selected @endif>Bank BNI Syariah</option>
                    <option value="bank bukopin" @if($data_rekening->bank=='bank bukopin') selected @endif>Bank Bukopin</option>
                    <option value="bank bumi arta" @if($data_rekening->bank=='bank bumi arta') selected @endif>Bank Bumi Arta</option>
                    <option value="bca" @if($data_rekening->bank=='bca') selected @endif>Bank Central Asia (BCA)</option>
                    <option value="bank cimb niaga" @if($data_rekening->bank=='bank cimb niaga') selected @endif>Bank Cimb Niaga</option>
                    <option value="bank danamon indonesia" @if($data_rekening->bank=='bank danamon indonesia') selected @endif>Bank Danamon Indonesia</option>
                    <option value="bank ekonomi raharja" @if($data_rekening->bank=='bank ekonomi raharja') selected @endif>Bank Ekonomi Raharja</option>
                    <option value="bank ganesha" @if($data_rekening->bank=='bank ganesha') selected @endif>Bank Ganesha</option>
                    <option value="bank hana" @if($data_rekening->bank=='bank hana') selected @endif>Bank Hana</option>
                    <option value="bank himpunan saudara" @if($data_rekening->bank=='bank himpunan saudara') selected @endif>Bank Himpunan Saudara 1906</option>
                    <option value="bank icb bumiputera" @if($data_rekening->bank=='bank icb bumiputera') selected @endif>Bank ICB Bumiputera</option>
                    <option value="bank icbc indonesia" @if($data_rekening->bank=='bank icbc indonesia') selected @endif>Bank ICBC Indonesia</option>
                    <option value="bank index selindo" @if($data_rekening->bank=='bank index selindo') selected @endif>Bank Index Selindo</option>
                    <option value="bank internasional indonesia" @if($data_rekening->bank=='bank internasional indonesia') selected @endif>Bank Internasional Indonesia</option>
                    <option value="bank maspion indonesia" @if($data_rekening->bank=='bank maspion indonesia') selected @endif>Bank Maspion Indonesia</option>
                    <option value="bank mayapada international" @if($data_rekening->bank=='bank mayapada international') selected @endif>Bank Mayapada International</option>
                    <option value="bank mega" @if($data_rekening->bank=='bank mega') selected @endif>Bank Mega</option>
                    <option value="bank mestika dharma" @if($data_rekening->bank=='bank mestika dharma') selected @endif>Bank Mestika Dharma</option>
                    <option value="bank metro express" @if($data_rekening->bank=='bank metro express') selected @endif>Bank Metro Express</option>
                    <option value="bank muamalat indonesia" @if($data_rekening->bank=='bank muamalat indonesia') selected @endif>Bank Muamalat Indonesia</option>
                    <option value="bank mutiara" @if($data_rekening->bank=='bank mutiara') selected @endif>Bank Mutiara</option>
                    <option value="bank nusantara parahyangan" @if($data_rekening->bank=='bank nusantara parahyangan') selected @endif>Bank Nusantara Parahyangan</option>
                    <option value="bank ocbc nisp" @if($data_rekening->bank=='bank ocbc nisp') selected @endif>Bank OCBC NISP</option>
                    <option value="bank of india indonesia" @if($data_rekening->bank=='bank of india indonesia') selected @endif>Bank Of India Indonesia</option>
                    <option value="bank permata" @if($data_rekening->bank=='bank permata') selected @endif>Bank Permata</option>
                    <option value="bank sbi indonesia" @if($data_rekening->bank=='bank sbi indonesia') selected @endif>Bank SBI Indonesia</option>
                    <option value="bank sinarmas" @if($data_rekening->bank=='bank sinarmas') selected @endif>Bank Sinarmas</option>
                    <option value="bank syariah mandiri" @if($data_rekening->bank=='bank syariah mandiri') selected @endif>Bank Syariah Mandiri</option>
                    <option value="bank syariah mega indonesia" @if($data_rekening->bank=='bank syariah mega indonesia') selected @endif>Bank Syariah Mega Indonesia</option>
                    <option value="bank uob indonesia" @if($data_rekening->bank=='bank uob indonesia') selected @endif>Bank UOB Indonesia</option>
                    <option value="pan bank indonesia" @if($data_rekening->bank=='pan bank indonesia') selected @endif>Pan Indonesia Bank</option>
                    <option value="qnb bank kesawan" @if($data_rekening->bank=='qnb bank kesawan') selected @endif>QNB Bank Kesawan</option>
                </select><br>
                </p>
                <p>
                    <label>Rekening Atas Nama</label><br>
                    <input type="text" name="atas_nama" class="form-control" value="{{$data_rekening->atas_nama}}"><br>
                </p>
                <p>
                    <label>Nomor Rekening</label><br>
                    <input type="text" name="no_rek" class="form-control" value="{{$data_rekening->no_rek}}"><br>
                </p>
                <p>
                    <input type="submit" name="tombol_add" value="Simpan" class="btn btn-primary">
                </p>
            </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
