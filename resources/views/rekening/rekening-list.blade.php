@extends('layout.user')

@section('title', 'Rekening')

@section('content')
<div class="row">
<!-- Column -->
    <div class="col-lg-12 col-xlg-9 col-md-7">
        <ul class="list-inline" style="list-style: none;">
            <li style="display:inline;"><a href="/users/kategori/tiket/{{$data_user->id}}/{{$data_event->id}}"><button class="btn btn-info text-white btn-rounded hover-shadow-sm"><i class="mdi mdi-arrow-left mr-2"></i><span>Kembali</span></button></a></li>
                <li style="display:inline;"><a href="/users/rekening/create/{{$data_user->id}}/{{$data_event->id}}"><button class="btn btn-purple text-white btn-rounded hover-shadow-sm ml-2"><i class="mdi mdi-credit-card-plus mr-2"></i><span>Tambah Rekening</span></button></a></li>
        </ul>
        @if ($message = Session::get('sukses'))
            <div class="alert alert-success alert-block">
            <a href="/users/events/{{$data_user->id}}"><button type="button" class="close" data-dismiss="alert">×</button></a>
            <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('gagal'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
            </div>
        @endif
        <!-- tabel -->
        <div class="card">
            <div class="tab-pane" id="profile" role="tabpanel">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="white-box">
                            <div class="ms-auto p-2 bd-highlight">
                                <form action="#" class="d-flex" method="GET">
                                    @csrf
                                    <table>
                                        <tr>
                                            <td>
                                                <input class="form-control me-2" type="search" id="myInput" name="search" placeholder="Search..." aria-label="Search" style="width:250px;">
                                            </td>
                                            <td>
                                                <button type="submit" value="1" name="btn"  class="btn btn-outline-success">Search</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form><br>
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                <script>
                                $(document).ready(function(){
                                $("#myInput").on("keyup", function() {
                                    var value = $(this).val().toLowerCase();
                                    $("#myTable tr").filter(function() {
                                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                    });
                                });
                                });
                                </script>
                            </div>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Bank</th>
                                                <th>No Rekening</th>
                                                <th>Atas Nama</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="myTable">
                                        @foreach($data_rekening as $row)
                                                <tr>
                                                    <td>{{$row->bank}}</td>
                                                    <td>{{$row->no_rek}}</td>
                                                    <td>{{$row->atas_nama}}</td>
                                                    <td>
                                                        <!-- <a href="#modalHapus" data-toggle="modal" onclick="" class="btn btn-danger btn-sm">Delete</a><br><br> -->
                                                        <a href="{{url('users/rekening/edit/'.$row->id_user.'/'.$row->id)}}" class="btn btn-primary btn-sm">Edit</a>
                                                        <a href="{{url('users/rekening/delete/'.$row->id_user.'/'.$row->id)}}" class="btn btn-danger btn-sm">Delete</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left: 45%;">
        {{ $data_rekening -> appends(Request::all()) -> links() }}
        </div>
    </div>
</div>

@stop

