@extends('layout.user')

@section('title', 'Create Rekening')

@section('content')
<h3>Form Rekening</h3><br>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

			<form action="/users/rekening/save/{{$data_user->id}}/{{$data_event->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                <label>Nama Bank</label>
                <select class="form-control" aria-label=".form-select-sm example" name="bank">
                    <option selected disabled></option>
                    <option value="bank indonesia">Bank Indonesia (Bank Sentral)</option>
                    <option value="bni">Bank Negara Indonesia</option>
                    <option value="bri">Bank Rakyat Indonesia</option>
                    <option value="bank tabungan negara">Bank Tabungan Negara</option>
                    <option value="bank mandiri">Bank Mandiri</option>
                    <option value="bank agroniaga">Bank Agroniaga</option>
                    <option value="bank antardaerah">Bank Antardaerah</option>
                    <option value="bank artha graha">Bank Artha Graha Internasional</option>
                    <option value="bank bni syariah">Bank BNI Syariah</option>
                    <option value="bank bukopin">Bank Bukopin</option>
                    <option value="bank bumi arta">Bank Bumi Arta</option>
                    <option value="bca">Bank Central Asia (BCA)</option>
                    <option value="bank cimb niaga">Bank Cimb Niaga</option>
                    <option value="bank danamon indonesia">Bank Danamon Indonesia</option>
                    <option value="bank ekonomi raharja">Bank Ekonomi Raharja</option>
                    <option value="bank ganesha">Bank Ganesha</option>
                    <option value="bank hana">Bank Hana</option>
                    <option value="bank himpunan saudara">Bank Himpunan Saudara 1906</option>
                    <option value="bank icb bumiputera">Bank ICB Bumiputera</option>
                    <option value="bank icbc indonesia">Bank ICBC Indonesia</option>
                    <option value="bank index selindo">Bank Index Selindo</option>
                    <option value="bank internasional indonesia">Bank Internasional Indonesia</option>
                    <option value="bank maspion indonesia">Bank Maspion Indonesia</option>
                    <option value="bank mayapada international">Bank Mayapada International</option>
                    <option value="bank mega">Bank Mega</option>
                    <option value="bank mestika dharma">Bank Mestika Dharma</option>
                    <option value="bank metro express">Bank Metro Express</option>
                    <option value="bank muamalat indonesia">Bank Muamalat Indonesia</option>
                    <option value="bank mutiara">Bank Mutiara</option>
                    <option value="bank nusantara parahyangan">Bank Nusantara Parahyangan</option>
                    <option value="bank ocbc nisp">Bank OCBC NISP</option>
                    <option value="bank of india indonesia">Bank Of India Indonesia</option>
                    <option value="bank permata">Bank Permata</option>
                    <option value="bank sbi indonesia">Bank SBI Indonesia</option>
                    <option value="bank sinarmas">Bank Sinarmas</option>
                    <option value="bank syariah mandiri">Bank Syariah Mandiri</option>
                    <option value="bank syariah mega indonesia">Bank Syariah Mega Indonesia</option>
                    <option value="bank uob indonesia">Bank UOB Indonesia</option>
                    <option value="pan bank indonesia">Pan Indonesia Bank</option>
                    <option value="qnb bank kesawan">QNB Bank Kesawan</option>
                </select><br>
                </p>
                <p>
                    <label>Rekening Atas Nama</label><br>
                    <input type="text" name="atas_nama" class="form-control"><br>
                </p>
                <p>
                    <label>Nomor Rekening</label><br>
                    <input type="text" name="no_rek" class="form-control"><br>
                </p>
                <p>
                    <input type="submit" name="tombol_add" value="Simpan" class="btn btn-primary">
                </p>
            </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
