@extends('layout.layout')

@section('name', 'Edit Ticket')

@section('title', 'Edit Kategori Tiket')

@section('content')

			<form action="/admin/tiket/update/{{$data_tiket->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <p>
                    <label>Jenis</label><br>
                    <input type="text" name="jenis" class="form-control" value="{{$data_tiket->jenis}}"><br>
                </p>
                <p>
                    <label>Harga</label><br>
                    <input type="text" name="harga" class="form-control" value="{{$data_tiket->harga}}"><br>
                </p>
                <p>
                    <label>Deskripsi</label><br>
                    <textarea type="text" name="deskripsi" class="form-control" rows="3">{{$data_tiket->deskripsi}}</textarea><br>
                </p>
                <p>
                    <input type="submit" name="tombol-add" value="Update" class="btn btn-primary">
                </p>
            </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
@stop
