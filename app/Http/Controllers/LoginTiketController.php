<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Event;

class LoginTiketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cek(Request $request, $id) {
        if($request->session()->has('id')) {
            $data_event = Event::find($id);
            $validate_admin = User::find($request->session()->get('id'));

            return redirect(url('tiket-registrasi/'.$data_event->id."/".$validate_admin->id))
                            ->with('data_event', $data_event)
                            ->with('validate_admin', $validate_admin);
        } else {
            return redirect(url('login-tiket/'.$id));
        }
    }
    
    public function index($id)
    {
            $data_event = Event::find($id);
            return view('navbar.login-tiket')
                    ->with('data_event', $data_event);
    }


    public function store(Request $request, $id)
    {
        $data_event=Event::find($id);
        $validate_admin = User::where('email', $request->input('email'))
                                ->first();

        if($validate_admin){

            if(Hash::check($request->input('password'), $validate_admin->password)){
                Auth::loginUsingId($validate_admin->id);
                $request->session()->put('id', $validate_admin->id);
                if($validate_admin->account_status  == '2'){
                    return redirect(url('tiket-registrasi/'.$data_event->id."/".$validate_admin->id))
                            ->with('data_event', $data_event)
                            ->with('validate_admin', $validate_admin);
                }
            } else{
                Session::flash('gagal','Oppsss, Email atau Password salah');
                return redirect(url('login-tiket/'.$data_event->id));
            }
        }
        else{
            Session::flash('gagal','Oppsss, Email atau Password salah');
            return redirect(url('login-tiket/'.$data_event->id));
        }
    }
}
