<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\Mail\EmailNotification;
use Illuminate\Support\Facades\Mail;
use App\User;
use Session;

class RegisterTiketController extends Controller
{
    public function show($id){
        $data_event=Event::find($id);
        return view('navbar.registrasi-tiket')
        ->with('data_event', $data_event);
    }

    public function save(Request $request, $id){
            $password = $request->input('password');
            $pass = bcrypt($password);
            $data_event = Event::find($id);

            $this->validate($request, [
                'name' => 'required|min:3|max:50',
                'email' => 'email',
                'password' => 'min:6',
                'confirm_password' => 'required_with:password|same:password|min:6',
            ]);
            
            $data_user = User::create([
                "name" => $request -> input("name"),
                "email" => $request -> input("email"),
                "password" => $pass,
                "account_status" => 2,
                "photo" => "default-profile.png",
            ]);
            Mail::to($data_user->email)->send(new EmailNotification($data_user));

            Session::flash('sukses','Berhasil membuat akun. Login Kembali!');
            return redirect(url("login-tiket/".$data_event->id))
                    ->with('data_user', $data_user)
                    ->with('data_event', $data_event);
    }
}
