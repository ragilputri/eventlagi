<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\AdminNotification;
use App\Mail\AdminNotification2;
use Illuminate\Support\Facades\Mail;
use App\Event;
use App\Penyelenggara;
use App\Komentar;
use App\KategoriTiket;
use Session;
class AdminController extends Controller
{
    function index(){
        $data_penyelenggara=Penyelenggara::all();
        $data_kategori=KategoriTiket::get();
        $data_event=DB::table('event')
        ->leftjoin('penyelenggara','penyelenggara.id','=','event.id_penyelenggara')
        ->select('penyelenggara.*','event.*')
        ->orderBy('event.id','desc')
        ->paginate(5);
        return view('admin.dashboard-admin')
                ->with('data_penyelenggara',$data_penyelenggara)
                ->with('data_kategori',$data_kategori)
                ->with('data_event',$data_event);
    }

    function terima(Request $request, $id, $id2){
        $data_event=Event::find($id);
        $data_penyelenggara = Penyelenggara::find($id2);

        $data_event->registered = "terima";
        $data_event->save();

        Mail::to($data_penyelenggara->email)->send(new AdminNotification($data_penyelenggara, $data_event));

        return redirect('admin/dashboard');
    }


    function tolak(Request $request, $id, $id2){
        $data_event=Event::find($id);
        $data_penyelenggara = Penyelenggara::find($id2);

        $data_event->registered = "tolak";
        $data_event->save();

        $komentar=Komentar::create([
            "id_event"=>$id,
            "komentar"=>$request->input('komentar'),
        ]);
        if($komentar){
            Mail::to($data_penyelenggara->email)->send(new AdminNotification2($data_penyelenggara,$data_event, $komentar));
            Session::flash('sukses','Komentar Berhasil Dikirim');
            return redirect()->back();
        }else {
            Session::flash('gagal','Gagal Mengirim Komentar');
            return redirect()->back();
        }

        // return redirect('admin/dashboard');
    }
}
