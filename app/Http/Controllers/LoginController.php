<?php

namespace App\Http\Controllers;

use App\User;
use Session;
use App\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Mail\PasswordNotification;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function index()
    {
        return view('layout.login');
    }

    public function store(Request $request)
    {
        $validate_admin = User::where('email', $request->input('email'))
                                ->first();

        if($validate_admin){

            if(Hash::check($request->input('password'), $validate_admin->password)){
                Auth::loginUsingId($validate_admin->id);
                $request->session()->put('id', $validate_admin->id);
                if($validate_admin->account_status == '1'){
                    return redirect('/admin/dashboard');
                }
                if($validate_admin->account_status  == '2'){
                    return redirect('/users/'.$validate_admin->id);
                }
            } else{
                Session::flash('gagal','Oppsss, Email atau Password salah');
                return redirect('/login')
                    ->with('account_status', 3);
            }

        }
        else{
            Session::flash('gagal','Oppsss, Email atau Password salah');
            return redirect('/login')
            ->with('account_status', 3);
        }
    }

    function logout(Request $request){
        Auth::logout();
        $request->session()->forget('id');
        
        return redirect('/');
    }

    function forgot(){
        return view('layout.forgot-password');
    }

    function forgot2(Request $request){
        $user = User::where('email','=',$request->email)->first();

        if(is_null($user)){
            Session::flash('gagal','Email Tidak Tersedia');
            return redirect()->back();
        }

        if(!is_null($user)){
            $reset=PasswordReset::create([
                'email' => $request->email,
                'token' => Str::random(60),
                'created_at' => Carbon::now()
            ]);

            Mail::to($user->email)->send(new PasswordNotification($reset));
            Session::flash('sukses','Berhasil mengirim link reset password melalui email Anda');
            return redirect()->back(); return redirect()->back();
        }

    }

    function reset($token){

        return view('layout.reset-password', ['token' => $token]);
    }

    function update_password(Request $request){
        $messages = [
            'required' => ':attribute wajib diisi!',
        ];

        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'confirm-password' => 'required',

        ],$messages);

        $updatePassword = DB::table('reset_password')
                    ->where(['email' => $request->email, 'token' => $request->token])
                    ->first();
        if($updatePassword){
            $user = User::where('email', $request->email)
                ->update(['password' => Hash::make($request->password)]);

            $reset_pass=DB::table('reset_password')->where(['email'=> $request->email])->delete();

            Session::flash('sukses','Berhasil mengubah password Anda!');
            return redirect('login');

        }elseif(!$updatePassword){
            Session::flash('gagal','Gagal mengubah password Anda!');
            return redirect()->back();
        }
    }

    public function cek(Request $request) {
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));

            return redirect(url('users/events/'.$data_user->id))
                            ->with('data_user', $data_user);
        } else {
            return redirect(url('login-event'));
        }
    }

    public function login_event() {
        return view('event.login-event');
    }

    public function simpan_event(Request $request) {
        $validate_admin = User::where('email', $request->input('email'))
                                ->first();

        if($validate_admin){

            if(Hash::check($request->input('password'), $validate_admin->password)){
                Auth::loginUsingId($validate_admin->id);
                $request->session()->put('id', $validate_admin->id);
                if($validate_admin->account_status  == '2'){
                    return redirect('/users/events/'.$validate_admin->id);
                }
            } else{
                Session::flash('gagal','Oppsss, Email atau Password salah');
                return redirect('/login-event')
                    ->with('account_status', 3);
            }

        }
        else{
            Session::flash('gagal','Oppsss, Email atau Password salah');
            return redirect('/login-event')
            ->with('account_status', 3);
        }
    }
}
