<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Event;
use App\KategoriTiket;
use App\Tiket;
use App\Transaksi;
use App\Peserta;
use SimpleSoftwareIO\QrCode\Generator;
use Illuminate\Support\Str;

class QrcodeController extends Controller
{
    public function confirm($id, $id2, $id3, $id4, $id5, $id6) {
        $data_user = User::find($id);
        $data_event = Event::find($id2);
        $data_tiket = KategoriTiket::find($id3);
        $data_eventtiket = Tiket::find($id4);
        $data_transaksi = Transaksi::find($id5);
        $data_peserta = Peserta::find($id6);

        if($data_transaksi->user_id==$id && $data_peserta->user_id==$id && $data_eventtiket->id_event==$id2 && $data_eventtiket->id_tiket==$id3 && $data_transaksi->id_eventtiket==$id4 && $data_transaksi->id_peserta==$id6 && $data_eventtiket->id_peserta==$id6) {
            if($data_transaksi->status=="terima") {
                if(date('Y-m-d') == $data_event->tanggal && (date('H:i:sa') >= $data_event->waktu_mulai && (date('H:i:sa') < $data_event->waktu_selesai))) {
                    $data_peserta->confirm_time = now();
                    $data_peserta->attend_time = now();
                    $data_peserta->status = "datang";
                    $data_peserta->save();

                    return view('tiket.tiket-confirmation', compact('data_user', 'data_event', 'data_tiket', 'data_eventtiket', 'data_transaksi', 'data_peserta'));
                }

                elseif(date('Y-m-d') < $data_event->tanggal || (date('Y-m-d') == $data_event->tanggal && (date('H:i:sa') < $data_event->waktu_mulai))) {
                    return redirect(url('waiting-event/'.$data_user->id.'/'.$data_event->id));
                }

                elseif(date('Y-m-d') > $data_event->tanggal || (date('Y-m-d') == $data_event->tanggal && (date('H:i:sa') > $data_event->waktu_selesai))) {
                    return redirect(url('expired-ticket'));
                }
            } else {
                return redirect(url('unconfirmed-transaction/'.$id.'/'.$id2));
            }
        } else {
            return view('/');
        }
    }

    public function waiting($id, $id2) {
        $data_user = User::find($id);
        $data_event = Event::find($id2);

        return view('tiket.tiket-waiting', compact('data_user','data_event'));
    }

    public function expired() {
        return view('tiket.tiket-hangus');
    }

    public function unconfirmed($id, $id2) {
        $data_user = User::find($id);
        $data_event = Event::find($id2);

        return view('tiket.tiket-unpaid', compact('data_user', 'data_event'));
    }
}
