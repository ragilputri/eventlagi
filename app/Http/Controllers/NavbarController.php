<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Tiket;
use App\Peserta;
use App\Transaksi;
use App\KategoriTiket;
use App\Mail\TiketFree;
use App\TransaksiDetail;
use App\Rekening;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\TiketNotification;
use App\Mail\TransaksiNotification;
use App\Mail\TransaksiNotification2;
use Illuminate\Support\Facades\Mail;
use Session;
use App\User;
use Illuminate\Support\Env;
use SimpleSoftwareIO\QrCode\Generator;
use PHPUnit\Framework\Constraint\Count;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class NavbarController extends Controller
{
    public function index(Request $request) {
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));
        } else {
            $data_user = '<a class="nav-link page-scroll" style="font: bold; color:#000000;" href="/login">Login</a>';
        }

        // $data_event=Event::all();
        // $data_event=Event::paginate(9);
        $search = $request -> input("search");
        $count = DB::table('event_tiket')
                ->rightJoin('event', 'event.id', '=', 'event_tiket.id_event')
                ->select('event.id', DB::raw('COUNT(event_tiket.id) as total'))
                ->groupBy('event_tiket.id_event', 'event.id')
                ->get();

        if($search!="") {
            $data_event=DB::table('event')
                ->leftjoin('tiket_kategori','tiket_kategori.id_event','=','event.id')
                ->where('event.nama','like', '%'.$search.'%')
                ->groupBy('id_event')
                ->distinct()
                ->select('tiket_kategori.harga','event.*')
                ->orderBy('event.id','asc')
                ->paginate(9);
        } elseif($search=="") {
            $data_event=DB::table('event')
            ->leftjoin('tiket_kategori','tiket_kategori.id_event','=','event.id')
            // ->groupBy('id_event')
            // ->distinct()
            ->select('tiket_kategori.harga','event.*')
            ->paginate(9);
        }
        return view('navbar.landing-page')
            ->with('count',$count)
            ->with('data_event', $data_event)
            ->with('data_user', $data_user)
            ->with('request', $request);
    }

    public function event_online(Request $request){
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));
        } else {
            $data_user = '<a class="nav-link page-scroll" style="font: bold; color:#000000;" href="/login">Login</a>';
        }

        $search = $request -> input("search");
        $count = DB::table('event_tiket')
                ->rightJoin('event', 'event.id', '=', 'event_tiket.id_event')
                ->select('event.id', DB::raw('COUNT(event_tiket.id) as total'))
                ->groupBy('event_tiket.id_event', 'event.id')
                ->get();
        if($search!=""){
            $data_event=DB::table('event')
                ->leftjoin('tiket_kategori','tiket_kategori.id_event','=','event.id')
                ->where('event.nama','like', '%'.$search.'%')
                ->where('event.status','=','Online')
                ->groupBy('id_event')
                ->distinct()
                ->select('tiket_kategori.harga','event.*')
                ->orderBy('event.id','asc')
                ->paginate(9);
        }elseif($search==""){
            $data_event=DB::table('event')
                ->leftjoin('tiket_kategori','tiket_kategori.id_event','=','event.id')
                ->where('event.status','=','Online')
                ->groupBy('id_event')
                ->distinct()
                ->select('tiket_kategori.harga','event.*')
                ->paginate(9);
        }

        return view('navbar.event-online')
            ->with('data_event',$data_event)
            ->with('request', $request)
            ->with('data_user', $data_user)
            ->with('count',$count);
    }

    public function event_offline(Request $request){
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));
        } else {
            $data_user = '<a class="nav-link page-scroll" style="font: bold; color:#000000;" href="/login">Login</a>';
        }

        $search = $request -> input("search");
        $count = DB::table('event_tiket')
                ->rightJoin('event', 'event.id', '=', 'event_tiket.id_event')
                ->select('event.id', DB::raw('COUNT(event_tiket.id) as total'))
                ->groupBy('event_tiket.id_event', 'event.id')
                ->get();
        if($search!=""){
            $data_event=DB::table('event')
                ->leftjoin('tiket_kategori','tiket_kategori.id_event','=','event.id')
                ->where('event.nama','like', '%'.$search.'%')
                ->where('event.status','=','Offline')
                ->groupBy('id_event')
                ->distinct()
                ->select('tiket_kategori.harga','event.*')
                ->orderBy('event.id','asc')
                ->paginate(9);
        }elseif($search==""){
            $data_event=DB::table('event')
                ->leftjoin('tiket_kategori','tiket_kategori.id_event','=','event.id')
                ->where('event.status','=','Offline')
                ->groupBy('id_event')
                ->distinct()
                ->select('tiket_kategori.harga','event.*')
                ->paginate(9);
        }

        return view('navbar.event-offline')
            ->with('data_event',$data_event)
            ->with('request', $request)
            ->with('data_user', $data_user)
            ->with('count',$count);
    }

    public function detail(Request $request, $slug){
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));
        } else {
            $data_user = '<a class="nav-link page-scroll" style="font: bold; color:#000000;" href="/login">Login</a>';
        }

        $data_tiket=KategoriTiket::all();
        $data_event = Event::where('slug', $slug)->first();
        $data_lain=Event::all();
        $nama=explode(' ' ,$data_event->nama);
        $tags=explode(', ' ,$data_event->tags);
        $data_tiket=KategoriTiket::leftjoin('event','event.id','=','tiket_kategori.id_event')
            ->select('event.*','tiket_kategori.*')
            ->where('tiket_kategori.id_event',$data_event->id)
            ->paginate(5);
            $data_lain=DB::table('event')
            ->where(function($query) use ($tags, $nama) {
                for($i=0; $i<count($tags); $i++){
                    $query->orWhere('tags', 'like', '%'.$tags[$i].'%');
                }
                for($i=0; $i<count($nama); $i++){
                    $query->orWhere('nama', 'like', '%'.$nama[$i].'%');
                }
            })
            ->paginate(5);
        return view('navbar.navbar-event', compact('data_tiket', 'data_event', 'data_lain', 'request', 'data_user'));
    }

    public function registrasi(Request $request, $id, $id2){
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));
        } else {
            $data_user = '<a class="nav-link page-scroll" style="font: bold; color:#000000;" href="/login">Login</a>';
        }

        $data_event = Event::find($id);
        $user_data=User::find($id2);

        $data_tiket=DB::table('tiket_kategori')
        ->leftjoin('event','event.id','=','tiket_kategori.id_event')
        ->select('event.*','tiket_kategori.*')
        ->where('tiket_kategori.id_event', $data_event->id)
        ->paginate(5);
        return view('navbar.navbar-tiket')
        ->with('data_event', $data_event)
        ->with('data_tiket',$data_tiket)
        ->with('request', $request)
        ->with('data_user', $data_user)
        ->with('user_data', $user_data);
    }

    public function save(Request $request,$id){

        $data_event=Event::find($id);
        $data_kategori = KategoriTiket::find($request->input('id_tiket'));

        $data_eventtiket = Tiket::create([
            "id_event"=>$request->input("id_event"),
            "id_tiket"=>$request->input("id_tiket"),
            "name"=>$request->input("nama"),
            "email"=>$request->input("email"),
            "no_telp"=>$request->input("no_telp"),
            "no_ktp"=>$request->input("no_ktp"),
            "alamat"=>$request->input("alamat"),
        ]);
        $data_transaksi = Transaksi::create([
            "name"=>$request->input("nama"),
            "email"=>$request->input("email"),
            "no_hp"=>$request->input("no_telp"),
            "id_eventtiket"=>$data_eventtiket->id,
            "harga"=>$data_kategori->harga,
            "total"=>$data_kategori->harga,
            "user_id"=>$request->input("user_id"),
        ]);
        $data_transaksidetail = TransaksiDetail::create([
            "transaksi_id"=>$data_transaksi->id,
            "id_eventtiket"=>$data_eventtiket->id,
            "harga"=>$data_kategori->harga,
            "total"=>$data_kategori->harga,

        ]);
        $data_peserta = Peserta::create([
            "nama"=>$request->input("nama"),
            "email"=>$request->input("email"),
            "no_hp"=>$request->input("no_telp"),
            "ktp"=>$request->input("no_ktp"),
            "user_id"=>$request->input("user_id"),
        ]);

        $link = "https://eventlagi.com/confirm-attend/".$request->input('user_id')."/".$request->input('id_event')."/".$request->input('id_tiket')."/".$data_eventtiket->id."/".$data_transaksi->id."/".$data_peserta->id;

        $qrcode = new Generator;
        $data_qr = $qrcode->size(150)->generate($link);
        $qrcode_img = QrCode::size(150)
                ->format('png')
                ->generate($link, public_path('images/qrcode.png'));

        $data_eventtiket = Tiket::find($data_eventtiket->id);
        $data_eventtiket->id_peserta = $data_peserta->id;
        $data_eventtiket->qr_code = $link;
        $data_eventtiket->save();

        $data_transaksi = Transaksi::find($data_transaksi->id);
        $data_transaksi->id_peserta = $data_peserta->id;
        $data_transaksi->qr_code = $link;
        
        if($data_transaksi->harga=="0"){
            $data_transaksi->status = "terima";
        }
        $data_transaksi->save();

        $data_peserta = Peserta::find($data_peserta->id);
        $data_peserta->qr_code = $link;
        $data_peserta->save();

        if($data_eventtiket){
            Session::flash('sukses','Notifikasi tiket berhasil dikirim!');
            if($data_transaksi->total=="0"){
                $data_qr = QrCode::size(150)->generate($data_transaksi->qr_code);
                Mail::to($data_eventtiket->email)->send(new TiketFree($data_eventtiket, $data_transaksi, $data_event));
                Mail::to($data_transaksi->email)->send(new TransaksiNotification($data_transaksi, $data_event, $data_kategori,$data_qr));
            }
            else{
                Mail::to($data_eventtiket->email)->send(new TiketNotification($data_eventtiket, $data_transaksi, $data_event));
            }

            return redirect(url('tiket-transaksi/'.$data_eventtiket->id."/".$data_transaksi->user_id))
                    ->with('data_transaksi',$data_transaksi)
                    ->with("data_eventtiket" , $data_eventtiket)
                    ->with("data_transaksidetail", $data_transaksidetail)
                    ->with("data_peserta", $data_peserta)
                    ->with("data_qr", $data_qr);
        }else{
            Session::flash('gagal','ERROR');
            return redirect()->back();
        }
    }

    public function tikettransaksi(Request $request, $id, $id2){
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));
        } else {
            $data_user = '<a class="nav-link page-scroll" style="font: bold; color:#000000;" href="/login">Login</a>';
        }

        $base_url = env('BASE_URL');
        $data_event=Event::all();
        $data_event = Event::find($id);
        $user_data=User::find($id2);
        $data_tiket=KategoriTiket::all();
        $data_eventtiket=DB::table('event_tiket')
        ->leftjoin('event','event.id','=','event_tiket.id_event')
        ->leftjoin('tiket_kategori','tiket_kategori.id','=','event_tiket.id_tiket')
        ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
        ->where('event_tiket.id', $id)
        ->select('event.*','event_tiket.*','tiket_kategori.jenis','transaksi.*')
        ->get();

        return view('navbar.navbar-transaksi')
        ->with('data_event', $data_event)
        ->with('data_tiket',$data_tiket)
        ->with('user_data',$user_data)
        ->with('base_url',$base_url)
        ->with('request', $request)
        ->with('data_user', $data_user)
        ->with('data_eventtiket',$data_eventtiket);
    }

    public function pengguna($id) {
        $data_event = Event::all();
        $data_transaksi = Transaksi::all();
        $data_user=User::find($id);
        $data_tiket=KategoriTiket::all();
        $data_eventtiket=DB::table('event_tiket')
        ->leftjoin('event','event.id','=','event_tiket.id_event')
        ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
        ->leftJoin('tiket_kategori','tiket_kategori.id','=','event_tiket.id_tiket')
        ->where('transaksi.user_id',$id)
        ->select('event.*','event_tiket.*','transaksi.*', 'tiket_kategori.jenis')
        ->orderBy("transaksi.id","desc")
        ->paginate(5);
        return view('profile.profile-transaksi')
            ->with('data_event', $data_event)
            ->with('data_user', $data_user)
            ->with('data_transaksi', $data_transaksi)
            ->with("data_eventtiket" , $data_eventtiket)
            ->with('data_tiket', $data_tiket);
    }
    public function profiletransaksi($id, Request $request){
        $imgName = $request->bukti_up->getClientOriginalName() . '-' . time() . '.' . $request->bukti_up->extension();
        $request->file('bukti_up')->move(public_path('images'), $imgName);

        $data_transaksi = Transaksi::where('id', $id)->first();
        $data_eventtiket=Tiket::find($data_transaksi->id_eventtiket)->first();

        $data_transaksi->bukti_up = $imgName;
        $data_transaksi->save();

        return redirect()->back();
    }

    public function create_event(Request $request) {
        if($request->session()->has('id')) {
            $data_user = User::find($request->session()->get('id'));

            return redirect(url('users/penyelenggara/create/'.$data_user->id));
        } else {
            return redirect(url('/login'));
        }
    }
}
