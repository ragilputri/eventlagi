<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\TransaksiNotification;
use App\Mail\TransaksiNotification2;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Generator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Session;
use App\Transaksi;
use App\Peserta;
use App\Tiket;
use App\Event;
use App\KategoriTiket;
use App\User;
use App\Komentar;

class TransaksiController extends Controller
{
    function list(Request $request){
        $data_event=Event::all();
        $month = $request->get('month');
        $year = $request->get('year');
        $search = $request -> input("search");
        $btn = $request -> input("btn");
        $status = $request->get("status");
        if ($btn == '1' && $month!="" && $year!="") {
            $data =DB::table('event_tiket')
            ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
            ->leftjoin('event','event.id','=','event_tiket.id_event')
            ->whereYear('transaksi.created_at', '=', $year)
            ->whereMonth('transaksi.created_at', '=', $month)
            ->select('event.*','event_tiket.*','transaksi.*')
            ->orderBy('transaksi.id','desc')
            ->paginate(5);
        }elseif ($btn == '1'  && $month=="" && $year!="") {
            $data =DB::table('event_tiket')
                    ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                    ->leftjoin('event','event.id','=','event_tiket.id_event')
                    ->whereYear('transaksi.created_at', '=', $year)
                    ->select('event.*','event_tiket.*','transaksi.*')
                    ->orderBy('transaksi.id','desc')
                    ->paginate(5);
        } elseif ($btn == '1' && $month!="" && $year=="")  {
            $data =DB::table('event_tiket')
                    ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                    ->leftjoin('event','event.id','=','event_tiket.id_event')
                    ->whereMonth('transaksi.created_at', '=', $month)
                    ->select('event.*','event_tiket.*','transaksi.*')
                    ->orderBy('transaksi.id','desc')
                    ->paginate(5);
        }elseif($btn == '1' && $month=="" && $year=="" && $status!="") {
            $data =DB::table('event_tiket')
            ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
            ->leftjoin('event','event.id','=','event_tiket.id_event')
            ->where('transaksi.status','=', $status)
            ->select('event.*','event_tiket.*','transaksi.*')
            ->orderBy('transaksi.id','desc')
            ->paginate(5);
        }else {
            $data =DB::table('event_tiket')
                    ->rightjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                    ->leftjoin('event','event.id','=','event_tiket.id_event')
                    ->leftjoin('tiket_kategori','tiket_kategori.id','=','event_tiket.id_tiket')
                    ->select('event.*','event_tiket.*','tiket_kategori.*','transaksi.*')
                    ->orderBy('transaksi.id','desc')
                    ->paginate(5);
            // $data_transaksi = Transaksi::orderBy('id','desc')->paginate(5);
        }
        return view('transaksi.transaksi-list')
                ->with('data',$data);
    }

    function create(){
        $data_transaksi = Transaksi::all();
        $data_peserta = Peserta::all();
        $data_transaksi = DB::table('transaksi')
        ->join('peserta', 'peserta.id', '=', 'transaksi.id_peserta')
        ->select('peserta.*','transaksi.*')
        ->paginate(5);
        return view('transaksi.transaksi-create')
                ->with('data_peserta',$data_peserta)
                ->with('data_transaksi',$data_transaksi);
    }

    function save(Request $request){
        $imgName = $request->bukti_up->getClientOriginalName() . '-' . time() . '.' . $request->bukti_up->extension();
        $request->bukti_up->move(public_path('images'), $imgName);

        $data_transaksi = Transaksi::create([
            "id_peserta"=>$request->input("nama"),
            "id_eventtiket"=>$request->input("id_eventtiket"),
            "status"=>$request->input("status"),
            "bukti_up"=>$imgName,
            "qr_code"=>$request->input("qr_code"),

        ]);
        if($data_transaksi){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('admin/transaksi'))
                    ->with("data_transaksi",$data_transaksi);
        }else{
            Session::flash('gagal','ERROR');
            return redirect(url('admin/transaksi'));
        }
    }

    function delete($id, Request $request){
        $data_transaksi = Transaksi::find($id);
        $data_transaksi -> delete();
        if($data_transaksi){
            Session::flash('sukses','Sukses Delete Data');
            return redirect(url('admin/transaksi'));
        }else{
            Session::flash('gagal','Gagal Delete Data');
            return redirect(url('admin/transaksi'));
        }
    }

    function edit($id){
        $data_transaksi = Transaksi::find($id);
        $data_peserta = DB::table('transaksi')
        ->leftjoin('peserta', 'peserta.id', '=', 'transaksi.id_peserta')
        ->select('peserta.*','transaksi.*')
        ->paginate(5);

        return view('transaksi.transaksi-edit')
                ->with('data_peserta',$data_peserta)
                ->with('data_transaksi',$data_transaksi);
    }

    function update($id, Request $request){

        $imgName = $request->bukti_up->getClientOriginalName() . '-' . time() . '.' . $request->bukti_up->extension();
        $request->bukti_up->move(public_path('images'), $imgName);

        $data_transaksi = Transaksi::find($id);
        $data_transaksi->id_eventtiket=$request->input("id_eventtiket");
        $data_transaksi->id_peserta=$request->input("nama");
        $data_transaksi->status=$request->input("status");
        $data_transaksi->bukti_up=$imgName;
        $data_transaksi->qr_code=$request->input("qr_code");

            {
                $data_transaksi->save();
                if($data_transaksi){
                    Session::flash('sukses','Sukses Menyimpan Data');
                    return redirect(url('admin/transaksi'))
                            ->with("data_transaksi",$data_transaksi);
                }else{
                    Session::flash('gagal','ERROR');
                    return redirect(url('admin/transaksi'));
                }

            }
        }

    // function transaksi_sukses(Request $request){
    //     $month = $request->get('month');
    //     $year = $request->get('year');
    //     $search = $request -> input("search");
    //     $btn = $request -> input("btn");
    //     if ($btn == '1' && $month!="" && $year!="") {
    //         $data_transaksi=Transaksi::where('transaksi.status','!=','tolak')
    //                             ->whereYear('transaksi.created_at', '=', $year)
    //                             ->whereMonth('transaksi.created_at', '=', $month)
    //                             ->paginate(5);
    //     } elseif ($btn == '1'  && $month=="" && $year!="") {
    //         $data_transaksi = Transaksi::where('transaksi.status','!=','tolak')
    //                             ->whereYear('transaksi.created_at', '=', $year)
    //                             ->paginate(5);
    //     } elseif ($btn == '1' && $month!="" && $year=="")  {
    //         $data_transaksi = Transaksi::where('transaksi.status','!=','tolak')
    //                             ->whereMonth('transaksi.created_at', '=', $month)
    //                             ->paginate(5);
    //     }
    //     else {
    //         $data_transaksi=Transaksi::where('transaksi.status','!=','tolak')
    //                             ->paginate(5);
    //     }
    //     return view('transaksi.transaksi-sukses')
    //             ->with('data_transaksi',$data_transaksi);
    // }

    // function transaksi_belum(Request $request){
    //     $month = $request->get('month');
    //     $year = $request->get('year');
    //     $search = $request -> input("search");
    //     $btn = $request -> input("btn");
    //     if ($btn == '1' && $month!="" && $year!="") {
    //         $data_transaksi=Transaksi::where('transaksi.status',NULL)
    //                             ->whereYear('transaksi.created_at', '=', $year)
    //                             ->whereMonth('transaksi.created_at', '=', $month)
    //                             ->paginate(5);
    //     } elseif ($btn == '1'  && $month=="" && $year!="") {
    //         $data_transaksi = Transaksi::where('transaksi.status',NULL)
    //                             ->whereYear('transaksi.created_at', '=', $year)
    //                             ->paginate(5);
    //     } elseif ($btn == '1' && $month!="" && $year=="")  {
    //         $data_transaksi = Transaksi::where('transaksi.status',NULL)
    //                             ->whereMonth('transaksi.created_at', '=', $month)
    //                             ->paginate(5);
    //     }
    //     else {
    //         $data_transaksi=Transaksi::where('transaksi.status',NULL)
    //                             ->paginate(5);
    //     }
    //     return view('transaksi.transaksi-belum')
    //             ->with('data_transaksi',$data_transaksi);
    // }

    public function qrcode($id) {
        $data_user = User::find($id);
        $link = "https://eventlagi.com/confirm-attend/".$data_user->id;

        \QrCode::size(100)
            ->format('png')
            ->generate($link, public_path('qrcode/qrcode.png'));

        return view('contoh');
    }

    public function transaksi_tolak(Request $request,$id){
        $data_transaksi = Transaksi::find($id);

        $komentar = Komentar::create([
            "id_transaksi"=>$data_transaksi->id,
            "komentar"=>$request->input("komentar"),

        ]);

        $data_transaksi->status="tolak";
        $data_transaksi->save();

        Mail::to($data_transaksi->email)->send(new TransaksiNotification2($data_transaksi,$komentar));

        return redirect()->back();

    }

    public function transaksi_terima($id,$id2){
        $data_transaksi=Transaksi::find($id);
        $data_eventtiket=Tiket::find($id2);
        $data_event = Event::where('event.id',$data_eventtiket->id_event)->first();
        $data_kategori = KategoriTiket::where('tiket_kategori.id',$data_eventtiket->id_tiket)->first();

        $data_transaksi->status="terima";
        $data_transaksi->save();

        $data_qr = QrCode::size(150)->generate($data_transaksi->qr_code);

        Mail::to($data_transaksi->email)->send(new TransaksiNotification($data_transaksi, $data_event, $data_kategori,$data_qr));

        return redirect()->back();

    }
}
