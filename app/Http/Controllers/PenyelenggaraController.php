<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penyelenggara;
use App\User;
use App\Event;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PenyelenggaraController extends Controller
{
    function list(Request $request){
        $data_user = User::all();
        $month = $request->get('month');
        $year = $request->get('year');
        $search = $request -> input("search");
        $btn = $request -> input("btn");
        if ($btn == '1' && $month!="" && $year!="") {
            $data_penyelenggara = DB::table('penyelenggara')
                                ->leftjoin('users','users.id','=','penyelenggara.user_id')
                                ->whereYear('penyelenggara.created_at', '=', $year)
                                ->whereMonth('penyelenggara.created_at', '=', $month)
                                ->select('users.*','penyelenggara.*')
                                ->paginate(5);
        } elseif ($btn == '1'  && $month=="" && $year!="") {
            $data_penyelenggara = DB::table('penyelenggara')
                                ->leftjoin('users','users.id','=','penyelenggara.user_id')
                                ->whereYear('penyelenggara.created_at', '=', $year)
                                ->select('users.*','penyelenggara.*')
                                ->paginate(5);
        } elseif ($btn == '1' && $month!="" && $year=="")  {
            $data_penyelenggara = DB::table('penyelenggara')
                                ->leftjoin('users','users.id','=','penyelenggara.user_id')
                                ->whereMonth('penyelenggara.created_at', '=', $month)
                                ->select('users.*','penyelenggara.*')
                                ->paginate(5);
        }
        else {
            $data_penyelenggara = DB::table('penyelenggara')
                                ->leftjoin('users','users.id','=','penyelenggara.user_id')
                                ->select('users.*','penyelenggara.*')
                                ->paginate(5);
        }

        return view('penyelenggara.penyelenggara-list')
                ->with('data_user',$data_user)
                ->with('data_penyelenggara',$data_penyelenggara);
    }

    function create(){
        return view('penyelenggara.penyelenggara-create');
    }

    function save(Request $request){
        $data_penyelenggara = Penyelenggara::create([
            "name_penyelenggara"=>$request->input("name_penyelenggara"),
            "email"=>$request->input("email"),
            "no_hp"=>$request->input("no_hp"),
            "ktp"=>$request->input("ktp"),
            "password"=>(Hash::make($request->input('password'))),
        ]);
        if($data_penyelenggara){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('admin/penyelenggara'))
                    ->with("data_penyelenggara",$data_penyelenggara);
        }else{
            Session::flash('gagal','ERROR');
            return redirect(url('admin/penyelenggara'));
        }
    }

    function daftar_event($id){
        $data_penyelenggara = Penyelenggara::find($id);
        $data_event = DB::table('event')
        ->leftjoin('penyelenggara','penyelenggara.id','=','event.id_penyelenggara')
        ->where('event.id_penyelenggara',$id)
        ->select('event.*','penyelenggara.*')
        ->paginate(5);

        return view('penyelenggara.penyelenggara-event')
            ->with('data_event',$data_event)
            ->with('data_penyelenggara',$data_penyelenggara);
    }

    function edit($id, Request $request)
    {
        $data_penyelenggara = Penyelenggara::find($id);
        return view ('penyelenggara.penyelenggara-edit')
                ->with('data_penyelenggara',$data_penyelenggara);
    }

    function update($id,Request $request)
    {
        $data_penyelenggara = Penyelenggara::find($id);
        $data_penyelenggara->name = $request->input("name");
        $data_penyelenggara->email = $request->input("email");
        $data_penyelenggara->no_hp = $request->input("no_hp");
        $data_penyelenggara->ktp = $request->input("ktp");
        $data_penyelenggara->password = $request->input("password");

        {
            $data_penyelenggara->save();
            if($data_penyelenggara){
                Session::flash('sukses','Sukses Update Data');
                return redirect(url('admin/penyelenggara'));
            }else{
                Session::flash('gagal','Gagal Update Data');
                return redirect(url('admin/penyelenggara'));
            }
        }

    }

    function delete($id){
        $data_penyelenggara = Penyelenggara::find($id);
        $data_penyelenggara -> delete();
        if($data_penyelenggara){
            Session::flash('sukses','Sukses Delete Data');
            return redirect(url('admin/penyelenggara'));
        }else{
            Session::flash('gagal','Gagal Delete Data');
            return redirect(url('admin/penyelenggara'));
        }

    }
}
