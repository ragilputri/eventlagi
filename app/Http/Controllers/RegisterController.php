<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EmailNotification;
use Illuminate\Support\Facades\Mail;
use App\User;
use Session;

class RegisterController extends Controller
{
    public function show() {
        return view('layout.register');
    }

    public function save(Request $request) {
        $password = $request->input('password');
        $pass = bcrypt($password);

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email',
            'password' => 'min:6',
            'confirm_password' => 'required_with:password|same:password|min:6',
        ]);

        $data_user = User::create([
            "name" => $request -> input("name"),
            "email" => $request -> input("email"),
            "password" => $pass,
            "account_status" => 2,
            "photo" => "default-profile.png",
        ]);

        Mail::to($data_user->email)->send(new EmailNotification($data_user));

        Session::flash('sukses','Berhasil membuat akun. Login Kembali!');
        return redirect(url("login"));
    }

    public function register() {
        return view('event.register-event');
    }

    public function save_event(Request $request) {
        $password = $request->input('password');
        $pass = bcrypt($password);

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email',
            'password' => 'min:6',
            'confirm_password' => 'required_with:password|same:password|min:6',
        ]);
        
        $data_user = User::create([
            "name" => $request -> input("name"),
            "email" => $request -> input("email"),
            "password" => $pass,
            "account_status" => 2,
            "photo" => "default-profile.png",
        ]);

        Mail::to($data_user->email)->send(new EmailNotification($data_user));

        Session::flash('sukses','Berhasil membuat akun. Login Kembali!');
        return redirect(url("login-event"))
                ->with('data_user', $data_user);
    }
}
