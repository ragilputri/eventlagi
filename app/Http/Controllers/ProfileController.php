<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Peserta;
use App\Tiket;
use App\User;
use App\Event;
use App\Transaksi;
use App\Penyelenggara;
use App\KategoriTiket;
use App\TransaksiDetail;
use Session;
use Illuminate\Support\Str;


class ProfileController extends Controller
{
    public function list($id){
        $data_user = User::find($id);
        return view('profile.profile')
            ->with('data_user', $data_user);
    }

    public function update($id, Request $request) {
        // $password = $request->input('pass');
        // $pass = bcrypt($password);

        $data_user = User::find($id);
        $data_user -> name = $request->input('nama');
        $data_user -> email = $request->input('email');
        // $data_user -> password = $pass;
        $data_user -> phone = $request->input('no_hp');
        $data_user -> bio = $request->input('bio');
        $data_user -> save();
        return redirect(url("users/profile/".$data_user->id))
        ->with('data_user', $data_user);
    }

    public function change(Request $request, $id) {
        $data_user = User::find($id);
        if(Hash::check($request->input('current_password'), $data_user->password)) {
            $this->validate($request, [
                'password' => 'min:6',
                'confirm_password' => 'required_with:password|same:password|min:6',
            ]);
            if($request->input('password') == $request->input('confirm_password')) {
                $password = $request->input('password');
                $pass = bcrypt($password);

                $data_user -> password = $pass;
                $data_user -> save();

                Session::flash('sukses','Password telah diubah.');
                return redirect(url('users/profile/'.$data_user->id));
            }
            elseif($request->input('password') != $request->input('confirm_password')) {
                Session::flash('gagal','Password salah.');
                return redirect(url('users/profile/'.$data_user->id));
            }
        } else {
            Session::flash('gagal','Password salah.');
            return redirect(url('users/profile/'.$data_user->id));
        }
    }

    public function tikets(Request $request,$id){
        $data_event = Event::all();
        $data_transaksi = Transaksi::all();
        $data_user=User::find($id);
        $search = $request -> input("search");
        $month = $request->get('month');
        $year = $request->get('year');
        $btn = $request -> input("btn");
        if ($btn == '1' && $month!="" && $year!="") {
            $data_eventtiket=DB::table('event_tiket')
                        ->leftjoin('event','event.id','=','event_tiket.id_event')
                        ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                        ->where('transaksi.user_id',$id)
                        ->whereYear('event_tiket.created_at', '=', $year)
                        ->whereMonth('event_tiket.created_at', '=', $month)
                        ->select('event.*','event_tiket.*','transaksi.*')
                        ->orderBy('transaksi.id','desc')
                        ->paginate(5);
        } elseif ($btn == '1'  && $month=="" && $year!="") {
            $data_eventtiket=DB::table('event_tiket')
                        ->leftjoin('event','event.id','=','event_tiket.id_event')
                        ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                        ->where('transaksi.user_id',$id)
                        ->whereYear('event_tiket.created_at', '=', $year)
                        ->select('event.*','event_tiket.*','transaksi.*')
                        ->orderBy('transaksi.id','desc')
                        ->paginate(5);
        } elseif ($btn == '1' && $month!="" && $year=="")  {
            $data_eventtiket=DB::table('event_tiket')
                        ->leftjoin('event','event.id','=','event_tiket.id_event')
                        ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                        ->where('transaksi.user_id',$id)
                        ->whereMonth('event_tiket.created_at', '=', $month)
                        ->select('event.*','event_tiket.*','transaksi.*')
                        ->orderBy('transaksi.id','desc')
                        ->paginate(5);
        }else {
            $data_eventtiket=DB::table('event_tiket')
                        ->leftjoin('event','event.id','=','event_tiket.id_event')
                        ->leftjoin('transaksi','transaksi.id_eventtiket','=','event_tiket.id')
                        ->where('transaksi.user_id',$id)
                        ->select('event.*','event_tiket.*','transaksi.*')
                        ->orderBy('transaksi.id','desc')
                        ->paginate(5);
        }

        return view('profile.profile-tiket')
            ->with('data_event', $data_event)
            ->with('data_user', $data_user)
            ->with('data_transaksi', $data_transaksi)
            ->with("data_eventtiket" , $data_eventtiket);

    }

    public function events(Request $request,$id){
        $data_user=User::find($id);
        $data_penyelenggara=Penyelenggara::all();
        $month = $request->get('month');
        $year = $request->get('year');
        $search = $request -> input("search");
        $btn = $request -> input("btn");
        if ($btn == '1' && $month!="" && $year!="") {
            $data_event = DB::table('event')
                        ->leftjoin('penyelenggara', 'penyelenggara.id', '=', 'event.id_penyelenggara')
                        ->where( "penyelenggara.user_id",$id )
                        ->whereYear('event.created_at', '=', $year)
                        ->whereMonth('event.created_at', '=', $month)
                        ->select('penyelenggara.*', 'event.*')
                        ->paginate(5);
        } elseif ($btn == '1'  && $month=="" && $year!="") {
            $data_event = DB::table('event')
                        ->leftjoin('penyelenggara', 'penyelenggara.id', '=', 'event.id_penyelenggara')
                        ->where( "penyelenggara.user_id",$id )
                        ->whereYear('event.created_at', '=', $year)
                        ->select('penyelenggara.*', 'event.*')
                        ->paginate(5);
        } elseif ($btn == '1' && $month!="" && $year=="")  {
            $data_event = DB::table('event')
                        ->leftjoin('penyelenggara', 'penyelenggara.id', '=', 'event.id_penyelenggara')
                        ->where( "penyelenggara.user_id",$id )
                        ->whereMonth('event.created_at', '=', $month)
                        ->select('penyelenggara.*', 'event.*')
                        ->paginate(5);
        }
        else {
            $data_event = DB::table('event')
                        ->leftjoin('penyelenggara', 'penyelenggara.id', '=', 'event.id_penyelenggara')
                        ->leftjoin('komentar','komentar.id_event','=','event.id')
                        ->where('penyelenggara.user_id',$id)
                        ->select('penyelenggara.*', 'komentar.komentar','event.*')
                        ->orderBy('event.id','desc')
                        ->paginate(5);
        }
        return view('profile.profile-event')
        ->with('data_event',$data_event)
        ->with('data_penyelenggara',$data_penyelenggara)
        ->with('data_user',$data_user);

    }

    public function penyelenggara_create($id){
        $data_user=User::find($id);
        return view('profile.profile-penyelenggara-create')
            ->with('data_user',$data_user);
    }

    public function penyelenggara_save(Request $request,$id){
            $data_user=User::find($id);

            $messages = [
                'required' => ':attribute wajib diisi!',
            ];

            if($request->tombol_add){
                $this->validate($request,[
                    'name_penyelenggara' => 'required',
                    'email' => 'required',
                    'no_hp' => 'required',
                    'ktp' => 'required',
                    'password' => 'required',
                ],$messages);
                $data_penyelenggara = Penyelenggara::create([
                    "user_id"=>$data_user->id,
                    "name_penyelenggara"=>$request->input("name_penyelenggara"),
                    "email"=>$request->input("email"),
                    "no_hp"=>$request->input("no_hp"),
                    "ktp"=>$request->input("ktp"),
                    "password"=>(Hash::make($request->input('password'))),
                ]);
            }
            return redirect(url('users/event/create/'.$data_user->id."/".$data_penyelenggara->id))
                    ->with("data_penyelenggara",$data_penyelenggara)
                    ->with("data_user",$data_user);

    }

    public function event_create($id,$id2){
        $data_user=User::find($id);
        $data_penyelenggara=Penyelenggara::find($id2);
        $data_event = DB::table('event')
        ->join('penyelenggara', 'penyelenggara.id', '=', 'event.id_penyelenggara')
        ->select('penyelenggara.*', 'event.*')
        ->get();

        return view('profile.profile-event-create')
                    ->with('data_penyelenggara', $data_penyelenggara)
                    ->with('data_user',$data_user)
                    ->with('data_event', $data_event);
    }

    public function event_save(Request $request,$id,$id2){
        $data_user=User::find($id);
        $data_penyelenggara=Penyelenggara::find($id2);

        $messages = [
            'required' => ':attribute wajib diisi!',
        ];

        if($request->tombol_add){
            $this->validate($request,[
                'jenis_event' => 'required',
                'foto' => 'required',
                'nama' => 'required',
                'tanggal' => 'required',
                'tempat' => 'required',
                'waktu_mulai' => 'required',
                'waktu_selesai' => 'required',
                'deskripsi' => 'required',
                'status' => 'required',
                'kuota' => 'required',
            ],$messages);

            $imgName = $request->foto->getClientOriginalName() . '-' . time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('images'), $imgName);
            $slug = Str::slug($request->nama);

            // $tags = explode(', ', $request->tags);
            $data_event = Event::create([
                "id_penyelenggara"=>$data_penyelenggara->id,
                "jenis_event"=>$request->input("jenis_event"),
                "foto"=>$imgName,
                "nama"=>$request->input("nama"),
                "tanggal"=>$request->input("tanggal"),
                "tempat"=>$request->input("tempat"),
                "waktu_mulai"=>$request->input("waktu_mulai"),
                "waktu_selesai"=>$request->input("waktu_selesai"),
                "deskripsi"=>$request->input("deskripsi"),
                "tags"=>$request->input("tags"),
                // "tags"=>array_values($tags),
                "status"=>$request->input("status"),
                "link"=>"https://eventlagi.com/detail-event/" . $slug,
                "slug"=>$slug,
                "kuota"=>$request->input("kuota"),

            ]);

            // $data_event->attachTags($tags);
        }
        return redirect(url('users/tiket/create/'.$data_user->id."/".$data_event->id))
                    ->with('data_user',$data_user)
                    ->with("data_event",$data_event);
    }

    public function event_edit($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);
        $data_penyelenggara=Penyelenggara::all();
        $data_event = DB::table('event')
        ->leftjoin('penyelenggara', 'event.id_penyelenggara', '=','penyelenggara.id')
        ->where('event.id',$id2)
        ->select('penyelenggara.*', 'event.*')
        ->first();

        return view('profile.profile-event-edit')
                ->with('data_user',$data_user)
                ->with('data_event',$data_event)
                ->with('data_penyelenggara',$data_penyelenggara);
    }

    public function event_update(Request $request,$id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);

        $imgName = $request->foto->getClientOriginalName() . '-' . time() . '.' . $request->foto->extension();
        $request->foto->move(public_path('images'), $imgName);
        $slug = Str::slug($request->nama);

        $data_event->jenis_event = $request->input("jenis_event");
        $data_event->foto = $imgName;
        $data_event->nama = $request->input("nama");
        $data_event->tanggal = $request->input("tanggal");
        $data_event->tempat = $request->input("tempat");
        $data_event->waktu_mulai = $request->input("waktu_mulai");
        $data_event->waktu_selesai = $request->input("waktu_selesai");
        $data_event->deskripsi = $request->input("deskripsi");
        $data_event->tags = $request->input("tags");
        $data_event->status = $request->input("status");
        $data_event->link = "https://eventlagi.com/detail-event/".$slug;
        $data_event->slug = $slug;
        $data_event->kuota = $request->input("kuota");

        $data_event->save();
            if($data_event){
                Session::flash('sukses','Sukses Update Data');
                return redirect(url('users/events/'.$data_user->id));
            }else{
                Session::flash('gagal','Gagal Update Data');
                return redirect(url('users/events/'.$data_user->id));
            }
    }

    public function event_peserta($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);
        $data_transaksi=Transaksi::all();
        $data_kategori = KategoriTiket::all();
        $data_eventtiket=DB::table('event_tiket')
        ->leftjoin('tiket_kategori','tiket_kategori.id','=','event_tiket.id_tiket')
        ->leftjoin('peserta','peserta.id','=','event_tiket.id_peserta')
        ->where('event_tiket.id_event',$id2)
        ->select('tiket_kategori.*','peserta.*','event_tiket.*')
        ->paginate(5);
        $count = DB::table('event_tiket')
                ->where('event_tiket.id_event',$id2)
                ->count();
        $hadir = DB::table('peserta')
                ->rightJoin('event_tiket', 'event_tiket.id_peserta', '=', 'peserta.id')
                ->select('event_tiket.id_event', DB::raw('COUNT(event_tiket.id_peserta) as total'))
                ->groupBy('event_tiket.id_event')
                ->where('peserta.status','=','datang')
                ->get();

        return view('profile.profile-event-peserta')
        ->with('data_user',$data_user)
        ->with('count',$count)
        ->with('data_kategori',$data_kategori)
        ->with('data_transaksi',$data_transaksi)
        ->with('data_eventtiket',$data_eventtiket)
        ->with('data_event', $data_event)
        ->with('hadir',$hadir);
    }

    public function tiket_create($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);
        $data_kategori=KategoriTiket::all();
        $data_kategori=DB::table('tiket_kategori')
        ->leftjoin('event','event.id','=','tiket_kategori.id_event')
        ->where('tiket_kategori.id_event',$id2)
        ->select('event.*','tiket_kategori.*')
        ->paginate(5);
        return view('profile.profile-tiket-create')
                    ->with('data_kategori',$data_kategori)
                    ->with('data_user',$data_user)
                    ->with('data_event', $data_event);
    }

    public function tiket_save(Request $request,$id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);

        $messages = [
            'required' => ':attribute wajib diisi!',
        ];

        if($request->simpan){
            $this->validate($request,[
                'jenis' => 'required',
                'harga' => 'required',
                'deskripsi' => 'required',
            ],$messages);
            $data_kategori=KategoriTiket::create([
                "jenis"=>$request->input("jenis"),
                "harga"=>$request->input("harga"),
                "deskripsi"=>$request->input("deskripsi"),
                "id_event"=>$data_event->id,
        ]);
        }
        if($data_kategori){
            Session::flash('sukses','Sukses Menyimpan Data, Klik KEMBALI untuk melihat data event yang telah dibuat!');
            return redirect(url('users/kategori/tiket/'.$data_user->id."/".$data_event->id))
                ->with('data_user',$data_user)
                ->with("data_event",$data_event);
        }else{
            Session::flash('gagal','Gagal Menyimpan Data');
            return redirect(url('users/tiket/create/'.$data_user->id."/".$data_event->id))
                    ->with('data_user',$data_user)
                    ->with("data_event",$data_event);
        }


    }

    public function tiket_kategori($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);
        $data_kategori=DB::table('tiket_kategori')
        ->leftjoin('event','event.id','=','tiket_kategori.id_event')
        ->where('tiket_kategori.id_event',$id2)
        ->select('event.*','tiket_kategori.*')
        ->paginate(5);
        return view('profile.profile-kategori-tiket')
                    ->with('data_kategori',$data_kategori)
                    ->with('data_event',$data_event)
                    ->with('data_user',$data_user);
    }

    public function tiket_delete($id){
        $data_kategori=KategoriTiket::find($id);
        $data_kategori->delete();
        if($data_kategori){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect()->back();
        }else {
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect()->back();
        }

    }

    public function event_delete($id,$id2){
        $data_event = Event::find($id2);
        $data_user = User::find($id);
        $data_event -> delete();
        if($data_event){
            Session::flash('sukses','Sukses Delete Data');
            return redirect(url('users/events/'.$data_user->id));
        }else{
            Session::flash('gagal','Gagal Delete Data');
            return redirect(url('users/events/'.$data_user->id));
        }

    }

    public function penyelenggara(Request $request,$id){
        $data_user=User::find($id);
        $data_event=Event::all();
        $search = $request -> input("search");
        $month = $request->get('month');
	    $year = $request->get('year');
        $btn = $request -> input("btn");
        if ($btn == '1' && $month!="" && $year!="") {
            $data_penyelenggara=DB::table('penyelenggara')
                            ->leftjoin('users','users.id','=','penyelenggara.user_id')
                            ->leftjoin('event','event.id_penyelenggara','=','penyelenggara.id')
                            ->where('penyelenggara.user_id',$id)
                            ->whereYear('penyelenggara.created_at', '=', $year)
                            ->whereMonth('penyelenggara.created_at', '=', $month)
                            ->select('users.*','penyelenggara.*','event.nama')
                            ->paginate(5);

        } elseif ($btn == '1'  && $month=="" && $year!="") {
            $data_penyelenggara=DB::table('penyelenggara')
                            ->leftjoin('users','users.id','=','penyelenggara.user_id')
                            ->leftjoin('event','event.id_penyelenggara','=','penyelenggara.id')
                            ->where('penyelenggara.user_id',$id)
                            ->whereYear('penyelenggara.created_at', '=', $year)
                            ->select('users.*','penyelenggara.*','event.nama')
                            ->paginate(5);
        } elseif ($btn == '1' && $month!="" && $year=="")  {
            $data_penyelenggara=DB::table('penyelenggara')
                            ->leftjoin('users','users.id','=','penyelenggara.user_id')
                            ->leftjoin('event','event.id_penyelenggara','=','penyelenggara.id')
                            ->where('penyelenggara.user_id',$id)
                            ->whereMonth('penyelenggara.created_at', '=', $month)
                            ->select('users.*','penyelenggara.*','event.nama')
                            ->paginate(5);
        }else{
            $data_penyelenggara=DB::table('penyelenggara')
                            ->leftjoin('users','users.id','=','penyelenggara.user_id')
                            ->leftjoin('event','event.id_penyelenggara','=','penyelenggara.id')
                            ->where('penyelenggara.user_id',$id)
                            ->select('users.*','penyelenggara.*','event.nama')
                            ->paginate(5);
        }

        return view('profile.profile-penyelenggara')
            ->with('data_penyelenggara',$data_penyelenggara)
            ->with('data_event',$data_event)
            ->with('data_user',$data_user);

    }

    public function remove($id) {
        $data_user = User::find($id);
        $data_user -> photo = "default-profile.png";
        $data_user -> save();
        return redirect(url("users/profile/".$data_user->id))
                ->with('data_user', $data_user);
    }

    public function upload(Request $request, $id) {
        $imgName = $request->photo->getClientOriginalName() . '-' . time() . '.' . $request->photo->extension();
        $request->photo->move(public_path('images'), $imgName);

        $data_user = User::find($id);
        $data_user -> photo = $imgName;
        $data_user -> save();
        return redirect(url("users/profile/".$data_user->id))
                ->with('data_user', $data_user);
    }


    public function list_peserta($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);
        $data_peserta=DB::table('peserta')
        ->first();
        return view('profile.profile-list-peserta')
                    ->with('data_peserta',$data_peserta)
                    ->with('data_event',$data_event)
                    ->with('data_user',$data_user);
    }
}
