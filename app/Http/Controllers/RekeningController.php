<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use App\User;
use App\Event;
use App\Rekening;
use App\Penyelenggara;

class RekeningController extends Controller
{
    public function list($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);

        $data_rekening=DB::table('rekening')
            ->leftjoin('event','event.id','=','rekening.id_event')
            ->where('rekening.id_event',$id2)
            ->select('event.*','rekening.*')
            ->paginate(5);

        return view('rekening.rekening-list')
            ->with('data_rekening',$data_rekening)
            ->with('data_event',$data_event)
            ->with('data_user',$data_user);
    }

    public function create($id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);

        return view('rekening.rekening-create')
        ->with('data_event',$data_event)
        ->with('data_user',$data_user);
    }

    public function save(Request $request,$id,$id2){
        $data_user=User::find($id);
        $data_event=Event::find($id2);

        $messages = [
            'required' => ':attribute wajib diisi!',
        ];

        if($request->tombol_add){
            $this->validate($request,[
                'bank'=>'required',
                'no_rek'=>'required',
                'atas_nama'=>'required',

            ],$messages);
        $data_rekening = Rekening::create([
            "id_penyelenggara"=>$data_event->id_penyelenggara,
            "id_event"=>$data_event->id,
            "id_user"=>$data_user->id,
            "bank"=>$request->input("bank"),
            "no_rek"=>$request->input("no_rek"),
            "atas_nama"=>$request->input("atas_nama"),
        ]);

        }

        if($data_rekening){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect('users/rekening/'.$data_user->id.'/'.$data_event->id)
                ->with('data_user',$data_user)
                ->with('data_event',$data_event);
        }else{
            Session::flash('gagal','Gagal Menyimpan Data');
            return redirect('users/rekening/'.$data_user->id.'/'.$data_event->id)
                ->with('data_user',$data_user)
                ->with('data_event',$data_event);
        }
    }

    public function edit($id,$id2){
        $data_user=User::find($id);
        $data_rekening=Rekening::find($id2);

        return view('rekening.rekening-edit')
            ->with('data_user',$data_user)
            ->with('data_rekening',$data_rekening);

    }

    public function update(Request $request,$id,$id2){
        $data_user=User::find($id);
        $data_rekening=Rekening::find($id2);

        $data_rekening->bank = $request->input("bank");
        $data_rekening->no_rek = $request->input("no_rek");
        $data_rekening->atas_nama = $request->input("atas_nama");

        $data_rekening->save();

        if($data_rekening){
            Session::flash('sukses','Sukses Update Data');
            return redirect('users/rekening/'.$data_user->id.'/'.$data_rekening->id_event)
                ->with('data_user',$data_user)
                ->with('data_event',$data_rekening);
        }else{
            Session::flash('gagal','Gagal Update Data');
            return redirect('users/rekening/'.$data_user->id.'/'.$data_rekening->id_event)
                ->with('data_user',$data_user)
                ->with('data_event',$data_rekening);
        }

    }
    public function delete($id, $id2){
        $data_user = User::find($id);
        $data_rekening = Rekening::find($id2);
        $data_rekening -> delete();
        if($data_rekening){
            Session::flash('sukses','Sukses Delete Data');
            return redirect()->back();
        }else{
            Session::flash('gagal','Gagal Delete Data');
            return redirect()->back();
        }
    }

    public function admin_rekening(){
        $data_rekening=DB::table('rekening')
            ->leftjoin('event','event.id','=','rekening.id_event')
            ->leftjoin('penyelenggara','penyelenggara.id','=','rekening.id_penyelenggara')
            ->select('event.*','penyelenggara.*','rekening.*')
            ->paginate(5);

        $count = DB::table('transaksi')
            ->rightJoin('event_tiket', 'event_tiket.id', '=', 'transaksi.id_eventtiket')
            ->select('event_tiket.id_event', DB::raw('SUM(transaksi.total) as hasil'))
            ->groupBy('event_tiket.id_event')
            ->get();

        $detail_transaksi = DB::table('transaksi_detail')
            ->leftJoin('event_tiket', 'event_tiket.id', '=', 'transaksi_detail.id_eventtiket')
            ->leftjoin('transaksi','transaksi.id','transaksi_detail.transaksi_id')
            ->select('event_tiket.id_event','transaksi.*','transaksi_detail.*')
            ->get();
        
        return view('admin-rekening.admin-rekening')
            ->with('count',$count)
            ->with('detail_transaksi',$detail_transaksi)
            ->with('data_rekening',$data_rekening);
    }
}
