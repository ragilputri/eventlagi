<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Transaksi;
use App\Komentar;

class TransaksiNotification2 extends Mailable
{
    use Queueable, SerializesModels;

    public $data_transaksi;
    public $komentar;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaksi $data_transaksi, Komentar $komentar)
    {
        $this->data_transaksi = $data_transaksi;
        $this->komentar = $komentar;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Notification.transaksinotif-2');
    }
}
