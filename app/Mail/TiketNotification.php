<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Tiket;
use App\Event;
use App\Transaksi;
use Illuminate\Support\Env;

class TiketNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $data_eventtiket;
    public $data_event;
    public $data_transaksi;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tiket $data_eventtiket, Transaksi $data_transaksi, Event $data_event)
    {
        $this->data_eventtiket = $data_eventtiket;
        $this->data_transaksi = $data_transaksi;
        $this->data_event = $data_event;

    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $base_url = env('BASE_URL');
        return $this->view('Notification.tiketnotif', compact('base_url'));
    }
}
