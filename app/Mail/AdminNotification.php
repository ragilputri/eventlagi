<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Penyelenggara;
use App\Event;

class AdminNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $data_penyelenggara;
    public $data_event;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Penyelenggara $data_penyelenggara, Event $data_event)
    {
        $this->data_penyelenggara = $data_penyelenggara;
        $this->data_event = $data_event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Notification.adminnotif');
    }
}
