<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\PasswordReset;

class PasswordNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reset;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PasswordReset $reset)
    {
        $this->reset = $reset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Notification.reset-password');
    }
}
