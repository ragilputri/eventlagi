<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Transaksi;
use App\Event;
use App\KategoriTiket;

class TransaksiNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $data_transaksi;
    public $data_event;
    public $data_kategori;
    public $data_qr;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaksi $data_transaksi, Event $data_event, KategoriTiket $data_kategori, $data_qr)
    {
        $this->data_transaksi = $data_transaksi;
        $this->data_event = $data_event;
        $this->data_kategori = $data_kategori;
        $this->data_qr = $data_qr;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Notification.transaksinotif');
    }
}
