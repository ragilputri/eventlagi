<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Event extends Model
{
    // use \Spatie\Tags\HasTags;
    protected $table = "event";
    protected $guarded = [];


    // protected $fillable = [
    //     'tags'
    // ];

    // /**
    //  * The attributes that should be casted to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'tags' => 'array',
    // ];
}
