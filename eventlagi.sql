-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2021 at 09:16 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventlagi`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_penyelenggara` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_event` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `tempat` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_mulai` time NOT NULL,
  `waktu_selesai` time NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kuota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL,
  `registered` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `id_penyelenggara`, `jenis_event`, `foto`, `nama`, `tanggal`, `tempat`, `waktu_mulai`, `waktu_selesai`, `deskripsi`, `status`, `link`, `kuota`, `created_at`, `updated_at`, `slug`, `views`, `registered`) VALUES
(5, '2', 'Seminar', 'sem.jpg-1611362691.jpg', 'Seminar intelektual Dihjab', '2015-12-16', 'USIM', '14:00:00', '18:00:00', 'Didatangi dewan kuliah FSU', 'Online', 'https://seminar-intelektual-dihjab', '1800', '2021-01-23 08:44:51', '2021-01-23 08:44:51', '', 0, 0),
(6, '3', 'workshop', 'un.jpg-1611362905.jpg', 'Fostering Online Education', '2016-06-06', 'Yogyakarta', '07:00:00', '11:00:00', 'seminar dan workshop online', 'Online', 'https://Fostering-Online-Education', '250', '2021-01-23 08:48:25', '2021-01-23 08:48:25', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_tiket`
--

CREATE TABLE `event_tiket` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tiket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_peserta` int(11) DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ktp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_tiket`
--

INSERT INTO `event_tiket` (`id`, `id_event`, `id_tiket`, `created_at`, `updated_at`, `deleted_at`, `id_peserta`, `name`, `email`, `no_telp`, `alamat`, `no_ktp`) VALUES
(1, '5', '4', '2021-02-04 02:39:33', '2021-02-04 02:39:33', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(2, '5', '4', '2021-02-04 02:43:51', '2021-02-04 02:43:51', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Purwokerto', '3103119032'),
(3, '5', '4', '2021-02-04 02:44:41', '2021-02-04 02:44:41', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Purwokerto', '3103119032'),
(4, '5', '4', '2021-02-04 02:45:33', '2021-02-04 02:45:33', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(5, '5', '4', '2021-02-04 02:46:11', '2021-02-04 02:46:11', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(6, '5', '4', '2021-02-04 19:36:59', '2021-02-04 19:36:59', NULL, NULL, 'Arina Kafa', 'cs@rscyber.com', '08123455', 'Jl. Hastrodirono RT 01 RW 05 Kudus', 'hsiqvw 321'),
(7, '5', '4', '2021-02-04 19:37:33', '2021-02-04 19:37:33', NULL, NULL, 'Arina Kafa', 'cs@rscyber.com', '08123455', 'Jl. Hastrodirono RT 01 RW 05 Kudus', 'hsiqvw 321'),
(8, '5', '5', '2021-02-07 03:52:53', '2021-02-07 03:52:53', NULL, NULL, 'Regina', 'Reginapermata06@gmail.com', '08123455', 'Purwokerto', '12345678'),
(9, '5', '5', '2021-02-07 20:53:30', '2021-02-07 20:53:30', NULL, NULL, 'Atina Nora Haya', 'raratna@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119032'),
(10, '5', '5', '2021-02-07 20:57:16', '2021-02-07 20:57:16', NULL, NULL, 'Regina', 'regintud@gmail.com', '0862456819006', 'jambi', '091655290075'),
(11, '5', '4', '2021-02-09 00:18:42', '2021-02-09 00:18:42', NULL, NULL, 'Riana', 'ariana@gmail.com', '087630026668', 'jakarta', '30000175529'),
(12, '5', '4', '2021-02-16 19:40:54', '2021-02-16 19:40:54', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(13, '5', '5', '2021-02-17 19:00:17', '2021-02-17 19:00:17', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(14, '5', '4', '2021-02-18 02:03:12', '2021-02-18 02:03:12', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(15, '5', '3', '2021-02-18 02:05:12', '2021-02-18 02:05:12', NULL, NULL, 'Regina', 'atinanhy@gmail.com', '081228467330', 'Purwokerto', '3103119112'),
(16, '5', '3', '2021-02-18 02:08:56', '2021-02-18 02:08:56', NULL, NULL, 'Regina', 'atinanhy@gmail.com', '081228467330', 'Purwokerto', '3103119112'),
(17, '5', '3', '2021-02-18 02:23:09', '2021-02-18 02:23:09', NULL, NULL, 'Regina', 'atinanhy@gmail.com', '081228467330', 'Purwokerto', '3103119112'),
(18, '5', '3', '2021-02-18 02:47:01', '2021-02-18 02:47:01', NULL, NULL, 'Regina', 'atinanhy@gmail.com', '081228467330', 'Purwokerto', '3103119112'),
(19, '5', '4', '2021-02-18 02:53:17', '2021-02-18 02:53:17', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(20, '5', '4', '2021-02-18 02:54:29', '2021-02-18 02:54:29', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(21, '5', '4', '2021-02-18 02:56:27', '2021-02-18 02:56:27', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(22, '5', '4', '2021-02-18 02:57:03', '2021-02-18 02:57:03', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(23, '5', '4', '2021-02-18 02:58:32', '2021-02-18 02:58:32', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(24, '5', '4', '2021-02-18 03:00:18', '2021-02-18 03:00:18', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(25, '5', '4', '2021-02-18 03:05:08', '2021-02-18 03:05:08', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(26, '5', '4', '2021-02-18 03:06:48', '2021-02-18 03:06:48', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(27, '5', '4', '2021-02-18 03:12:49', '2021-02-18 03:12:49', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(28, '5', '4', '2021-02-18 03:12:55', '2021-02-18 03:12:55', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(29, '5', '3', '2021-02-18 03:30:47', '2021-02-18 03:30:47', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119112'),
(30, '5', '3', '2021-02-18 03:36:06', '2021-02-18 03:36:06', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119112'),
(31, '5', '4', '2021-02-18 04:25:54', '2021-02-18 04:25:54', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Medan', '091655290075'),
(32, '5', '4', '2021-02-18 06:40:35', '2021-02-18 06:40:35', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '30000175529'),
(33, '5', '4', '2021-02-18 06:42:13', '2021-02-18 06:42:13', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '30000175529'),
(34, '5', '5', '2021-02-18 06:44:29', '2021-02-18 06:44:29', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(35, '5', '5', '2021-02-18 06:46:36', '2021-02-18 06:46:36', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(36, '5', '4', '2021-02-18 06:46:47', '2021-02-18 06:46:47', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(37, '5', '4', '2021-02-18 06:46:54', '2021-02-18 06:46:54', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(38, '5', '4', '2021-02-18 06:47:01', '2021-02-18 06:47:01', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(39, '5', '4', '2021-02-18 06:49:13', '2021-02-18 06:49:13', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(40, '5', '5', '2021-02-18 06:51:16', '2021-02-18 06:51:16', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(41, '5', '5', '2021-02-18 06:52:38', '2021-02-18 06:52:38', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(42, '5', '5', '2021-02-18 06:52:44', '2021-02-18 06:52:44', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '12345678'),
(43, '5', '4', '2021-02-18 07:04:10', '2021-02-18 07:04:10', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Purwokerto', '3103119032'),
(44, '5', '4', '2021-02-18 07:07:25', '2021-02-18 07:07:25', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330', 'jambi', 'hsiqvw 321'),
(45, '5', '4', '2021-02-18 07:13:36', '2021-02-18 07:13:36', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(46, '5', '5', '2021-02-18 07:14:12', '2021-02-18 07:14:12', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(47, '5', '5', '2021-02-18 07:14:47', '2021-02-18 07:14:47', NULL, NULL, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337', 'Kudus', '3103119032'),
(48, '5', '4', '2021-02-18 07:16:09', '2021-02-18 07:16:09', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '08123455', 'Kudus', '12345678'),
(49, '5', '5', '2021-02-18 07:20:02', '2021-02-18 07:20:02', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '08123455', 'Kudus', '12345678'),
(50, '5', '5', '2021-02-18 07:20:06', '2021-02-18 07:20:06', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '08123455', 'Kudus', '12345678'),
(51, '5', '5', '2021-02-18 07:20:13', '2021-02-18 07:20:13', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '08123455', 'Kudus', '12345678'),
(52, '5', '4', '2021-02-18 07:22:23', '2021-02-18 07:22:23', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(53, '5', '4', '2021-02-18 07:26:01', '2021-02-18 07:26:01', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(54, '5', '4', '2021-02-18 07:29:09', '2021-02-18 07:29:09', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(55, '5', '4', '2021-02-18 07:32:26', '2021-02-18 07:32:26', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(56, '5', '4', '2021-02-18 07:34:55', '2021-02-18 07:34:55', NULL, NULL, 'Atina', 'atinanhy@gmail.com', '081228467337', 'Jl. Hastrodirono RT 01 RW 05 Kudus', '3103119112'),
(57, '5', '4', '2021-02-18 07:38:20', '2021-02-18 07:38:20', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(58, '5', '4', '2021-02-18 07:40:34', '2021-02-18 07:40:34', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(59, '5', '4', '2021-02-18 07:42:02', '2021-02-18 07:42:02', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(60, '5', '4', '2021-02-18 07:46:24', '2021-02-18 07:46:24', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(61, '5', '4', '2021-02-18 07:47:40', '2021-02-18 07:47:40', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(62, '5', '4', '2021-02-18 07:49:45', '2021-02-18 07:49:45', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(63, '5', '4', '2021-02-18 07:50:18', '2021-02-18 07:50:18', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(64, '5', '4', '2021-02-18 07:51:42', '2021-02-18 07:51:42', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637', 'Purwokerto', '30000175529'),
(65, '5', '4', '2021-02-18 07:54:24', '2021-02-18 07:54:24', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330', 'Kudus', '091655290075'),
(66, '5', '4', '2021-02-18 07:54:31', '2021-02-18 07:54:31', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330', 'Kudus', '091655290075'),
(67, '5', '4', '2021-02-18 07:57:40', '2021-02-18 07:57:40', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330', 'Kudus', '091655290075'),
(68, '5', '5', '2021-02-18 07:58:38', '2021-02-18 07:58:38', NULL, NULL, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330', 'Kudus', '091655290075'),
(69, '5', '5', '2021-02-18 07:59:34', '2021-02-18 07:59:34', NULL, NULL, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330', 'Kudus', '091655290075'),
(70, '5', '5', '2021-02-18 07:59:42', '2021-02-18 07:59:42', NULL, NULL, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330', 'Kudus', '091655290075'),
(71, '5', '5', '2021-02-18 08:01:09', '2021-02-18 08:01:09', NULL, NULL, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330', 'Kudus', '091655290075'),
(72, '5', '5', '2021-02-18 08:01:50', '2021-02-18 08:01:50', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330', 'Kudus', '091655290075'),
(73, '5', '5', '2021-02-18 08:04:13', '2021-02-18 08:04:13', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330', 'Kudus', '091655290075'),
(74, '5', '5', '2021-02-18 08:15:55', '2021-02-18 08:15:55', NULL, NULL, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330', 'Kudus', '091655290075');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_14_085206_create_penyelenggara_table', 1),
(5, '2021_01_14_085446_create_event_table', 1),
(6, '2021_01_14_085530_create_event_tiket_table', 1),
(7, '2021_01_14_085604_create_tiket_kategori_table', 1),
(8, '2021_01_14_085632_create_peserta_table', 1),
(9, '2021_01_14_085706_create_transaksi_table', 1),
(10, '2021_01_21_025331_update_kategori_table', 1),
(11, '2021_01_21_025710_update_tiket_kategori_table', 1),
(12, '2021_01_21_032024_add_id_event_to_tiket_kategori_table', 1),
(13, '2021_01_26_021742_add_data_to_users_table', 2),
(14, '2021_01_26_050257_add_coloumn_to_event_table', 3),
(15, '2021_01_26_050707_add_coloumn_to_tiket_kategori_table', 4),
(16, '2021_01_26_071201_add_data_to_event_tiket_table', 5),
(17, '2021_01_26_083028_add_data_to_peserta_table', 6),
(18, '2021_01_26_091004_add_data_to_transaksi_table', 7),
(19, '2021_01_26_095213_create_transaksi_detail_table', 8),
(20, '2021_02_11_024012_add_bio_to_users_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `penyelenggara`
--

CREATE TABLE `penyelenggara` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ktp` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirm_notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirm_time` datetime NOT NULL,
  `category_id` int(11) NOT NULL,
  `gift_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attend_time` datetime NOT NULL,
  `admin_id` int(11) NOT NULL,
  `reception_id` int(11) NOT NULL,
  `qr_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `ktp` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT '2021-01-26 02:02:06',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tiket_kategori`
--

CREATE TABLE `tiket_kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tiket_kategori`
--

INSERT INTO `tiket_kategori` (`id`, `id_event`, `jenis`, `harga`, `created_at`, `updated_at`, `deleted_at`, `deskripsi`, `status`) VALUES
(3, '5', 'Silver', '50.000', '2021-01-26 00:36:48', '2021-01-26 00:36:48', '2021-01-27 22:07:15', '', 0),
(4, '5', 'Diamond', '150.000', '2021-01-26 00:37:13', '2021-01-26 00:37:13', '2021-01-27 22:07:15', '', 0),
(5, '5', 'Gold', '70.000', '2021-01-26 00:37:29', '2021-01-26 00:37:29', '2021-01-27 22:07:15', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_link` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_approve` datetime DEFAULT NULL,
  `id_eventtiket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_peserta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_up` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp(),
  `qr_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime NOT NULL DEFAULT '2021-01-26 09:51:29',
  `harga` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `user_id`, `payment_link`, `payment_method`, `payment_date`, `payment_approve`, `id_eventtiket`, `id_peserta`, `status`, `bukti_up`, `waktu`, `qr_code`, `created_at`, `updated_at`, `deleted_at`, `harga`, `discount`, `total`, `name`, `email`, `no_hp`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-04 09:46:11', NULL, '2021-02-04 02:46:11', '2021-02-04 02:46:11', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(2, NULL, NULL, NULL, NULL, NULL, '6', NULL, NULL, NULL, '2021-02-05 02:36:59', NULL, '2021-02-04 19:36:59', '2021-02-04 19:36:59', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'cs@rscyber.com', '08123455'),
(3, NULL, NULL, NULL, NULL, NULL, '7', NULL, NULL, NULL, '2021-02-05 02:37:33', NULL, '2021-02-04 19:37:33', '2021-02-04 19:37:33', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'cs@rscyber.com', '08123455'),
(4, NULL, NULL, NULL, NULL, NULL, '8', NULL, NULL, NULL, '2021-02-07 10:52:53', NULL, '2021-02-07 03:52:53', '2021-02-07 03:52:53', '2021-01-26 09:51:29', 70, NULL, 70, 'Regina', 'Reginapermata06@gmail.com', '08123455'),
(5, 4, NULL, NULL, NULL, NULL, '9', NULL, NULL, 'cut.pngC:\\xampp\\tmp\\php13BC.tmp', '2021-02-08 03:53:30', NULL, '2021-02-07 20:53:30', '2021-02-07 20:57:28', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'raratna@gmail.com', '081228467337'),
(6, 4, NULL, NULL, NULL, NULL, '10', NULL, NULL, 'charlieq.jpgC:\\xampp\\tmp\\php3FF1.tmp', '2021-02-08 03:57:16', NULL, '2021-02-07 20:57:16', '2021-02-09 01:55:28', '2021-01-26 09:51:29', 70, NULL, 70, 'Regina', 'regintud@gmail.com', '0862456819006'),
(7, 4, NULL, NULL, NULL, NULL, '11', NULL, NULL, 'cut.pngC:\\xampp\\tmp\\php1129.tmp', '2021-02-09 07:18:42', NULL, '2021-02-09 00:18:42', '2021-02-09 02:00:44', '2021-01-26 09:51:29', 150, NULL, 150, 'Riana', 'ariana@gmail.com', '087630026668'),
(8, 15, NULL, NULL, NULL, NULL, '12', NULL, NULL, NULL, '2021-02-17 02:40:54', NULL, '2021-02-16 19:40:54', '2021-02-16 19:40:54', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(9, 5, NULL, NULL, NULL, NULL, '13', NULL, NULL, NULL, '2021-02-18 02:00:17', NULL, '2021-02-17 19:00:17', '2021-02-17 19:00:17', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(10, 5, NULL, NULL, NULL, NULL, '14', NULL, NULL, NULL, '2021-02-18 02:03:12', NULL, '2021-02-18 02:03:12', '2021-02-18 02:03:12', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(11, 5, NULL, NULL, NULL, NULL, '15', NULL, NULL, NULL, '2021-02-18 02:05:12', NULL, '2021-02-18 02:05:12', '2021-02-18 02:05:12', '2021-01-26 09:51:29', 50, NULL, 50, 'Regina', 'atinanhy@gmail.com', '081228467330'),
(12, 5, NULL, NULL, NULL, NULL, '16', NULL, NULL, NULL, '2021-02-18 02:08:56', NULL, '2021-02-18 02:08:56', '2021-02-18 02:08:56', '2021-01-26 09:51:29', 50, NULL, 50, 'Regina', 'atinanhy@gmail.com', '081228467330'),
(13, 5, NULL, NULL, NULL, NULL, '17', NULL, NULL, NULL, '2021-02-18 02:23:09', NULL, '2021-02-18 02:23:09', '2021-02-18 02:23:09', '2021-01-26 09:51:29', 50, NULL, 50, 'Regina', 'atinanhy@gmail.com', '081228467330'),
(14, 5, NULL, NULL, NULL, NULL, '18', NULL, NULL, NULL, '2021-02-18 02:47:01', NULL, '2021-02-18 02:47:01', '2021-02-18 02:47:01', '2021-01-26 09:51:29', 50, NULL, 50, 'Regina', 'atinanhy@gmail.com', '081228467330'),
(15, 5, NULL, NULL, NULL, NULL, '19', NULL, NULL, NULL, '2021-02-18 02:53:17', NULL, '2021-02-18 02:53:17', '2021-02-18 02:53:17', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(16, 5, NULL, NULL, NULL, NULL, '20', NULL, NULL, NULL, '2021-02-18 02:54:29', NULL, '2021-02-18 02:54:29', '2021-02-18 02:54:29', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(17, 5, NULL, NULL, NULL, NULL, '21', NULL, NULL, NULL, '2021-02-18 02:56:27', NULL, '2021-02-18 02:56:27', '2021-02-18 02:56:27', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(18, 5, NULL, NULL, NULL, NULL, '22', NULL, NULL, NULL, '2021-02-18 02:57:03', NULL, '2021-02-18 02:57:03', '2021-02-18 02:57:03', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(19, 5, NULL, NULL, NULL, NULL, '23', NULL, NULL, NULL, '2021-02-18 02:58:32', NULL, '2021-02-18 02:58:32', '2021-02-18 02:58:32', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(20, 5, NULL, NULL, NULL, NULL, '24', NULL, NULL, NULL, '2021-02-18 03:00:18', NULL, '2021-02-18 03:00:18', '2021-02-18 03:00:18', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(21, 5, NULL, NULL, NULL, NULL, '25', NULL, NULL, NULL, '2021-02-18 03:05:08', NULL, '2021-02-18 03:05:08', '2021-02-18 03:05:08', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(22, 5, NULL, NULL, NULL, NULL, '26', NULL, NULL, NULL, '2021-02-18 03:06:48', NULL, '2021-02-18 03:06:48', '2021-02-18 03:06:48', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(23, 5, NULL, NULL, NULL, NULL, '27', NULL, NULL, NULL, '2021-02-18 03:12:49', NULL, '2021-02-18 03:12:49', '2021-02-18 03:12:49', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(24, 5, NULL, NULL, NULL, NULL, '28', NULL, NULL, NULL, '2021-02-18 03:12:55', NULL, '2021-02-18 03:12:55', '2021-02-18 03:12:55', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(25, 5, NULL, NULL, NULL, NULL, '29', NULL, NULL, NULL, '2021-02-18 03:30:47', NULL, '2021-02-18 03:30:47', '2021-02-18 03:30:47', '2021-01-26 09:51:29', 50, NULL, 50, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(26, 5, NULL, NULL, NULL, NULL, '30', NULL, NULL, NULL, '2021-02-18 03:36:06', NULL, '2021-02-18 03:36:06', '2021-02-18 03:36:06', '2021-01-26 09:51:29', 50, NULL, 50, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(27, 5, NULL, NULL, NULL, NULL, '31', NULL, NULL, NULL, '2021-02-18 04:25:54', NULL, '2021-02-18 04:25:54', '2021-02-18 04:25:54', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(28, 5, NULL, NULL, NULL, NULL, '32', NULL, NULL, NULL, '2021-02-18 06:40:35', NULL, '2021-02-18 06:40:35', '2021-02-18 06:40:35', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(29, 5, NULL, NULL, NULL, NULL, '33', NULL, NULL, NULL, '2021-02-18 06:42:13', NULL, '2021-02-18 06:42:13', '2021-02-18 06:42:13', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(30, 5, NULL, NULL, NULL, NULL, '34', NULL, NULL, NULL, '2021-02-18 06:44:29', NULL, '2021-02-18 06:44:29', '2021-02-18 06:44:29', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(31, 5, NULL, NULL, NULL, NULL, '35', NULL, NULL, NULL, '2021-02-18 06:46:36', NULL, '2021-02-18 06:46:36', '2021-02-18 06:46:36', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(32, 5, NULL, NULL, NULL, NULL, '36', NULL, NULL, NULL, '2021-02-18 06:46:47', NULL, '2021-02-18 06:46:47', '2021-02-18 06:46:47', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(33, 5, NULL, NULL, NULL, NULL, '37', NULL, NULL, NULL, '2021-02-18 06:46:54', NULL, '2021-02-18 06:46:54', '2021-02-18 06:46:54', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(34, 5, NULL, NULL, NULL, NULL, '38', NULL, NULL, NULL, '2021-02-18 06:47:01', NULL, '2021-02-18 06:47:01', '2021-02-18 06:47:01', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(35, 5, NULL, NULL, NULL, NULL, '39', NULL, NULL, NULL, '2021-02-18 06:49:13', NULL, '2021-02-18 06:49:13', '2021-02-18 06:49:13', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(36, 5, NULL, NULL, NULL, NULL, '40', NULL, NULL, NULL, '2021-02-18 06:51:16', NULL, '2021-02-18 06:51:16', '2021-02-18 06:51:16', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(37, 5, NULL, NULL, NULL, NULL, '41', NULL, NULL, NULL, '2021-02-18 06:52:38', NULL, '2021-02-18 06:52:38', '2021-02-18 06:52:38', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(38, 5, NULL, NULL, NULL, NULL, '42', NULL, NULL, NULL, '2021-02-18 06:52:44', NULL, '2021-02-18 06:52:44', '2021-02-18 06:52:44', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(39, 5, NULL, NULL, NULL, NULL, '43', NULL, NULL, NULL, '2021-02-18 07:04:10', NULL, '2021-02-18 07:04:10', '2021-02-18 07:04:10', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(40, 5, NULL, NULL, NULL, NULL, '44', NULL, NULL, NULL, '2021-02-18 07:07:25', NULL, '2021-02-18 07:07:25', '2021-02-18 07:07:25', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467330'),
(41, 5, NULL, NULL, NULL, NULL, '45', NULL, NULL, NULL, '2021-02-18 07:13:36', NULL, '2021-02-18 07:13:36', '2021-02-18 07:13:36', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(42, 5, NULL, NULL, NULL, NULL, '46', NULL, NULL, NULL, '2021-02-18 07:14:12', NULL, '2021-02-18 07:14:12', '2021-02-18 07:14:12', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(43, 5, NULL, NULL, NULL, NULL, '47', NULL, NULL, NULL, '2021-02-18 07:14:47', NULL, '2021-02-18 07:14:47', '2021-02-18 07:14:47', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina Nora Haya', 'atinanhy@gmail.com', '081228467337'),
(44, 5, NULL, NULL, NULL, NULL, '48', NULL, NULL, NULL, '2021-02-18 07:16:09', NULL, '2021-02-18 07:16:09', '2021-02-18 07:16:09', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina', 'atinanhy@gmail.com', '08123455'),
(45, 5, NULL, NULL, NULL, NULL, '49', NULL, NULL, NULL, '2021-02-18 07:20:02', NULL, '2021-02-18 07:20:02', '2021-02-18 07:20:02', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina', 'atinanhy@gmail.com', '08123455'),
(46, 5, NULL, NULL, NULL, NULL, '50', NULL, NULL, NULL, '2021-02-18 07:20:06', NULL, '2021-02-18 07:20:06', '2021-02-18 07:20:06', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina', 'atinanhy@gmail.com', '08123455'),
(47, 5, NULL, NULL, NULL, NULL, '51', NULL, NULL, NULL, '2021-02-18 07:20:13', NULL, '2021-02-18 07:20:13', '2021-02-18 07:20:13', '2021-01-26 09:51:29', 70, NULL, 70, 'Atina', 'atinanhy@gmail.com', '08123455'),
(48, 5, NULL, NULL, NULL, NULL, '52', NULL, NULL, NULL, '2021-02-18 07:22:23', NULL, '2021-02-18 07:22:23', '2021-02-18 07:22:23', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina', 'atinanhy@gmail.com', '081228467337'),
(49, 5, NULL, NULL, NULL, NULL, '53', NULL, NULL, NULL, '2021-02-18 07:26:02', NULL, '2021-02-18 07:26:02', '2021-02-18 07:26:02', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina', 'atinanhy@gmail.com', '081228467337'),
(50, 5, NULL, NULL, NULL, NULL, '54', NULL, NULL, NULL, '2021-02-18 07:29:09', NULL, '2021-02-18 07:29:09', '2021-02-18 07:29:09', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina', 'atinanhy@gmail.com', '081228467337'),
(51, 5, NULL, NULL, NULL, NULL, '55', NULL, NULL, NULL, '2021-02-18 07:32:26', NULL, '2021-02-18 07:32:26', '2021-02-18 07:32:26', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina', 'atinanhy@gmail.com', '081228467337'),
(52, 5, NULL, NULL, NULL, NULL, '56', NULL, NULL, NULL, '2021-02-18 07:34:55', NULL, '2021-02-18 07:34:55', '2021-02-18 07:34:55', '2021-01-26 09:51:29', 150, NULL, 150, 'Atina', 'atinanhy@gmail.com', '081228467337'),
(53, 5, NULL, NULL, NULL, NULL, '57', NULL, NULL, NULL, '2021-02-18 07:38:20', NULL, '2021-02-18 07:38:20', '2021-02-18 07:38:20', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(54, 5, NULL, NULL, NULL, NULL, '58', NULL, NULL, NULL, '2021-02-18 07:40:34', NULL, '2021-02-18 07:40:34', '2021-02-18 07:40:34', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(55, 5, NULL, NULL, NULL, NULL, '59', NULL, NULL, NULL, '2021-02-18 07:42:02', NULL, '2021-02-18 07:42:02', '2021-02-18 07:42:02', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(56, 5, NULL, NULL, NULL, NULL, '60', NULL, NULL, NULL, '2021-02-18 07:46:24', NULL, '2021-02-18 07:46:24', '2021-02-18 07:46:24', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(57, 5, NULL, NULL, NULL, NULL, '61', NULL, NULL, NULL, '2021-02-18 07:47:40', NULL, '2021-02-18 07:47:40', '2021-02-18 07:47:40', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(58, 5, NULL, NULL, NULL, NULL, '62', NULL, NULL, NULL, '2021-02-18 07:49:45', NULL, '2021-02-18 07:49:45', '2021-02-18 07:49:45', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(59, 5, NULL, NULL, NULL, NULL, '63', NULL, NULL, NULL, '2021-02-18 07:50:18', NULL, '2021-02-18 07:50:18', '2021-02-18 07:50:18', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(60, 5, NULL, NULL, NULL, NULL, '64', NULL, NULL, NULL, '2021-02-18 07:51:42', NULL, '2021-02-18 07:51:42', '2021-02-18 07:51:42', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '08363736323637'),
(61, 5, NULL, NULL, NULL, NULL, '65', NULL, NULL, NULL, '2021-02-18 07:54:24', NULL, '2021-02-18 07:54:24', '2021-02-18 07:54:24', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330'),
(62, 5, NULL, NULL, NULL, NULL, '66', NULL, NULL, NULL, '2021-02-18 07:54:31', NULL, '2021-02-18 07:54:31', '2021-02-18 07:54:31', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330'),
(63, 5, NULL, NULL, NULL, NULL, '67', NULL, NULL, NULL, '2021-02-18 07:57:40', NULL, '2021-02-18 07:57:40', '2021-02-18 07:57:40', '2021-01-26 09:51:29', 150, NULL, 150, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330'),
(64, 5, NULL, NULL, NULL, NULL, '68', NULL, NULL, NULL, '2021-02-18 07:58:38', NULL, '2021-02-18 07:58:38', '2021-02-18 07:58:38', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330'),
(65, 5, NULL, NULL, NULL, NULL, '69', NULL, NULL, NULL, '2021-02-18 07:59:34', NULL, '2021-02-18 07:59:34', '2021-02-18 07:59:34', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330'),
(66, 5, NULL, NULL, NULL, NULL, '70', NULL, NULL, NULL, '2021-02-18 07:59:42', NULL, '2021-02-18 07:59:42', '2021-02-18 07:59:42', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330'),
(67, 5, NULL, NULL, NULL, NULL, '71', NULL, NULL, NULL, '2021-02-18 08:01:09', NULL, '2021-02-18 08:01:09', '2021-02-18 08:01:09', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'ragilputrirahmadani03@gmail.com', '081228467330'),
(68, 5, NULL, NULL, NULL, NULL, '72', NULL, NULL, NULL, '2021-02-18 08:01:50', NULL, '2021-02-18 08:01:50', '2021-02-18 08:01:50', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330'),
(69, 5, NULL, NULL, NULL, NULL, '73', NULL, NULL, NULL, '2021-02-18 08:04:13', NULL, '2021-02-18 08:04:13', '2021-02-18 08:04:13', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330'),
(70, 5, NULL, NULL, NULL, NULL, '74', NULL, NULL, NULL, '2021-02-18 08:15:55', NULL, '2021-02-18 08:15:55', '2021-02-18 08:15:55', '2021-01-26 09:51:29', 70, NULL, 70, 'Arina Kafa', 'atinanhy@gmail.com', '081228467330');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` int(11) NOT NULL,
  `id_eventtiket` int(11) NOT NULL,
  `harga` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `potongan` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `total` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`id`, `transaksi_id`, `id_eventtiket`, `harga`, `potongan`, `total`, `created_at`, `updated_at`) VALUES
(1, 3, 7, '150.000', NULL, '150.000', '2021-02-04 19:37:33', '2021-02-04 19:37:33'),
(2, 4, 8, '70.000', '0', '70.000', '2021-02-07 03:52:53', '2021-02-07 03:52:53'),
(3, 5, 9, '70.000', '0', '70.000', '2021-02-07 20:53:30', '2021-02-07 20:53:30'),
(4, 6, 10, '70.000', '0', '70.000', '2021-02-07 20:57:16', '2021-02-07 20:57:16'),
(5, 7, 11, '150.000', '0', '150.000', '2021-02-09 00:18:42', '2021-02-09 00:18:42'),
(6, 8, 12, '150.000', '0', '150.000', '2021-02-16 19:40:54', '2021-02-16 19:40:54'),
(7, 9, 13, '70.000', '0', '70.000', '2021-02-17 19:00:17', '2021-02-17 19:00:17'),
(8, 10, 14, '150.000', '0', '150.000', '2021-02-18 02:03:12', '2021-02-18 02:03:12'),
(9, 11, 15, '50.000', '0', '50.000', '2021-02-18 02:05:12', '2021-02-18 02:05:12'),
(10, 12, 16, '50.000', '0', '50.000', '2021-02-18 02:08:56', '2021-02-18 02:08:56'),
(11, 13, 17, '50.000', '0', '50.000', '2021-02-18 02:23:09', '2021-02-18 02:23:09'),
(12, 14, 18, '50.000', '0', '50.000', '2021-02-18 02:47:01', '2021-02-18 02:47:01'),
(13, 15, 19, '150.000', '0', '150.000', '2021-02-18 02:53:17', '2021-02-18 02:53:17'),
(14, 16, 20, '150.000', '0', '150.000', '2021-02-18 02:54:29', '2021-02-18 02:54:29'),
(15, 17, 21, '150.000', '0', '150.000', '2021-02-18 02:56:27', '2021-02-18 02:56:27'),
(16, 18, 22, '150.000', '0', '150.000', '2021-02-18 02:57:03', '2021-02-18 02:57:03'),
(17, 19, 23, '150.000', '0', '150.000', '2021-02-18 02:58:32', '2021-02-18 02:58:32'),
(18, 20, 24, '150.000', '0', '150.000', '2021-02-18 03:00:18', '2021-02-18 03:00:18'),
(19, 21, 25, '150.000', '0', '150.000', '2021-02-18 03:05:08', '2021-02-18 03:05:08'),
(20, 22, 26, '150.000', '0', '150.000', '2021-02-18 03:06:48', '2021-02-18 03:06:48'),
(21, 23, 27, '150.000', '0', '150.000', '2021-02-18 03:12:49', '2021-02-18 03:12:49'),
(22, 24, 28, '150.000', '0', '150.000', '2021-02-18 03:12:55', '2021-02-18 03:12:55'),
(23, 25, 29, '50.000', '0', '50.000', '2021-02-18 03:30:47', '2021-02-18 03:30:47'),
(24, 26, 30, '50.000', '0', '50.000', '2021-02-18 03:36:06', '2021-02-18 03:36:06'),
(25, 27, 31, '150.000', '0', '150.000', '2021-02-18 04:25:54', '2021-02-18 04:25:54'),
(26, 28, 32, '150.000', '0', '150.000', '2021-02-18 06:40:35', '2021-02-18 06:40:35'),
(27, 29, 33, '150.000', '0', '150.000', '2021-02-18 06:42:13', '2021-02-18 06:42:13'),
(28, 30, 34, '70.000', '0', '70.000', '2021-02-18 06:44:29', '2021-02-18 06:44:29'),
(29, 31, 35, '70.000', '0', '70.000', '2021-02-18 06:46:36', '2021-02-18 06:46:36'),
(30, 32, 36, '150.000', '0', '150.000', '2021-02-18 06:46:47', '2021-02-18 06:46:47'),
(31, 33, 37, '150.000', '0', '150.000', '2021-02-18 06:46:54', '2021-02-18 06:46:54'),
(32, 34, 38, '150.000', '0', '150.000', '2021-02-18 06:47:01', '2021-02-18 06:47:01'),
(33, 35, 39, '150.000', '0', '150.000', '2021-02-18 06:49:13', '2021-02-18 06:49:13'),
(34, 36, 40, '70.000', '0', '70.000', '2021-02-18 06:51:16', '2021-02-18 06:51:16'),
(35, 37, 41, '70.000', '0', '70.000', '2021-02-18 06:52:38', '2021-02-18 06:52:38'),
(36, 38, 42, '70.000', '0', '70.000', '2021-02-18 06:52:44', '2021-02-18 06:52:44'),
(37, 39, 43, '150.000', '0', '150.000', '2021-02-18 07:04:10', '2021-02-18 07:04:10'),
(38, 40, 44, '150.000', '0', '150.000', '2021-02-18 07:07:25', '2021-02-18 07:07:25'),
(39, 41, 45, '150.000', '0', '150.000', '2021-02-18 07:13:36', '2021-02-18 07:13:36'),
(40, 42, 46, '70.000', '0', '70.000', '2021-02-18 07:14:12', '2021-02-18 07:14:12'),
(41, 43, 47, '70.000', '0', '70.000', '2021-02-18 07:14:47', '2021-02-18 07:14:47'),
(42, 44, 48, '150.000', '0', '150.000', '2021-02-18 07:16:09', '2021-02-18 07:16:09'),
(43, 45, 49, '70.000', '0', '70.000', '2021-02-18 07:20:02', '2021-02-18 07:20:02'),
(44, 46, 50, '70.000', '0', '70.000', '2021-02-18 07:20:06', '2021-02-18 07:20:06'),
(45, 47, 51, '70.000', '0', '70.000', '2021-02-18 07:20:13', '2021-02-18 07:20:13'),
(46, 48, 52, '150.000', '0', '150.000', '2021-02-18 07:22:23', '2021-02-18 07:22:23'),
(47, 49, 53, '150.000', '0', '150.000', '2021-02-18 07:26:02', '2021-02-18 07:26:02'),
(48, 50, 54, '150.000', '0', '150.000', '2021-02-18 07:29:09', '2021-02-18 07:29:09'),
(49, 51, 55, '150.000', '0', '150.000', '2021-02-18 07:32:26', '2021-02-18 07:32:26'),
(50, 52, 56, '150.000', '0', '150.000', '2021-02-18 07:34:55', '2021-02-18 07:34:55'),
(51, 53, 57, '150.000', '0', '150.000', '2021-02-18 07:38:20', '2021-02-18 07:38:20'),
(52, 54, 58, '150.000', '0', '150.000', '2021-02-18 07:40:34', '2021-02-18 07:40:34'),
(53, 55, 59, '150.000', '0', '150.000', '2021-02-18 07:42:02', '2021-02-18 07:42:02'),
(54, 56, 60, '150.000', '0', '150.000', '2021-02-18 07:46:24', '2021-02-18 07:46:24'),
(55, 57, 61, '150.000', '0', '150.000', '2021-02-18 07:47:40', '2021-02-18 07:47:40'),
(56, 58, 62, '150.000', '0', '150.000', '2021-02-18 07:49:45', '2021-02-18 07:49:45'),
(57, 59, 63, '150.000', '0', '150.000', '2021-02-18 07:50:18', '2021-02-18 07:50:18'),
(58, 60, 64, '150.000', '0', '150.000', '2021-02-18 07:51:42', '2021-02-18 07:51:42'),
(59, 61, 65, '150.000', '0', '150.000', '2021-02-18 07:54:24', '2021-02-18 07:54:24'),
(60, 62, 66, '150.000', '0', '150.000', '2021-02-18 07:54:31', '2021-02-18 07:54:31'),
(61, 63, 67, '150.000', '0', '150.000', '2021-02-18 07:57:40', '2021-02-18 07:57:40'),
(62, 64, 68, '70.000', '0', '70.000', '2021-02-18 07:58:38', '2021-02-18 07:58:38'),
(63, 65, 69, '70.000', '0', '70.000', '2021-02-18 07:59:34', '2021-02-18 07:59:34'),
(64, 66, 70, '70.000', '0', '70.000', '2021-02-18 07:59:42', '2021-02-18 07:59:42'),
(65, 67, 71, '70.000', '0', '70.000', '2021-02-18 08:01:09', '2021-02-18 08:01:09'),
(66, 68, 72, '70.000', '0', '70.000', '2021-02-18 08:01:50', '2021-02-18 08:01:50'),
(67, 69, 73, '70.000', '0', '70.000', '2021-02-18 08:04:13', '2021-02-18 08:04:13'),
(68, 70, 74, '70.000', '0', '70.000', '2021-02-18 08:15:55', '2021-02-18 08:15:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_verified_at` datetime NOT NULL DEFAULT '2021-01-26 02:50:46',
  `account_type` int(11) DEFAULT NULL,
  `account_role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-profile.png',
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `account_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `fcm_token` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `pin`, `phone`, `phone_verified_at`, `account_type`, `account_role`, `photo`, `bio`, `last_login`, `account_status`, `fcm_token`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@eventlagi.com', NULL, '$2y$10$LLfSNGKdd18kApmAGqEd6.Mrptxgo.yQ7CnJxXo6S/g.4Q4mRp0dy', '', '', '2021-01-26 02:50:46', 0, '', '', NULL, '2021-01-25 19:50:46', '1', '', NULL, NULL, NULL, NULL),
(2, 'member', 'member@eventlagi.com', NULL, '$2y$10$FDwH/GyxlX8Spq0C3tpqqeEFzk1c19QUKrBRvTRGq0Dcn6wP6B0lC', '', '', '2021-01-26 02:50:46', 0, '', '', NULL, '2021-01-25 19:50:46', '2', '', NULL, NULL, NULL, NULL),
(5, 'Rara', 'raratna@gmail.com', NULL, '$2y$10$p37SMIFn049P5sCCTbWAP.sbdQ/A9LpG7t8iWwucW9LgsSHUH5iui', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-07 23:11:28', '2', NULL, NULL, '2021-02-07 16:11:28', '2021-02-07 16:11:28', NULL),
(7, 'Roland', 'rolandanggara@gmail.com', NULL, '$2y$10$dh5A1ezQb/k48nPilCakeePxcm2ELXHSvkvZhntzNjlOyBcxK8soa', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-08 08:35:56', '2', NULL, NULL, '2021-02-08 01:35:56', '2021-02-08 01:35:56', NULL),
(8, 'Regina Permata', 'Reginapermata06@gmail.com', NULL, '$2y$10$NQu6uKTHbBT4meFFP3fsH.jKvWVCOgLlomuw9n507KnMW0A7rxtFy', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-08 08:50:31', '2', NULL, NULL, '2021-02-08 01:50:31', '2021-02-08 01:50:31', NULL),
(10, 'Ragil Putri', 'ragilputri@gmail.com', NULL, '$2y$10$lAdXNZkcyFRx/3GdPbRVDuBbqGP1WzMCAFU5eXOr4Sy50C9LmEPE6', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-08 09:03:33', '2', NULL, NULL, '2021-02-08 02:03:33', '2021-02-08 02:03:33', NULL),
(11, 'Arina Kafa', 'arinakafa@gmail.com', NULL, '$2y$10$uY6edBg0dMOhvg9JcDEgJe7wRGZlVRbFZRxR6BLdcGgeUZK6fEYba', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-08 09:16:49', '2', NULL, NULL, '2021-02-08 02:16:49', '2021-02-08 02:16:49', NULL),
(12, 'Ariana Grande', 'ariana@gmail.com', NULL, '$2y$10$.Y6KKoquTA8JOFKvybJQSOI/COYCIUpp4OtpO8cWNuNMrlvX2SGCi', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-08 09:46:23', '2', NULL, NULL, '2021-02-08 02:46:23', '2021-02-08 02:46:23', NULL),
(13, 'In\'am Muhammad', 'inamuhammad@gmail.com', NULL, '$2y$10$qlAyUTdHF2ghA7sBWSJfxumYpq7F2fyodfdm4yF7nNCw88TAuiyjO', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-09 07:28:02', '2', NULL, NULL, '2021-02-09 00:28:02', '2021-02-09 00:28:02', NULL),
(15, 'Atinanoraa', 'atinanhy@gmail.com', NULL, '$2y$10$sA6ozoqjkAV3qgPKC.9f0OVh7p7NAUQLk45TX7zM9oPYV/RcPWU6a', NULL, NULL, '2021-01-26 02:50:46', NULL, NULL, 'default-profile.png', NULL, '2021-02-17 02:35:37', '2', NULL, NULL, '2021-02-16 19:35:37', '2021-02-16 19:35:37', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_tiket`
--
ALTER TABLE `event_tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `penyelenggara`
--
ALTER TABLE `penyelenggara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tiket_kategori`
--
ALTER TABLE `tiket_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `event_tiket`
--
ALTER TABLE `event_tiket`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `penyelenggara`
--
ALTER TABLE `penyelenggara`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tiket_kategori`
--
ALTER TABLE `tiket_kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
