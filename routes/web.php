<?php

use Illuminate\Support\Facades\Route;

    // NavBar
Route::get('/', 'NavbarController@index');
Route::get('event/online','NavbarController@event_online');
Route::get('event/offline','NavbarController@event_offline');
Route::get('detail-event/{id}','NavbarController@detail');

    // Login
Route::get('login', 'LoginController@index');
Route::post('auth-login', 'LoginController@store');
Route::get('logout', 'LoginController@logout');
Route::get('forgot-pass', 'LoginController@forgot');
Route::post('forgot-pass2', 'LoginController@forgot2');
Route::get('reset-password/{token}','LoginController@reset');
Route::post('reset-password','LoginController@update_password');

    // Register
Route::get('sign-up', 'RegisterController@show');
Route::post('auth-register', 'RegisterController@save');

    // Login Create Events
Route::get('create-events/check-session','LoginController@cek');
Route::get('login-event','LoginController@login_event');
Route::post('simpan-event','LoginController@simpan_event');

    // Register Create Events
Route::get('register-event','RegisterController@register');
Route::post('save-event','RegisterController@save_event');

    //Detail Event
Route::get('detail-event/{slug}','NavbarController@detail');

    //  Login sebelum beli
Route::get('check-session/{id}','LoginTiketController@cek');
Route::get('login-tiket/{id}','LoginTiketController@index');
Route::post('simpan-login/{id}', 'LoginTiketController@store');
    
    //  Register sebelum beli
Route::get('register-tiket/{id}', 'RegisterTiketController@show');
Route::post('simpan-register/{id}', 'RegisterTiketController@save');

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {

    //  Admin
Route::get('admin/dashboard','AdminController@index');
Route::get('admin/dashboard/terima/{id}/{id2}','AdminController@terima');
Route::get('admin/dashboard/tolak/{id}/{id2}','AdminController@tolak');

    //  Transaksi
Route::get('admin/transaksi','TransaksiController@list');
Route::get('admin/transaksi/create','TransaksiController@create');
Route::post('admin/transaksi/save','TransaksiController@save');
Route::get('admin/transaksi/edit/{id}','TransaksiController@edit');
Route::post('admin/transaksi/update/{id}','TransaksiController@update');
Route::get('admin/transaksi/delete/{id}','TransaksiController@delete');
Route::get('admin/transaksi/sukses','TransaksiController@transaksi_sukses');
Route::get('admin/transaksi/belum','TransaksiController@transaksi_belum');
Route::get('admin/transaksi-tolak/{id}','TransaksiController@transaksi_tolak');
Route::get('admin/transaksi-terima/{id}/{id2}','TransaksiController@transaksi_terima');

    //  Kategori tiket
Route::get('admin/tiket','TiketKategoriController@list');
Route::get('admin/tiket/create','TiketKategoriController@create');
Route::post('admin/tiket/save','TiketKategoriController@save');
Route::get('admin/tiket/edit/{id}','TiketKategoriController@edit');
Route::post('admin/tiket/update/{id}','TiketKategoriController@update');
Route::get('admin/tiket/delete/{id}','TiketKategoriController@delete');

    //  Penyelenggara
Route::get('admin/penyelenggara','PenyelenggaraController@list');
Route::get('admin/penyelenggara/create','PenyelenggaraController@create');
Route::post('admin/penyelenggara/save','PenyelenggaraController@save');
Route::get('admin/penyelenggara/edit/{id}','PenyelenggaraController@edit');
Route::post('admin/penyelenggara/update/{id}','PenyelenggaraController@update');
Route::get('admin/penyelenggara/delete/{id}','PenyelenggaraController@delete');
Route::get('admin/penyelenggara/event/{id}','PenyelenggaraController@daftar_event');

    //  Event
Route::get('admin/event','EventController@list');
Route::get('admin/event/create','EventController@create');
Route::post('admin/event/save','EventController@save');
Route::get('admin/event/edit/{id}','EventController@edit');
Route::post('admin/event/update/{id}','EventController@update');
Route::get('admin/event/delete/{id}','EventController@delete');
Route::get('admin/event/peserta/{id}','EventController@event_peserta');

    //  Peserta
Route::get('admin/peserta','PesertaController@list');
Route::get('admin/peserta/create','PesertaController@create');
Route::post('admin/peserta/save','PesertaController@save');
Route::get('admin/peserta/edit/{id}','PesertaController@edit');
Route::post('admin/peserta/update/{id}','PesertaController@update');
Route::get('admin/peserta/delete/{id}','PesertaController@delete');

});

Route::group(['middleware' => 'App\Http\Middleware\PesertaMiddleware'], function () {

    // NavBar
Route::get('tiket-event','NavbarController@tiket');
Route::post('tiket-event/save/{id}','NavbarController@save');
Route::get('tiket-registrasi/{id}/{id2}','NavbarController@registrasi');
Route::get('tiket-transaksi/{id}/{id2}','NavbarController@tikettransaksi');
Route::post('uploadbukti/{id}','NavbarController@profiletransaksi');

    //  User Profile
Route::get('users/{id}','NavbarController@pengguna');
Route::get('users/profile/{id}','ProfileController@list');
Route::post('users/update-profile/{id}','ProfileController@update');
Route::post('users/change-password/{id}','ProfileController@change');
Route::get('users/remove-photo/{id}','ProfileController@remove');
Route::post('users/upload-photo/{id}','ProfileController@upload');

Route::get('users/penyelenggara/{id}','ProfileController@penyelenggara');
Route::get('users/penyelenggara/create/{id}','ProfileController@penyelenggara_create');
Route::post('users/penyelenggara/save/{id}','ProfileController@penyelenggara_save');

Route::get('users/events/{id}','ProfileController@events');
Route::get('users/event/create/{id}/{id2}','ProfileController@event_create');
Route::post('users/event/save/{id}/{id2}','ProfileController@event_save');
Route::get('users/event/edit/{id}/{id2}','ProfileController@event_edit');
Route::post('users/event/update/{id}/{id2}','ProfileController@event_update');
Route::get('users/event/delete/{id}/{id2}','ProfileController@event_delete');
Route::get('users/event/peserta/{id}/{id2}','ProfileController@event_peserta');

Route::get('users/tikets/{id}','ProfileController@tikets');
Route::get('users/tiket/create/{id}/{id2}','ProfileController@tiket_create');
Route::post('users/tiket/save/{id}/{id2}','ProfileController@tiket_save');
Route::get('users/kategori/tiket/{id}/{id2}','ProfileController@tiket_kategori');
Route::get('users/tiket/delete/{id}','ProfileController@tiket_delete');

    // Rekening
Route::get('users/rekening/{id}/{id2}','RekeningController@list');
Route::get('users/rekening/create/{id}/{id2}','RekeningController@create');
Route::get('users/rekening/edit/{id}/{id2}','RekeningController@edit');
Route::post('users/rekening/save/{id}/{id2}','RekeningController@save');
Route::post('users/rekening/update/{id}/{id2}','RekeningController@update');
Route::get('users/rekening/delete/{id}/{id2}', 'RekeningController@delete');
Route::get('admin/rekening', 'RekeningController@admin_rekening');

    // User Events
Route::get('users','EventController@users');
Route::get('users/events/create','EventController@buat');
Route::post('users/events/save','EventController@save');
Route::get('users/events/edit','EventController@ubah');
Route::post('users/events/update','EventController@update');
Route::get('users/events/delete/{id}','EventController@delete');

    // Tiket Confirmation
Route::get('confirm-attend/{id}/{id2}/{id3}/{id4}/{id5}/{id6}','QrcodeController@confirm');
Route::get('waiting-event/{id}/{id2}','QrcodeController@waiting');
Route::get('expired-ticket','QrcodeController@expired');
Route::get('unconfirmed-transaction/{id}/{id2}','QrcodeController@unconfirmed');

});
